; -------------------------------------------------------------------------------------------------
;
;
;   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
;    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
;    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
;    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
;    MMMM       MMMM     PPPP              MMMM       MMMM
;    MMMM       MMMM     PPPP              MMMM       MMMM
;   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
;
;                       IIIIII    888888888888        000000000        888888888888        000000000
;                        IIII   8888888888888888    0000000000000    8888888888888888    0000000000000
;                        IIII   8888        8888  0000         0000  8888        8888  0000         0000
;                        IIII     888888888888    0000         0000    888888888888    0000         0000
;                        IIII   8888        8888  0000         0000  8888        8888  0000         0000
;                        IIII   8888888888888888    0000000000000    8888888888888888    0000000000000
;                       IIIIII    888888888888        000000000        888888888888        000000000
;
;  Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
;
;  This file is part of Mpm.
;  Mpm is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by the Free Software Foundation;
;  either version 2, or (at your option) any later version.
;  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
;  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;  See the GNU General Public License for more details.
;  You should have received a copy of the GNU General Public License along with Mpm;
;  see the file COPYING.  If not, write to the
;  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;
; -----------------------------------------------------------------------------
;  Intel 8080 CPU Instruction Reference, supported by Mpm V1.9.x
;
;  To see opcodes, generate listing file: mpm -m8080 -va i8080.asm
;  To generate a binary: mpm -m8080 -vab i8080.asm
;  (-v is verbose compile, can be omitted)
; -------------------------------------------------------------------------------------------------

module i8080ref

org $4000


; -------------------------------------------------------------------------------------------------
; Main instruction set (Z80 binary compatible)
; -------------------------------------------------------------------------------------------------

        NOP                                     ; NOP
        LXI     B,$9999                         ; LD      BC,$0000
        STAX    B                               ; LD      (BC),A
        INX     B                               ; INC     BC
        INR     B                               ; INC     B
        DCR     B                               ; DEC     B
        MVI     B,0                             ; LD      B,0
        RLC                                     ; RLCA
        DAD     B                               ; ADD     HL,BC
        LDAX    B                               ; LD      A,(BC)
        DCX     B                               ; DEC     BC
        INR     C                               ; INC     C
        DCR     C                               ; DEC     C
        MVI     C,0                             ; LD      C,0
        RRC                                     ; RRCA
        LXI     D,$9999                         ; LD      DE,$0000
        STAX    D                               ; LD      (DE),A
        INX     D                               ; INC     DE
        INR     D                               ; INC     D
        DCR     D                               ; DEC     D
        MVI     D,0                             ; LD      D,0
        RAL                                     ; RLA
        DAD     D                               ; ADD     HL,DE
        LDAX    D                               ; LD      A,(DE)
        DCX     D                               ; DEC     DE
        INR     E                               ; INC     E
        DCR     E                               ; DEC     E
        MVI     E,0                             ; LD      E,0
        RAR                                     ; RRA
        LXI     H,$9999                         ; LD      HL,$0000
        SHLD    $9998                           ; LD      ($0000),HL
        INX     H                               ; INC     HL
        INR     H                               ; INC     H
        DCR     H                               ; DEC     H
        MVI     H,0                             ; LD      H,0
        DAA                                     ; DAA
        DAD     H                               ; ADD     HL,HL
        LHLD    $9998                           ; LD      HL,($9988)
        DCX     H                               ; DEC     HL
        INR     L                               ; INC     L
        DCR     L                               ; DEC     L
        MVI     L,0                             ; LD      L,0
        CMA                                     ; CPL
        LXI     SP,$9999                        ; LD      SP,$0000
        STA     $0000                           ; LD      ($0000),A
        INX     SP                              ; INC     SP
        INR     M                               ; INC     (HL)
        DCR     M                               ; DEC     (HL)
        MVI     M,0                             ; LD      (HL),0
        STC                                     ; SCF
        DAD     SP                              ; ADD     HL,SP
        LDA     $0000                           ; LD      A,($0000)
        DCX     SP                              ; DEC     SP
        INR     A                               ; INC     A
        DCR     A                               ; DEC     A
        MVI     A,0                             ; LD      A,0
        CMC                                     ; CCF
        MOV     B,B                             ; LD      B,B
        MOV     B,C                             ; LD      B,C
        MOV     B,D                             ; LD      B,D
        MOV     B,E                             ; LD      B,E
        MOV     B,H                             ; LD      B,H
        MOV     B,L                             ; LD      B,L
        MOV     B,M                             ; LD      B,(HL)
        MOV     B,A                             ; LD      B,A
        MOV     C,B                             ; LD      C,B
        MOV     C,C                             ; LD      C,C
        MOV     C,D                             ; LD      C,D
        MOV     C,E                             ; LD      C,E
        MOV     C,H                             ; LD      C,H
        MOV     C,L                             ; LD      C,L
        MOV     C,M                             ; LD      C,(HL)
        MOV     C,A                             ; LD      C,A
        MOV     D,B                             ; LD      D,B
        MOV     D,C                             ; LD      D,C
        MOV     D,D                             ; LD      D,D
        MOV     D,E                             ; LD      D,E
        MOV     D,H                             ; LD      D,H
        MOV     D,L                             ; LD      D,L
        MOV     D,M                             ; LD      D,(HL)
        MOV     D,A                             ; LD      D,A
        MOV     E,B                             ; LD      E,B
        MOV     E,C                             ; LD      E,C
        MOV     E,D                             ; LD      E,D
        MOV     E,E                             ; LD      E,E
        MOV     E,H                             ; LD      E,H
        MOV     E,L                             ; LD      E,L
        MOV     E,M                             ; LD      E,(HL)
        MOV     E,A                             ; LD      E,A
        MOV     H,B                             ; LD      H,B
        MOV     H,C                             ; LD      H,C
        MOV     H,D                             ; LD      H,D
        MOV     H,E                             ; LD      H,E
        MOV     H,H                             ; LD      H,H
        MOV     H,L                             ; LD      H,L
        MOV     H,M                             ; LD      H,(HL)
        MOV     H,A                             ; LD      H,A
        MOV     L,B                             ; LD      L,B
        MOV     L,C                             ; LD      L,C
        MOV     L,D                             ; LD      L,D
        MOV     L,E                             ; LD      L,E
        MOV     L,H                             ; LD      L,H
        MOV     L,L                             ; LD      L,L
        MOV     L,M                             ; LD      L,(HL)
        MOV     L,A                             ; LD      L,A
        MOV     M,B                             ; LD      (HL),B
        MOV     M,C                             ; LD      (HL),C
        MOV     M,D                             ; LD      (HL),D
        MOV     M,E                             ; LD      (HL),E
        MOV     M,H                             ; LD      (HL),H
        MOV     M,L                             ; LD      (HL),L
        HLT                                     ; HALT
        MOV     M,A                             ; LD      (HL),A
        MOV     A,B                             ; LD      A,B
        MOV     A,C                             ; LD      A,C
        MOV     A,D                             ; LD      A,D
        MOV     A,E                             ; LD      A,E
        MOV     A,H                             ; LD      A,H
        MOV     A,L                             ; LD      A,L
        MOV     A,M                             ; LD      A,(HL)
        MOV     A,A                             ; LD      A,A
        ADD     A,B                             ; ADD     A,B
        ADD     A,C                             ; ADD     A,C
        ADD     A,D                             ; ADD     A,D
        ADD     A,E                             ; ADD     A,E
        ADD     A,H                             ; ADD     A,H
        ADD     A,L                             ; ADD     A,L
        ADD     A,M                             ; ADD     A,(HL)
        ADD     A,A                             ; ADD     A,A
        ADC     A,B                             ; ADC     A,B
        ADC     A,C                             ; ADC     A,C
        ADC     A,D                             ; ADC     A,D
        ADC     A,E                             ; ADC     A,E
        ADC     A,H                             ; ADC     A,H
        ADC     A,L                             ; ADC     A,L
        ADC     A,M                             ; ADC     A,(HL)
        ADC     A,A                             ; ADC     A,A
        SUB     B                               ; SUB     B
        SUB     C                               ; SUB     C
        SUB     D                               ; SUB     D
        SUB     E                               ; SUB     E
        SUB     H                               ; SUB     H
        SUB     L                               ; SUB     L
        SUB     M                               ; SUB     (HL)
        SUB     A,A                             ; SUB     A,A
        SUB     A                               ; SUB     A
        SBB     B                               ; SBC     A,B
        SBB     C                               ; SBC     A,C
        SBB     D                               ; SBC     A,D
        SBB     E                               ; SBC     A,E
        SBB     H                               ; SBC     A,H
        SBB     L                               ; SBC     A,L
        SBB     M                               ; SBC     A,(HL)
        SBB     A                               ; SBC     A,A
        ANA     B                               ; AND     A,B
        ANA     B                               ; AND     B
        ANA     C                               ; AND     C
        ANA     D                               ; AND     D
        ANA     E                               ; AND     E
        ANA     H                               ; AND     H
        ANA     L                               ; AND     L
        ANA     M                               ; AND     (HL)
        ANA     A                               ; AND     A
        XRA     B                               ; XOR     A,B
        XRA     B                               ; XOR     B
        XRA     C                               ; XOR     C
        XRA     D                               ; XOR     D
        XRA     E                               ; XOR     E
        XRA     H                               ; XOR     H
        XRA     L                               ; XOR     L
        XRA     M                               ; XOR     (HL)
        XRA     A                               ; XOR     A
        ORA     A,B                             ; OR      A,B
        ORA     B                               ; OR      B
        ORA     C                               ; OR      C
        ORA     D                               ; OR      D
        ORA     E                               ; OR      E
        ORA     H                               ; OR      H
        ORA     L                               ; OR      L
        ORA     M                               ; OR      (HL)
        ORA     A                               ; OR      A
        CMP     A,B                             ; CP      A,B
        CMP     B                               ; CP      B
        CMP     C                               ; CP      C
        CMP     D                               ; CP      D
        CMP     E                               ; CP      E
        CMP     H                               ; CP      H
        CMP     L                               ; CP      L
        CMP     M                               ; CP      (HL)
        CMP     A                               ; CP      A
        RNZ                                     ; RET     NZ
        POP     B                               ; POP     BC
        JNZ     $0000                           ; JP      NZ,$0000
        JMP     $0000                           ; JP      $0000
        CNZ     $0000                           ; CALL    NZ,$0000
        PUSH    B                               ; PUSH    BC
        ADI     A,0                             ; ADD     A,0
        RST     0                               ; RST     0
        RZ                                      ; RET     Z
        RET                                     ; RET
        JZ      $0000                           ; JP      Z,$0000
        CZ      $0000                           ; CALL    Z,$0000
        CALL    $0000                           ; CALL    $0000
        ACI     A,0                             ; ADC     A,0
        RST     1                               ; RST     08h
        RST     08h
        RNC                                     ; RET     NC
        POP     D                               ; POP     DE
        JNC     $0000                           ; JP      NC,$0000
        OUT     128                             ; OUT     (0),A
        CNC     $0000                           ; CALL    NC,$0000
        PUSH    D                               ; PUSH    DE
        SUI     12                              ; SUB     12
        SUI     A,12
        RST     2                               ; RST     10h
        RST     10h
        RC                                      ; RET     C
        JC      $0000                           ; JP      C,$0000
        IN      $80                             ; IN      A,($80)
        CC      $0000                           ; CALL    C,$0000
        SBI     0                               ; SBC     A,0
        SBI     A,0
        RST     3                               ; RST     18h
        RST     18h
        RPO                                     ; RET     PO
        POP     H                               ; POP     HL
        JPO     $0000                           ; JP      PO,$0000
        XTHL                                    ; EX      (SP),HL
        CPO     $0000                           ; CALL    PO,$0000
        PUSH    H                               ; PUSH    HL
        ANI     0                               ; AND     0
        ANI     A,0
        RST     4                               ; RST     $20
        RST     $20
        RPE                                     ; RET     PE
        PCHL                                    ; JP      (HL)
        JPE     $0000                           ; JP      PE,$0000
        XCHG                                    ; EX      DE,HL
        CPE     $0000                           ; CALL    PE,$0000
        XRI     0                               ; XOR     0
        XRI     A,0
        RST     5                               ; RST     28h
        RST     28h
        RP                                      ; RET     P
        POP     PSW                             ; POP     AF
        JP      $0000                           ; JP      P,$0000
        DI                                      ; DI
        CP      $0000                           ; CALL    P,$0000
        PUSH    PSW                             ; PUSH    AF
        ORI     0                               ; OR      0
        ORI     A,0
        RST     6                               ; RST     30h
        RST     30h
        RM                                      ; RET     M
        SPHL                                    ; LD      SP,HL
        JM      $0000                           ; JP      M,$0000
        EI                                      ; EI
        CM      $0000                           ; CALL    M,$0000
        CPI     0                               ; CP      0
        CPI     A,0
        RST     7                               ; RST     38h
        RST     38h
