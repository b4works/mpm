; ***************************************************************************************************
; Mpm test library routine, compiled as part of
;
;       mpm -d -xtest.lib du16 m16 m24
;

xlib m16

; ***************************************************************************************************
;
; 16-bit unsigned multiplication
; (derived from GN_M16 / OZ, https://bitbucket.org/cambridge/oz, GPL v2)
;
; IN:
;    DE = multiplier
;    HL = multiplicant
;
; OUT:
;    HL = product
;
; Registers changed after return:
;    ..BCDE../IXIY  same
;    AF....HL/....  different
;
.m16                push de
                    ld   c, l                  ; BC=HL(in)
                    ld   b, h
                    ld   hl, 0                 ; HL=0
                    ld   a, 15
.m16_1
                    sla  e                     ; DE << 1
                    rl   d
                    jr   nc, m16_2
                    add  hl, bc                ; HL += HL(in)
.m16_2
                    add  hl, hl                ; HL << 1
                    dec  a
                    jr   nz, m16_1
                    or   d
                    jp   p, m16_3
                    add  hl, bc                ; HL += HL(in)
.m16_3
                    pop  de
                    ret
