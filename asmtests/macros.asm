; -------------------------------------------------------------------------------------------------
;
;    MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
;     MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
;     MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
;     MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
;     MMMM       MMMM     PPPP              MMMM       MMMM
;     MMMM       MMMM     PPPP              MMMM       MMMM
;    MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
;
;  Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
;
;  This file is part of Mpm.
;  Mpm is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by the Free Software Foundation;
;  either version 2, or (at your option) any later version.
;  Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
;  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;  See the GNU General Public License for more details.
;  You should have received a copy of the GNU General Public License along with Mpm;
;  see the file COPYING.  If not, write to the
;  Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;
; -------------------------------------------------------------------------------------------------
;
; Macro test-cases for Mpm V1.5
; execute to get listing output (and verbose):
; 	mpm -vam macros.asm
;
; -------------------------------------------------------------------------------------------------

module test_macro

DEFC  GN_Opf = $6009                    ; open file/stream (or device)
DEFC  Os_Gb  = #39                      ; get byte from file or device
DEFC  constant_name = %1111'0011        ; use binary constant notation with neutral separator symbol

macro xyz ( par1 = 1, par2 = "(string, embedded)", par3 = [(constant_name+1)] )
	; defb \$macargv[3]
    defb \par1
    defm "\par2"
    defb \$macargv[2]
endmacro

macro recursive ( from = 0, to = 5 )
    defb \@, \$[\from], \$h[ \$b[10] + \from], \$b[\from]
	if \to - \from
		recursive \$[\from+1],\to
	else
		macexit
	endif
endmacro

macro dorname (Name)
    defb 'N'
    defb \eon - $-1
    defm "\Name",0
    .\eon
endmacro

macro abc ( par1 = , par2 = 0)
    defb \par1, \par2
    defm "\\string\\",0        ; \\ encodes a single \ for text purposes (not a macro variable)
endmacro

macro resw par1, par2
  defw \par1\Fh, \par2\b
endmacro

macro z88 (syscall)
	if \% > 0 				; only generate code if API Call is specified
		rst 20h             ;
		if \$$ > 255
			defw \syscall 	; OZ high level OS / Generic system call
		else
			defb \syscall 	; OZ low level system call
		endif
	else
		error "Missing OZ API Call"
	endif
endmacro

ld hl,0
z88(GN_Opf)
z88 Os_Gb
; z88 ; this will generate an error!

xyz
xyz ,, 3 	; this is a comment
xyz 4
xyz 2, a new string without quotes, (constant_name+2)

abc (0)
abc 0,1

resw 2, 0101

recursive 1,"3"
recursive 0,5

dorname "PipeDream"
dorname "Diary"

defb $eval[ (((((0+1)+1)+1)+1)+1) ]

; specify PC as $, to allow this syntax to be compatible with other assemblers
defb $ - 1


; ------------------------------------------------------------------------------------------------
; testing of a Z88 MTH Command macro

macro mthcmd (ccode, kbsequence, name, attribute = 0, helppage)
.\cmdstart
        defb \cmdend - \cmdstart+1
        defb \ccode
        defm \kbsequence,0
        defm \name,0                            ; always null-terminate, so offset can be bigger than 8K by default
if \attribute & 0b00010000                      ; Command has help page
  if \$?[4]
        defb \helppage / 256                    ; offset to page from application main help, high byte first (reverse order)
        defb \helppage % 256
  else
    error "Command Help Page Offset not specified"
  endif
endif
        defb \attribute                         ; $00-$1F act as a terminator too
.\cmdend
        defb \cmdend - \cmdstart+1
endmacro

; MTH Command without MTH Help Page
; (cmd.code = $06, cmd.sequence = $1b, command name = $b1 (token))
mthcmd  ( $06, $1b, $b1 )

; MTH Command with MTH Help Page
; Use continuous line, so it´s easier to see macro arguments
mthcmd  (                                       \
            $08,                                \ comments...
            ["CARD"],                           \ comments...
            ["C", $8c, $b2, "Dis", $d9, $fb],   \ comments...
            0b00010000,                         \ comments... comments... comments...
            (AppCmdCardHelp-AppHelp)            \ comments... comments...
        )

; MTH Command with MTH Help Page, but no reference to help page offset (produces assembler error)
;mthcmd  ( $08, ["CARD"], ["C", $8c, $b2, "Dis", $d9, $fb], 0b00010000 )

.AppHelp        defm "Main Application Introduction",0
.AppCmdCardHelp defm "MTH Command Help Page",0
; ------------------------------------------------------------------------------------------------
