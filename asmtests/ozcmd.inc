; **************************************************************************************************
; Shell command test case for OZ v5
; - relocatable command definitions and variable space
;
; Copyright (C) 2022, Gunther Strube, hello@bits4fun.net
; ***************************************************************************************************


defc    EXEC_ORG = 0            ; identify as relocatable executable code.
defc    SIZEOF_Workspace = 0