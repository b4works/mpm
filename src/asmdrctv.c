/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include "config.h"
#include "datastructs.h"
#include "main.h"
#include "options.h"
#include "errors.h"
#include "modules.h"
#include "section.h"
#include "avltree.h"
#include "asmdrctv.h"
#include "symtables.h"
#include "exprprsr.h"
#include "sourcefile.h"
#include "memfile.h"
#include "macros.h"
#include "pass.h"
#include "prsline.h"
#include "i8080.h"
#include "z80.h"
#include "zxn.h"
#include "z180.h"
#include "z380.h"

/******************************************************************************
    Specify API for localtime_r() C2X Ansi C standard
 ******************************************************************************/

extern struct tm *localtime_r (const time_t *__timer, struct tm *__tp);

/******************************************************************************
                          Global variables
 ******************************************************************************/
short clineno = 0;


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/
static int identcmp (const char *idptr, const identfunc_t *symptr);
static int asmfunccmp (const char *idptr, const exprfunc_t *symptr);
static int StreamAsciiString(sourcefile_t *csfile, enum symbols strsym, int *firstchar);
static int StreamStringExpression(sourcefile_t *csfile, int bytepos, const unsigned char *lineptr);
static int DEFSP (sourcefile_t *csfile);
static void AdjustLabelAddresses(size_t OrigPC, size_t PC);
static long EvalLogExpr (sourcefile_t *csfile);
static long Parsedefvarsize (sourcefile_t *csfile, long offset);
static long Parsevarsize (sourcefile_t *csfile);
static void MfGetfilename (sourcefile_t *csfile, char *filename);
static void DeclModuleName (sourcefile_t *csfile, enum symbols sym);
static void DeclMacro (sourcefile_t *csfile);
static void ExitMacro (sourcefile_t *csfile);
static void AlignAddress(const sourcefile_t *csfile, size_t adj);
static void ParseElseEndif(sourcefile_t *csfile, bool interpret);
static void ParseEndif(sourcefile_t *csfile, bool interpret);
static void ASCII(sourcefile_t *csfile, bool nullterminate);
static void ParseDefGroupLine(sourcefile_t *csfile, long *enumconst);
static symbol_t *__AsmEvalExpr (const sourcefile_t *csfile, const void *expr);
static symbol_t *AsmSymLinkAddr (const sourcefile_t *csfile, const void *);
static symbol_t *AsmSymDay (const sourcefile_t *csfile, const void *p);
static symbol_t *AsmSymHour (const sourcefile_t *csfile, const void *p);
static symbol_t *AsmSymMinute (const sourcefile_t *csfile, const void *p);
static symbol_t *AsmSymMonth (const sourcefile_t *csfile, const void *p);
static symbol_t *AsmSymMpmVersion (const sourcefile_t *csfile, const void *);
static symbol_t *AsmSymAssemblerPC (const sourcefile_t *csfile, const void *p);
static symbol_t *AsmSymSecond (const sourcefile_t *csfile, const void *p);
static symbol_t *AsmSymYear (const sourcefile_t *csfile, const void *p);

/* Pre-defined assembler functions, defined here for quick validation with SearchAsmFunction () */
static exprfunc_t asmfunctionlist[] = {
    {"$DAY", AsmSymDay},
    {"$EVAL", __AsmEvalExpr},
    {"$HOUR", AsmSymHour},
    {"$LINKADDR", AsmSymLinkAddr},
    {"$MINUTE", AsmSymMinute},
    {"$MONTH", AsmSymMonth},
    {"$MPMVERSION", AsmSymMpmVersion},
    {"$PC", AsmSymAssemblerPC},
    {"$SECOND", AsmSymSecond},
    {"$YEAR", AsmSymYear}
};

/* ------------------------------------------------------------------------------
   Pre-sorted array of directive mnemonics.
   ------------------------------------------------------------------------------ */
static identfunc_t directives[] = {
    {"ALIGN", ALIGN},
    {"ASCII", DEFM},
    {"ASCIIZ", DEFMZ},
    {"BINARY", BINARY},
    {"BYTE", DEFB},
    {"CPU", CPU},
    {"DB", DEFB},
    {"DC", DEFC},
    {"DEFB", DEFB},
    {"DEFC", DEFC},
    {"DEFGROUP", DEFGROUP},
    {"DEFINE", DefSym},
    {"DEFL", DEFL},
    {"DEFM", DEFM},
    {"DEFMZ", DEFMZ},
    {"DEFP", DEFP},
    {"DEFS", DEFS},
    {"DEFSYM", DefSym},
    {"DEFVARS", DEFVARS},
    {"DEFW", DEFW},
    {"DL", DEFL},
    {"DM", DEFM},
    {"DMZ", DEFM},
    {"DP", DEFP},
    {"DS", DEFS},
    {"DV", DEFVARS},
    {"DW", DEFW},
    {"ELSE", ELSEstat},
    {"END", ExitPass1},
    {"ENDDEF", ENDDEFstat},
    {"ENDIF", ENDIFstat},
    {"ENDM", EndMacro},
    {"ENDMACRO", EndMacro},
    {"ENUM", DEFGROUP},
    {"EQU", EQU},
    {"ERROR", ERROR},
    {"EXITM", ExitMacro},
    {"EXITMACRO", ExitMacro},
    {"EXTERN", DeclExternIdent},
    {"GLOBAL", DeclGlobalIdent},
    {"IF", IFstat},
    {"INCBIN", BINARY},
    {"INCLUDE", IncludeFile},
    {"LIB", DeclLibIdent},
    {"LIBRARY", DeclLibIdent},
    {"LINE", LINE},
    {"LONG", DEFL},
    {"LSTOFF", ListingOff},
    {"LSTON", ListingOn},
    {"MACEND", EndMacro},
    {"MACEXIT", ExitMacro},
    {"MACRO", DeclMacro},
    {"MODULE", DeclModule},
    {"ORG", ORG},
    {"SPACE", DEFS},
    {"STRING", DEFS},
    {"UNDEF", UnDefineSym},
    {"UNDEFINE", UnDefineSym},
    {"VARAREA", DEFVARS},
    {"WORD", DEFW},
    {"XDEF", DeclGlobalIdent},
    {"XLIB", DeclGlobalLibIdent},
    {"XREF", DeclExternIdent}
};


/*!
 * \brief Parse and stream "str" or 'str' sequence to code memory
 * \details
 * Single char string is returned in byRef firstchar.
 * Special single characted ''' returns ', and """ returns "
 * Empty strings are allowed (return length = 0, nothing streamed)
 *
 * \param csfile
 * \param strsym defines either ' or " symbol as string delimeter
 * \param firstchar byRef argument to return single char string
 * \return length of string scanned and streamed to code memory
 */
static int
StreamAsciiString(sourcefile_t *csfile, enum symbols strsym, int *firstchar)
{
    int strch;
    int tmpch;
    int slen = 0;
    int strdelim = separators[strsym];

    while (!MemfEof (csfile->mf)) {
        if ((strch = GetChar(csfile)) == MFEOF) {
            csfile->sym = newline;
            csfile->eol = true;
            ReportError (csfile, Err_Syntax);
            return MFEOF;
        }

        if (strch == strdelim) {
            /* read (temporarily) next char ahead */
            tmpch = MemfGetc(csfile->mf);
            MemfUngetc(csfile->mf);

            /* allow single character wierdo's: ''' and """ */
            if (slen == 0 && strdelim == tmpch) {
                /* dumb statement to fall-through and record single character */
                strch = strdelim;
            } else {
                /* end of string marker */
                break;
            }
        }

        ++slen;
        switch(slen) {
            case 1:
                *firstchar = strch;
                break;
            case 2:
                StreamCodeByte(csfile, (unsigned char) *firstchar);
                StreamCodeByte(csfile, (unsigned char) strch);
                break;
            default:
                StreamCodeByte(csfile, (unsigned char) strch);
        }
    }

    return slen;
}


static long
Parsevarsize (sourcefile_t *csfile)
{
    expression_t *postfixexpr;
    long varsize;
    long size_multiplier;
    long offset = 0;

    if (strcmp (csfile->ident, "DS") != 0) {
        ReportError (csfile, Err_IllegalIdent);
        return 0;
    }

    if ((varsize = DEFSP (csfile)) == -1) {
        ReportError (csfile, Err_UnknownIdent);
        return 0;
    }

    GetSym(csfile);
    if ((postfixexpr = ParseNumExpr (csfile)) != NULL) {
        if (postfixexpr->rangetype & NOTEVALUABLE) {
            ReportError (csfile, Err_SymNotDefined);
            FreeNumExpr (postfixexpr);
        } else {
            size_multiplier = EvalFreeNumExpr (postfixexpr);
            if (size_multiplier > 0) {
                offset = varsize * size_multiplier;
            } else {
                ReportError (csfile, Err_IntegerRange);
            }
        }
    }

    return offset;
}


/*!
 * \brief Return the local system time day
 * \details function argmuments are not used, but specified due to signature declaration
 * \param csfile
 * \param p
 * \return
 */
static symbol_t *
AsmSymDay (const sourcefile_t *csfile, const void *p)
{
    time_t asmtime;
    symbol_t *symptr = FindSymbol ("$DAY", globalsymbols);

    if (csfile != NULL && p == NULL) { /* make function argments useful for compiler */
        time(&asmtime);
#if MSWIN
        struct tm *localtm = localtime(&asmtime);
        if (symptr != NULL)
            symptr->symvalue = localtm->tm_mday;
#else
        struct tm localtm;
        localtime_r(&asmtime, &localtm);
        if (symptr != NULL)
            symptr->symvalue = localtm.tm_mday;
#endif
    }

    return symptr;
}


/*!
 * \brief EVAL directive function wrapper for AsmEvalExpr()
 * \details function argmument csfile is not used, but specified due to signature declaration
 * \param csfile
 * \param p
 * \return
 */
static symbol_t *
__AsmEvalExpr(const sourcefile_t *csfile, const void *expr)
{
    if (csfile != NULL && expr == NULL) { /* make function argments useful for compiler */
        /* no expression specified */
        return NULL;
    } else {
        return AsmEvalExpr("EVAL", 0, expr);
    }
}


/*!
 * \brief Return the local system time hours
 * \details function argmuments are not used, but specified due to signature declaration
 * \param csfile
 * \param p
 * \return
 */
static symbol_t *
AsmSymHour (const sourcefile_t *csfile, const void *p)
{
    time_t asmtime;
    symbol_t *symptr = FindSymbol ("$HOUR", globalsymbols);

    if (csfile != NULL && p == NULL) { /* make function argments useful for compiler */
        time(&asmtime);
#if MSWIN
        struct tm *localtm = localtime(&asmtime);
        if (symptr != NULL)
            symptr->symvalue = localtm->tm_hour;
#else
        struct tm localtm;
        localtime_r(&asmtime, &localtm);
        if (symptr != NULL)
            symptr->symvalue = localtm.tm_hour;
#endif
    }

    return symptr;
}


/*!
 * \brief Return the local system time minutes
 * \details function argmuments are not used, but specified due to signature declaration
 * \param csfile
 * \param p
 * \return
 */
static symbol_t *
AsmSymMinute (const sourcefile_t *csfile, const void *p)
{
    time_t asmtime;
    symbol_t *symptr = FindSymbol ("$MINUTE", globalsymbols);

    if (csfile != NULL && p == NULL) { /* make function argments useful for compiler */
        time(&asmtime);
#if MSWIN
        struct tm *localtm = localtime(&asmtime);
        if (symptr != NULL)
            symptr->symvalue = localtm->tm_min;
#else
        struct tm localtm;
        localtime_r(&asmtime, &localtm);
        if (symptr != NULL)
            symptr->symvalue = localtm.tm_min;
#endif
    }

    return symptr;
}


/*!
 * \brief Return the local system time seconds
 * \details function argmuments are not used, but specified due to signature declaration
 * \param csfile
 * \param p
 * \return
 */
static symbol_t *
AsmSymSecond (const sourcefile_t *csfile, const void *p)
{
    time_t asmtime;

    symbol_t *symptr = FindSymbol ("$SECOND", globalsymbols);

    if (csfile != NULL && p == NULL) { /* make function argments useful for compiler */
        time(&asmtime);
#if MSWIN
        struct tm *localtm = localtime(&asmtime);
        if (symptr != NULL)
            symptr->symvalue = localtm->tm_sec;
#else
        struct tm localtm;
        localtime_r(&asmtime, &localtm);
        if (symptr != NULL)
            symptr->symvalue = localtm.tm_sec;
#endif
    }

    return symptr;
}


/*!
 * \brief Return the local system time month
 * \details function argmuments are not used, but specified due to signature declaration
 * \param csfile
 * \param p
 * \return
 */
static symbol_t *
AsmSymMonth (const sourcefile_t *csfile, const void *p)
{
    time_t asmtime;
    symbol_t *symptr = FindSymbol ("$MONTH", globalsymbols);

    if (csfile != NULL && p == NULL) { /* make function argments useful for compiler */
        time(&asmtime);
#if MSWIN
        struct tm *localtm = localtime(&asmtime);
        if (symptr != NULL)
            symptr->symvalue = localtm->tm_mon+1;
#else
        struct tm localtm;

        localtime_r(&asmtime, &localtm);
        if (symptr != NULL)
            symptr->symvalue = localtm.tm_mon+1;
#endif
    }

    return symptr;
}


/*!
 * \brief Return the current version of Mpm assembler
 * \details function argmuments are not used, but specified due to signature declaration
 * \param csfile
 * \param p
 * \return
 */
static symbol_t *
AsmSymMpmVersion (const sourcefile_t *csfile, const void *p)
{
    symbol_t *symptr = FindSymbol ("$MPMVERSION", globalsymbols);

    if (csfile != NULL && p == NULL) { /* make function argments useful for compiler */
        if (symptr != NULL)
            symptr->symvalue = VERSION_NUMBER;

        return symptr;
    }

    return NULL;
}



/*!
 * \brief Return the local system time year
 * \details function argmuments are not used, but specified due to signature declaration
 * \param csfile
 * \param p
 * \return
 */
static symbol_t *
AsmSymYear (const sourcefile_t *csfile, const void *p)
{
    time_t asmtime;
    symbol_t *symptr = FindSymbol ("$YEAR", globalsymbols);

    if (csfile != NULL && p == NULL) { /* make function argments useful for compiler */
        time(&asmtime);
#if MSWIN
        struct tm *localtm = localtime(&asmtime);
        if (symptr != NULL)
            symptr->symvalue = localtm->tm_year+1900;
#else
        struct tm localtm;
        localtime_r(&asmtime, &localtm);
        if (symptr != NULL)
            symptr->symvalue = localtm.tm_year+1900;
#endif
    }

    return symptr;
}


/*!
 * \brief Return the current assembler program counter symbol
 * \details function argmuments are not used, but specified due to signature declaration
 * \param csfile
 * \param p
 * \return
 */
static symbol_t *
AsmSymAssemblerPC (const sourcefile_t *csfile, const void *p)
{
    symbol_t *symptr = NULL;

    if (csfile != NULL && p == NULL) { /* make function argments useful for compiler */
        symptr = gAsmpcPtr;
    }

    return symptr;
}


static void
DeclModuleName (sourcefile_t *csfile, enum symbols sym)
{
    if (csfile->module->mname == NULL) {
        if (sym == name) {
            if ((csfile->module->mname = strclone(csfile->ident)) == NULL) {
                ReportError (NULL, Err_Memory);
            }
        } else {
            ReportError (csfile, Err_IllegalIdent);
        }
    } else {
        ReportError (csfile, Err_ModNameDefined);
    }
}


static void
AdjustLabelAddresses(size_t OrigPC, size_t NewPC)
{
    symbol_t *lbl;

    while(addresses != NULL) {
        lbl = GetAddress (&addresses);
        if (lbl->symvalue - (symvalue_t) OrigPC == 0) {
            lbl->symvalue = (symvalue_t) NewPC;
        } else {
            break;    /* this label address were smaller than OrigPC */
        }
    }           /* (declared two or more times before previous label definition */
}



/* ---------------------------------------------------------------
   Size specifiers
     DS.B = 8 bit
     DS.W = 16 bit ('Word')
     DS.P = 24 bit ('Pointer')
     DS.L = 32 bit ('Long')
   --------------------------------------------------------------- */
static int
DEFSP (sourcefile_t *csfile)
{
    if (GetSym(csfile) == fullstop && GetSym(csfile) == name) {
        switch (csfile->ident[0]) {
            case 'B':
                return 1;

            case 'W':
                return 2;

            case 'P':
                return 3;

            case 'L':
                return 4;

            default:
                break;
        }
    }

    ReportError (csfile, Err_Syntax);
    return -1;
}


static long
Parsedefvarsize (sourcefile_t *csfile, long offset)
{
    symvalue_t varoffset = 0;

    if (csfile->sym != name) {
        ReportError (csfile, Err_Syntax);
        return 0;
    }

    if (strcmp (csfile->ident, "DS") != 0) {
        DefineSymbol (csfile, csfile->ident, offset, 0);
        GetSym(csfile);
    }
    if (csfile->sym == name) {
        varoffset = Parsevarsize (csfile);
    }

    return varoffset;
}


/* ----------------------------------------------------------------
    Get a filename from current memory file at current read position, onwards.
    The filename is stored / null-terminated in the buffer <filename>,
    truncated at #define MAX_FILENAME_SIZE chars.

    Terminating character of the filename is a space, EOL or a double quote.

    on return, the file pointer is updated to point at the beginning
    of the next line (or EOF if last line was read).
   ---------------------------------------------------------------- */
static void
MfGetfilename (sourcefile_t *csfile, char *filename)
{
    int l = 0;
    int c;

    while (l<=MAX_FILENAME_SIZE && !MemfEof (csfile->mf)) {
        c = GetChar(csfile);

        if (!isspace(c) && c != '\n' && c != MFEOF && c != ':' && c != '"' && c != ';') {
            /* read filename until a space, a comment or a double quote */
            /* swallow '#' (old syntax no longer used), but use all other chars in filename */
            if (c != '#') {
                filename[l++] = (char) c;
            }
        } else {
            break;
        }
    }

    filename[l] = '\0';           /* null-terminate file name string */
    AdjustPlatformFilename(filename);

    MemfUngetc(csfile->mf);             /* evaluate last char by caller... */

    if (GetChar (csfile) != '\n') {
        SkipLine (csfile);    /* skip rest of line and get ready for first char of next line */
    }
}


static int
identcmp (const char *idptr, const identfunc_t *symptr)
{
    return strcmp (idptr, symptr->mnem);
}

static int
asmfunccmp (const char *idptr, const exprfunc_t *symptr)
{
    return strcmp (idptr, symptr->asm_mnem);
}


static long
EvalLogExpr (sourcefile_t *csfile)
{
    expression_t *postfixexpr;
    long constant = 0;

    /* get logical expression */
    GetSym(csfile);
    if ((postfixexpr = ParseNumExpr (csfile)) != NULL) {
        constant = EvalFreeNumExpr (postfixexpr);
    }

    return constant;
}


static void
AlignAddress(const sourcefile_t *csfile, size_t align)
{
    size_t OrigPC;
    size_t adjustment;
    size_t remainder;

    remainder = (size_t) GetStreamCodePC(csfile) % align;
    if (remainder != 0) { /* Is it necessary to align? */
        adjustment = align-remainder;

        OrigPC = (size_t) GetStreamCodePC(csfile);
        while(adjustment--) {
            StreamCodeByte(csfile, 0); /* pad adjustment with null bytes */
        }

        AdjustLabelAddresses(OrigPC, (size_t) GetStreamCodePC(csfile));
    }
}


static symbol_t *
AsmSymLinkAddr (const sourcefile_t *csfile, const void *label)
{
    const symbol_t *labelptr;
    symbol_t *linkaddrptr;
    symvalue_t linkaddr;
    char linkaddrname[MAX_NAME_SIZE];

    if (label == NULL) {
        ReportError ( (sourcefile_t *) csfile, Err_SymNotDefined);
        return NULL;
    } else {
        /* define complete assembler function name as a searchable result */
        snprintf(linkaddrname, MAX_NAME_SIZE-1, "$LINKADDR[%s]", (const char *) label);

        /* was assembler function previously evaluated? */
        linkaddrptr = GetSymPtr ((sourcefile_t *) csfile, linkaddrname);
        if (linkaddrptr != NULL) {
            return linkaddrptr;    /* Mission completed: return previously calculated address (always the same...) */
        }

        labelptr = GetSymPtr ((sourcefile_t *) csfile, label);
        if (labelptr == NULL) {
            ReportError ((sourcefile_t *) csfile, Err_SymNotDefined);
            return NULL;
        }

        if (modules->first->origin == 0xFFFFFFFF) {
            ReportError ((sourcefile_t *) csfile, Err_OrgNotDefined);
            return NULL;
        }

        linkaddr = (symvalue_t) modules->first->origin;   /* first module ORG */
        linkaddr += csfile->module->startoffset;            /* then added with accumulated module codesizes in list */
        linkaddr += (size_t) labelptr->symvalue;            /* finally, add the label offset address from the current module */

        linkaddrptr = DefineSymbol ((sourcefile_t *) csfile, linkaddrname, linkaddr, SYMTOUCHED);
    }

    return linkaddrptr;
}


/* ------------------------------------------------------------------------------
    Exit an executing expanded macro (the current file).

    This identifier function is called and accepted, when inside an executing macro.
    This identifier has no meaning outside a macro definition.
   ------------------------------------------------------------------------------ */
static void
ExitMacro (sourcefile_t *csfile)
{
    if (csfile->macro != NULL) {
        /* a current executing expanded macro requests to abort */
        /* simply signal EOF to the current pass 1 parser to exit the include file */
        csfile->mf->eof = true;
        writeline = false;    /* don't write this line to listing file, if enabled */
    } else {
        ReportSrcAsmMessage (csfile, "Macro body declaration missing");
    }
}


/* ------------------------------------------------------------------------------
    [<name>] MACRO [<name>] [(] [Par1 [= value] [, Par2 [= value]] . .] [)]

    A macro can be defined either as:
        name MACRO <parameters>
        or
        MACRO name <parameters>

    <name> might have already been fetched and is available in global variable *labelname
    This determines which construct is used.

    Parameters are optional and so are the brackets around them.

    A macro sequence can be ended "in the middle" with EXITM, MACEXIT or EXITMACRO

    A macro definition ends with ENDM, MACEND or ENDMACRO
   ------------------------------------------------------------------------------ */
static void
DeclMacro (sourcefile_t *csfile)
{
    char *argv[MAX_STRBUF_SIZE];
    char *argptr;
    const unsigned char *lineptr;
    int  maxargs = 255;
    int  argc = 0;
    char *macroParameter = AllocIdentifier(MAX_NAME_SIZE+1);
    char *macroname = AllocIdentifier(MAX_NAME_SIZE+1);

    if ( (macroParameter == NULL) || (macroname == NULL) ) {
        /* heap memory exhausted! */
        if (macroParameter != NULL) free(macroParameter);
        if (macroname != NULL) free(macroname);
        return;
    }

    if (csfile->labelname != NULL && csfile->labelname->symname != NULL) {
        /* name was put in front of macro definition */
        strncpy(macroname, csfile->labelname->symname, MAX_NAME_SIZE);

        /* get ready to parse optional parameters */
        GetSym(csfile);
    } else {
        if (GetSym(csfile) == name) {
            strncpy(macroname, csfile->ident, MAX_NAME_SIZE);

            /* get ready to parse optional parameters */
            GetSym(csfile);
        } else {
            /* Macro name was expected */
            ReportError (csfile, Err_Syntax);
            free(macroParameter);
            free(macroname);
            return;
        }
    }

    /* re-declaration of macro not allowed */
    if (FindMacro (macroname, csfile->module->macros) != NULL) {
        ReportError (csfile, Err_MacroDefined);
        free(macroParameter);
        free(macroname);
        return;
    }

    /* check that macroname is not using the name of an existing directive or mnemonic */
    if ( (LookupCpuMnemonic (csfile->sym, macroname) != NULL) || (LookupDirective (csfile->sym, macroname) != NULL) ) {
        ReportError (csfile, Err_SymResvName);
        free(macroParameter);
        free(macroname);
        return;
    }

    // eliminate the optional bracket around macro arguments, if specified...
    if (csfile->sym == lparen && !DiscardMacroArgBrackets(csfile) ) {
        free(macroParameter); /* missing right-bracked, error reported, exit... */
        free(macroname);
        return;
    }

    /* Collect arguments */

    while ( (argc <= maxargs) && (csfile->sym != rparen) && (csfile->sym != newline) ) {
        macroParameter[0] = '\0'; /* prepare for next parameter definition */

        if (csfile->sym == name) {
            strncpy(macroParameter, csfile->ident, MAX_NAME_SIZE); /* collect macro parameter name */
        }

        GetSym(csfile);
        switch(csfile->sym) {
            case comma:
                /* the current name definition has been fetched, store it in argument array */
                if (strlen(macroParameter) == 0) {
                    /* no parameter name was specified! */
                    ReportError (csfile, Err_Syntax);
                    FreeMacroNameParameters(argc, argv);
                    free(macroParameter);
                    free(macroname);
                    return;
                }

                if ( (argv[argc] = strclone(macroParameter)) == NULL ) {
                    ReportError (NULL, Err_Memory);
                    FreeMacroNameParameters(argc, argv);
                    free(macroParameter);
                    free(macroname);
                    return;
                } else {
                    argc++;

                    /* fetch next macro name definition */
                    GetSym(csfile);
                }
                break;

            case assign:
                /* read default value string until ",", ")" or end of line */
                argptr = macroParameter + strlen(macroParameter);
                *argptr++ = '=';

                lineptr = MemfTellPtr (csfile->mf);
                switch(GetSym(csfile)) { /* fetch first part of argument value, to check for " and [ */
                    case dquote:
                        fetchMacroArgumentString(csfile, argptr, (int) (MAX_NAME_SIZE - (argptr - macroParameter)), '\"');
                        trim(macroParameter);
                        break;
                    case lexpr:
                        fetchMacroArgumentString(csfile, argptr, (int) (MAX_NAME_SIZE - (argptr - macroParameter)), ']');
                        trim(macroParameter);
                        break;
                    default:
                        MemfSeekPtr(csfile->mf, lineptr);
                        fetchMacroArgument(csfile, argptr, (int) (MAX_NAME_SIZE - (argptr - macroParameter)));
                        trim(macroParameter);
                        break;
                }

                /* the current name definition with default value has been fetched, store it in argument array */
                if ( (argv[argc] = strclone(macroParameter)) == NULL ) {
                    ReportError (NULL, Err_Memory);
                    FreeMacroNameParameters(argc, argv);
                    free(macroParameter);
                    free(macroname);
                    return;
                } else {
                    argc++;

                    /* fetch next macro name definition */
                    GetSym(csfile);
                }
                break;

            case rparen:
            case newline:
                /* final macro parameter definition has been fetched, add it to arguments array */
                if (strlen(macroParameter) == 0) {
                    /* no parameter name was specified! */
                    ReportError (csfile, Err_Syntax);
                    FreeMacroNameParameters(argc, argv);
                    free(macroParameter);
                    free(macroname);
                    return;
                }

                if ( (argv[argc] = strclone(macroParameter)) == NULL ) {
                    ReportError (NULL, Err_Memory);
                    FreeMacroNameParameters(argc, argv);
                    free(macroParameter);
                    free(macroname);
                    return;
                } else {
                    argc++;
                }
                break;

            default:
                /* ignore rest of symbols... */
                break;
        }

        if (csfile->sym == comma) {
            /* fetch next macro name definition */
            GetSym(csfile);
        }

        if ( (csfile->sym != name) && (csfile->sym != rparen) && (csfile->sym != newline) ) {
            ReportError (csfile, Err_Syntax);
            FreeMacroNameParameters(argc, argv);
            free(macroParameter);
            free(macroname);
            return;
        }
    }

    if (csfile->sym != newline)  {
        SkipLine (csfile);    /* ignore rest of line, get ready for first line of macro body */
    }

    /* parse macro body in current source code file and create a macro definition */
    ParseMacro(csfile, macroname, argc, argv);

    free(macroParameter);
    free(macroname);
}


/*!
 * \brief parse lines until ELSE or ENDIF statement, according to interpret status
 * \details Use by Ifstatement()
 * \param csfile
 * \param interpret if set to true, evaluate line, otherwise skip line
 */
static void
ParseElseEndif(sourcefile_t *csfile, bool interpret) {
    do {
        /* interpret lines until ELSE or ENDIF */
        if (!MemfEof (csfile->mf)) {
            writeline = interpret;
            ParseLine (csfile, interpret);
        } else {
            return;    /* end of file - exit from this IF level */
        }
    } while ((csfile->sym != elsestatm) && (csfile->sym != endifstatm));
}


/*!
 * \brief parse lines until ENDIF statement, according to interpret status
 * \details Use by Ifstatement()
 * \param csfile
 * \param interpret if set to true, evaluate line, otherwise skip line
 */
static void
ParseEndif(sourcefile_t *csfile, bool interpret)
{
    do {
        /* scan lines according to interpret until ENDIF */
        if (!MemfEof (csfile->mf)) {
            writeline = interpret;
            ParseLine (csfile, interpret);
        } else {
            return;    /* end of file - exit from this IF level */
        }
    } while (csfile->sym != endifstatm);
}


static int
StreamStringExpression(sourcefile_t *csfile, int bytepos, const unsigned char *lineptr)
{
    int slen;
    int firstchar = 0;

    slen = StreamAsciiString(csfile, csfile->sym, &firstchar);
    if (slen == MFEOF) {
        return MFEOF;
    }

    GetSym(csfile);

    if (slen == 1 && csfile->sym != strconq && csfile->sym != comma && csfile->sym != newline && csfile->sym != semicolon) {
        /* "x"<expr-operator>  or  'x'<expr-operator>  -- parse this as a single char constant expression */
        MemfSeekPtr(csfile->mf, lineptr);
        GetSym(csfile);
        ProcessExpr (csfile, NULL, RANGE_8UNSIGN, bytepos);
    } else if (slen == 1 && (csfile->sym == strconq || csfile->sym == comma || csfile->sym == newline || csfile->sym == semicolon)) {
        /* "x",  or  'x',  or  "x".  or  'x'. */
        StreamCodeByte (csfile, (unsigned char ) firstchar);
    }

    return slen;
}


static void
ASCII(sourcefile_t *csfile, bool nullterminate)
{
    int slen;
    int bytepos = 0;
    const unsigned char *lineptr;

    do {
        lineptr = MemfTellPtr(csfile->mf);
        GetSym(csfile);
        if (csfile->sym == squote || csfile->sym == dquote) {
            slen = StreamStringExpression(csfile, bytepos, lineptr);
            if (slen == MFEOF) {
                /* string constant parsing aborted due to premature EOF */
                return;
            }

            bytepos += slen;

        } else {
            if (!ProcessExpr (csfile, NULL, RANGE_8UNSIGN, bytepos)) {
                /* syntax error in expression - get next line from file... */
                return;
            }

            ++bytepos;
        }

        if (csfile->sym != strconq && csfile->sym != comma && csfile->sym != newline && csfile->sym != semicolon) {
            ReportError (csfile, Err_Syntax);
            return;
        }

    } while ((csfile->sym != newline) && (csfile->sym != semicolon));

    if (nullterminate == true) {
        StreamCodeByte (csfile, 0);
    }
}


static void
ParseDefGroupLine(sourcefile_t *csfile, long *enumconst)
{
    char strconst[MAX_NAME_SIZE+1];
    expression_t *postfixexpr;

    do {
        if (csfile->sym == comma) {
            GetSym(csfile);    /* prepare for next identifier */
        }

        switch (csfile->sym) {
            case rcurly:
            case semicolon:
            case newline:
                break;

            case name:
                strncpy (strconst, csfile->ident, MAX_NAME_SIZE);      /* remember name */

                if (GetSym(csfile) != assign) {
                    DefineSymbol (csfile, strconst, *enumconst, 0);
                    (*enumconst)++;
                    break;
                }

                GetSym(csfile);
                if ((postfixexpr = ParseNumExpr (csfile)) == NULL) {
                    SkipLine (csfile);
                    return; /* abort DEFGROUP parsing, fatal error occurred */
                }

                if (postfixexpr->rangetype & NOTEVALUABLE) {
                    ReportError (csfile, Err_SymNotDefined);
                    FreeNumExpr (postfixexpr);
                    SkipLine (csfile);
                    return;
                } else {
                    *enumconst = EvalFreeNumExpr (postfixexpr);
                    DefineSymbol (csfile, strconst, *enumconst, 0);
                    (*enumconst)++;
                }
                break;

            default:
                ReportError (csfile, Err_Syntax);
                break;
        }
        /* get enum definitions separated by comma in current line */
    } while (csfile->sym == comma);

    SkipLine (csfile);    /* ignore rest of line */
}


/******************************************************************************
                               Public functions
 ******************************************************************************/

/*!
 * \brief LookupIdentifier
 * \param identifier
 * \param st type of symbol
 * \param asmident
 * \param totalid
 * \return
 */
mnemprsrfunc
LookupIdentifier (const char *identifier, enum symbols st, const identfunc_t asmident[], size_t totalid)
{
    const identfunc_t *foundsym;

    if (st == name) {
        foundsym = (identfunc_t *) bsearch (identifier, asmident, totalid, sizeof (identfunc_t), (compfunc_t) identcmp);
        if (foundsym == NULL) {
            return NULL;
        } else {
            return foundsym->mnemfn;
        }
    } else {
        /* all directives are names, therefore nothing would be found anyway... */
        return NULL;
    }
}


symfunc
LookupAsmFunction (const char *fnname)
{
    const exprfunc_t *foundsym;

    foundsym = (exprfunc_t *) bsearch (fnname, asmfunctionlist, sizeof(asmfunctionlist)/sizeof(exprfunc_t), sizeof (exprfunc_t), (compfunc_t) asmfunccmp);
    if (foundsym == NULL) {
        return NULL;
    } else {
        return foundsym->asm_func;
    }
}


mnemprsrfunc
LookupDirective(enum symbols st, const char *id)
{
    return LookupIdentifier (id, st, directives, sizeof(directives)/sizeof(identfunc_t));
}


void
CPU (sourcefile_t *csfile)
{
    /* switch parser & code generator */
    if ( GetSym(csfile) == name ) {
        if (strcmp(csfile->ident, "I8080") == 0) {
            SetCpuAssembler(z80, ParseLineZ80, Lookupi8080Mnemonic);
            return;
        }
        if (strcmp(csfile->ident, "Z80") == 0) {
            SetCpuAssembler(z80, ParseLineZ80, LookupZ80Mnemonic);
            return;
        }

        if (strcmp(csfile->ident, "ZXN") == 0) {
            SetCpuAssembler(zxn, ParseLineZ80, LookupZXNMnemonic);
            return;
        }

        if (strcmp(csfile->ident, "Z180") == 0) {
            SetCpuAssembler(z180, ParseLineZ80, LookupZ180Mnemonic);
            return;
        }
        if (strcmp(csfile->ident, "Z380") == 0) {
            SetCpuAssembler(z380, ParseLineZ80, LookupZ380Mnemonic);
            return;
        }
    }

    ReportError (csfile, Err_IllegalIdent);
}


void
ListingOn (sourcefile_t *csfile)
{
    csfile->sym = newline;          /* silence compiler warning */

    if (writelistingfile == true) {
        uselistingfile = true;       /* switch listing true again... */
        writeline = false;           /* but don't write this line to listing file */
    }

    line[0] = '\0';
}



void
ListingOff (sourcefile_t *csfile)
{
    csfile->sym = newline;                  /* silence compiler warning */

    if (writelistingfile == true) {
        uselistingfile = writeline = false; /* but don't write this line to listing file */
    }

    line[0] = '\0';
}


/*
 * Directive to define an external line number reference,
 * for example a line number in a C source file.
 * This feature is a legacy from the early z88dk C compiler adaption.
 */
void LINE(sourcefile_t *csfile)
{
    char err;

    GetSym(csfile);
    clineno = (short) GetConstant(csfile, &err);
    if (err != 0) {
        clinemode = false;  /* line number argument was not a constant, show the error with original line number */
        ReportError (csfile, Err_Syntax);
    }

    line[0]='\0';
}


/* dummy function - not used but needed for C compiler & program logic */
void
IFstat (sourcefile_t *csfile)
{
    /* Intentionally unimplemented... */
    csfile->sym = ifstatm;
}


void
ELSEstat (sourcefile_t *csfile)
{
    csfile->sym = elsestatm;
    /* but don't write this line in listing file */
    writeline = false;
}


void
ENDIFstat (sourcefile_t *csfile)
{
    csfile->sym = endifstatm;
    /* but don't write this line in listing file */
    writeline = false;
}


void
ENDDEFstat (sourcefile_t *csfile)
{
    /* ENDDEF statement was reached */
    csfile->sym = enddefstatm;
}


/* multilevel conditional assembly logic */
void
Ifstatement (sourcefile_t *csfile, bool interpret)
{
    if (interpret == true) {
        /* evaluate IF expression */
        if (EvalLogExpr (csfile) != 0) {
            /* expression is TRUE, interpret lines until ELSE or ENDIF */
            ParseElseEndif(csfile, true);

            if (csfile->sym == elsestatm) {
                /* then ignore lines until ENDIF ... */
                ParseEndif(csfile, false);
            }
        } else {
            /* expression is FALSE, ignore until ELSE or ENDIF */
            ParseElseEndif(csfile, false);

            if (csfile->sym == elsestatm) {
                ParseEndif(csfile, true);
            }
        }
    } else {
        /* don't evaluate IF expression and ignore all lines until ENDIF */
        ParseEndif(csfile, false);
    }

    csfile->sym = nil;
}


void ExitPass1(sourcefile_t *csfile)
{
    /* stop parsing by signalling EOF to pass 1 loop */
    csfile->mf->eof = true;
}


void ALIGN(sourcefile_t *csfile)
{
    expression_t *postfixexpr;
    long constant;

    /* get numerical expression */
    GetSym(csfile);
    if ((postfixexpr = ParseNumExpr (csfile)) != NULL) {
        if (postfixexpr->rangetype & NOTEVALUABLE) {
            ReportError (csfile, Err_SymNotDefined);
            FreeNumExpr (postfixexpr);
        } else {
            constant = EvalFreeNumExpr (postfixexpr);     /* ALIGN expression must not contain undefined symbols */
            if (constant >= 0 && constant < 17) {
                AlignAddress(csfile, (size_t) constant);    /* Align the codeptr and PC according to value */
            } else {
                ReportError (csfile, Err_ExprOutOfRange);
            }
        }
    }
}


void
DeclGlobalIdent (sourcefile_t *csfile)
{
    do {
        if ( GetSym(csfile) == name) {
            DeclSymGlobal (csfile, csfile->ident, 0);
        } else {
            ReportError (csfile, Err_Syntax);
            return;
        }
    } while (GetSym(csfile) == comma);

    if (csfile->sym != newline) {
        ReportError (csfile, Err_Syntax);
    }
}


void
DeclGlobalLibIdent (sourcefile_t *csfile)
{
    if (GetSym (csfile) == name) {
        DeclModuleName (csfile, csfile->sym);        /* XLIB name is implicit MODULE name */
        DeclSymGlobal (csfile, csfile->ident, SYMDEF);
    } else {
        ReportError (csfile, Err_Syntax);
        return;
    }
}


void
DeclExternIdent (sourcefile_t *csfile)
{
    do {
        if (GetSym(csfile) == name) {
            DeclSymExtern (csfile, csfile->ident, 0);    /* Define symbol as extern */
        } else {
            ReportError (csfile, Err_Syntax);
            return;
        }
    } while (GetSym(csfile) == comma);

    if (csfile->sym != newline) {
        ReportError (csfile, Err_Syntax);
    }
}


void
DeclLibIdent (sourcefile_t *csfile)
{
    do {
        if (GetSym(csfile) == name) {
            DeclSymExtern (csfile, csfile->ident, SYMDEF);    /* Define symbol as extern LIB reference */
        } else {
            ReportError (csfile, Err_Syntax);
            return;
        }
    } while (GetSym(csfile) == comma);

    if (csfile->sym != newline) {
        ReportError (csfile, Err_Syntax);
    }
}


void
DeclModule (sourcefile_t *csfile)
{
    DeclModuleName (csfile, GetSym(csfile));
}


/* ------------------------------------------------------------------------------
    A macro definition is completed with ENDM, MACEND or ENDMACRO

    If this identifier function is called, it means the that this identifier
    has been used before a MACRO start definition.

    A macro definition is parsed through DeclMacro()
   ------------------------------------------------------------------------------ */
void
EndMacro (sourcefile_t *csfile)
{
    ReportSrcAsmMessage (csfile, "Macro body declaration missing");
}


void
DEFVARS (sourcefile_t *csfile)
{
    expression_t *postfixexpr;
    long offset;

    writeline = false;              /* DEFVARS definitions are not output'ed to listing file */
    GetSym(csfile);

    if ((postfixexpr = ParseNumExpr (csfile)) != NULL) {
        /* expr. must not be stored in relocatable file */
        if (postfixexpr->rangetype & NOTEVALUABLE) {
            ReportError (csfile, Err_SymNotDefined);
            FreeNumExpr (postfixexpr);
            return;
        } else {
            offset = EvalFreeNumExpr (postfixexpr);  /* offset expression must not contain undefined symbols */
        }
    } else {
        return;    /* syntax error - get next line from file... */
    }

    GetSym(csfile); /* read first symbol of new line */

    /* skip anything until we meet a name - but also allow for an empty DEFVARS */
    while (!MemfEof (csfile->mf) && csfile->sym != name && csfile->sym != rcurly && (LookupDirective(csfile->sym, csfile->ident) != &ENDDEFstat)) {
        if (csfile->eol == true) {
            ++csfile->lineno;
            csfile->eol = false;
        }

        GetSym(csfile);
    }

    /* found a name definition - parse variable area definition until } or ENDDEF */
    while (!MemfEof (csfile->mf) && csfile->sym != rcurly && (LookupDirective(csfile->sym, csfile->ident) != &ENDDEFstat) ) {
        if (csfile->eol == true) {
            ++csfile->lineno;
            csfile->eol = false;
        } else {
            offset += Parsedefvarsize (csfile, offset);
        }

        GetSym(csfile);
    }
}


void
DEFGROUP (sourcefile_t *csfile)
{
    writeline = false;              /* DEFGROUP definitions are not sent to listing file */
    long enumconst = 0;

    /* skip anything until we meet a name */
    while (!MemfEof (csfile->mf) && (GetSym(csfile)) != name ) {
        SkipLine (csfile);

        ++csfile->lineno;
        csfile->eol = false;
    }

    while (!MemfEof (csfile->mf) && csfile->sym != rcurly && (LookupDirective(csfile->sym, csfile->ident) != &ENDDEFstat) ) {
        if (csfile->eol == true) {
            /* End of line encountered, skip to next line... */
            ++csfile->lineno;
            csfile->eol = false;
            GetSym(csfile);
            continue;
        }

        ParseDefGroupLine(csfile, &enumconst);
    }
    GetSym(csfile);
}


/* DEFS <size> [(<byte>)] */
void
DEFS (sourcefile_t *csfile)
{
    expression_t *sizeexpr;
    expression_t *byteexpr;
    long constant;
    long byte;

    GetSym(csfile); /* point at numerical expression */
    if ((sizeexpr = ParseNumExpr (csfile)) == NULL) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    /* expr. must not be stored in relocatable file */
    if (sizeexpr->rangetype & NOTEVALUABLE) {
        ReportError (csfile, Err_SymNotDefined);
        FreeNumExpr (sizeexpr);     /* remove linked list, expression discarded */
        return;
    }

    constant = EvalFreeNumExpr (sizeexpr);
    if (constant < 0) {
        ReportError (csfile, Err_ExprOutOfRange);
        return;
    }

    byte = 0;     /* use 0 as default padding byte */

    if (csfile->sym == lparen) {
        GetSym(csfile);
        if ((byteexpr = ParseNumExpr (csfile)) == NULL) {
            ReportError (csfile, Err_Syntax);
            return;
        }

        /* expr. must not be stored in relocatable file */
        if (byteexpr->rangetype & NOTEVALUABLE) {
            ReportError (csfile, Err_SymNotDefined);
            FreeNumExpr (byteexpr);     /* remove linked list, expression discarded */
            return;
        }

        byte = EvalFreeNumExpr (byteexpr);
        if (byte < 0 || byte > 255) {
            ReportError (csfile, Err_ExprOutOfRange);
            return;
        }
    }

    while (constant--) {
        StreamCodeByte (csfile, (unsigned char) byte);
    }
}


void
DefSym (sourcefile_t *csfile)
{
    do {
        if (GetSym(csfile) == name) {
            DefineDefSym (csfile, csfile->ident, 1, &csfile->module->localsymbols);
        } else {
            ReportError (csfile, Err_Syntax);
            break;
        }
    } while (GetSym(csfile) == comma);
}


void
UnDefineSym(sourcefile_t *csfile)
{
    symbol_t *foundsym;

    do {
        if (GetSym(csfile) == name) {
            foundsym = FindSymbol(csfile->ident, csfile->module->localsymbols);
            if ( foundsym != NULL ) {
                DeleteNode (&csfile->module->localsymbols, foundsym, (compfunc_t) cmpidstr, (void (*)(void *)) FreeSym);
            }
        } else {
            ReportError (csfile, Err_Syntax);
            break;
        }
    } while (GetSym(csfile) == comma);
}


/* ------------------------------------------------------------------------------
    DEFC <name> = <expression>
   ------------------------------------------------------------------------------ */
void
DEFC (sourcefile_t *csfile)
{
    char strconst[MAX_NAME_SIZE+1];
    expression_t *postfixexpr;
    long constant;

    do {
        if (GetSym(csfile) != name) {
            ReportError (csfile, Err_Syntax);
            return;
        }

        strncpy (strconst, csfile->ident, MAX_NAME_SIZE);  /* remember name */

        if (GetSym(csfile) != assign) {
            ReportError (csfile, Err_Syntax);
            return;
        }

        GetSym(csfile);        /* fetch numerical expression */
        if ((postfixexpr = ParseNumExpr (csfile)) != NULL) {
            /* expression must not be stored in relocatable file */
            if (postfixexpr->rangetype & NOTEVALUABLE) {
                ReportError (csfile, Err_SymNotDefined);
                FreeNumExpr (postfixexpr);
                return;
            } else {
                constant = EvalFreeNumExpr (postfixexpr);    /* DEFC expression must not contain undefined symbols */
                DefineSymbol (csfile, strconst, constant, 0);
            }
        } else {
            return;    /* syntax error - get next line from file... */
        }
    } while (csfile->sym == comma);       /* get all DEFC definition separated by comma */
}



/* ------------------------------------------------------------------------------
    <name> EQU <expression>

    <name> has already been fetched and is available in global variable *labelname
   ------------------------------------------------------------------------------ */
void
EQU (sourcefile_t *csfile)
{
    expression_t *postfixexpr;
    long constant;

    GetSym(csfile);        /* get numerical expression */
    if ((postfixexpr = ParseNumExpr (csfile)) != NULL) {
        /* expression is not be stored in relocatable file and must not contain undefined symbols */
        if (postfixexpr->rangetype & NOTEVALUABLE) {
            ReportError (csfile, Err_SymNotDefined);
            FreeNumExpr (postfixexpr);
        } else {
            constant = EvalFreeNumExpr (postfixexpr);
            if (csfile->labelname != NULL) {
                /* address label is now converted to a constant definition */
                csfile->labelname->symvalue = constant;
                csfile->labelname->type = SYMDEFINED;
                csfile->labelname = GetAddress (&addresses);    /* remove label from address stack */
            } else {
                /* Name was missing in EQU declaration */
                ReportError (csfile, Err_Syntax);
            }
        }
    }
}


void
ORG (sourcefile_t *csfile)
{
    expression_t *postfixexpr;
    unsigned long orgaddr;

    GetSym(csfile);                    /* get numerical expression */

    if ((postfixexpr = ParseNumExpr (csfile)) != NULL) {
        if (postfixexpr->rangetype & NOTEVALUABLE) {
            ReportError (csfile, Err_SymNotDefined);
            FreeNumExpr (postfixexpr);
        } else {
            /* ORG expression must not contain undefined symbols */
            orgaddr = (unsigned long) EvalFreeNumExpr (postfixexpr);
            if ( orgaddr <= 65535 ) {
                csfile->module->origin = orgaddr;
            } else {
                ReportError (csfile, Err_IntegerRange);
            }
        }
    }
}


void
DEFB (sourcefile_t *csfile)
{
    ASCII(csfile, false);
}


void
DEFW (sourcefile_t *csfile)
{
    int bytepos = 0;

    if (addressalign == true) {
        /* make sure that 16bit words are address aligned before actually creating them */
        AlignAddress(csfile, 2);
        /* adjust for automatic address alignment */
        bytepos += (GetStreamCodePC(csfile) - (int) oldPC);
    }

    do {
        GetSym(csfile);
        if (!ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, bytepos)) {
            /* syntax error - get next line from file... */
            return;
        }
        /* DEFW allocated, update listing file patch offset */
        bytepos += 2;

        if (csfile->sym == newline) {
            return;
        } else if (csfile->sym != comma) {
            ReportError (csfile, Err_Syntax);
            return;
        }

        /* get all DEFB definitions separated by comma */
    } while (csfile->sym == comma);
}


/* Z88 specific feature: a 24bit pointer; 16 offset within bank, then bank number */
void
DEFP (sourcefile_t *csfile)
{
    int bytepos = 0;

    do {
        GetSym(csfile);
        if (!ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, bytepos)) {
            /* syntax error - get next line from file... */
            return;
        }
        /* DEFP allocated, update listing file patch offset */
        bytepos += 2;

        if (csfile->sym != comma) {
            ReportError (csfile, Err_Syntax);
            return;
        } else {
            GetSym(csfile);
            if (!ProcessExpr (csfile, NULL, RANGE_8UNSIGN, bytepos)) {
                return;                     /* syntax error - get next line from file... */
            }
            /* Bank number allocated, update listing file patch offset */
            ++bytepos;
        }
    } while (csfile->sym == comma);    /* get all DEFP definitions separated by comma */
}


void
DEFL (sourcefile_t *csfile)
{
    int bytepos = 0;

    if (addressalign == true) {
        /* make sure that 32bit words are address aligned before actually creating them */
        AlignAddress(csfile, 4);
        /* adjust for automatic address alignment */
        bytepos += (GetStreamCodePC(csfile) - (int) oldPC);
    }

    do {
        GetSym(csfile);
        if (!ProcessExpr (csfile, NULL, RANGE_LSB_32CONST, bytepos)) {
            /* syntax error - get next line from file... */
            return;
        }
        /* DEFL allocated, update listing file patch offset */
        bytepos += 4;

        if (csfile->sym == newline) {
            return;
        } else if (csfile->sym != comma) {
            ReportError (csfile, Err_Syntax);
            return;
        }
    } while (csfile->sym == comma);    /* get all DEFB definitions separated by comma */
}


void
DEFM (sourcefile_t *csfile)
{
    ASCII(csfile, false);
}


void
DEFMZ (sourcefile_t *csfile)
{
    ASCII(csfile, true);
}


void
IncludeFile (sourcefile_t *csfile)
{
    const includefile_t *includefile;
    sourcefile_t *newfile;
    char includefilename[MAX_FILENAME_SIZE+1];

    if (GetSym(csfile) != dquote) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    /* fetch filename of include file in current (cached) source file */
    MfGetfilename (csfile, includefilename);

    if ((includefile = CacheIncludeFile(csfile, includefilename)) == NULL) {
        /* file wasn't available on disc, file I/O problems or no memory... */
        return;
    }

    /* INCLUDE file loaded (from disc or fetched from - already included - File Cache) */
    newfile = Newfile (csfile->module, csfile, includefilename);
    /* Allocate new file into file information list (if memory available) */
    if (newfile != NULL) {
        /* focus to new file as the current one to be parsed... */
        csfile = newfile;
        if (verbose) {
            puts (includefilename);  /* display name of INCLUDE file */
        }

        if (newfile->mf != NULL) {
            /* link to cached include file, which later is released independently */
            MemfLinkCachedFile (newfile->mf, includefile->filedata, includefile->filesize);
            newfile->includedfile = true; /* indicate this memory file is allocated via includefile_t */
        }

        SourceFilePass1 (csfile);           /* parse include file */
        csfile = Prevfile (csfile);         /* then get back to previous file... */
    }

    switch (csfile->asmerror) {
        case Err_FileIO:
        case Err_Memory:
            return;                                /* Fatal errors, return immediatly... */
        default:
            break;
    }

    csfile->sym = newline;
    writeline = false;    /* don't write current source line to listing file (empty line of INCLUDE file) */
}


void
ERROR (sourcefile_t *csfile)
{
    char errmsg[MAX_STRBUF_SIZE+1];
    int constant;
    int bytepos = 0;

    if (GetSym(csfile) == dquote) {
        while (!MemfEof (csfile->mf) && bytepos < MAX_STRBUF_SIZE) {
            constant = GetChar(csfile);
            if (constant == MFEOF || constant == '\"') {
                csfile->sym = newline;
                csfile->eol = true;
                break;
            } else {
                errmsg[bytepos++] = (char) constant;
            }
        }

        errmsg[bytepos] = 0;
        ReportSrcAsmMessage (csfile, errmsg);
    } else {
        ReportError (csfile, Err_Syntax);
    }
}


void
BINARY (sourcefile_t *csfile)
{
    FILE *binfile;
    int codesize;

    if (GetSym(csfile) == dquote) {
        MfGetfilename (csfile, csfile->ident); /* get filename from current read position of (cached) source file */

        if ((binfile = fopen (AdjustPlatformFilename(csfile->ident), "rb")) == NULL) {
            ReportIOError (csfile->ident);
            return;
        }

        fseek(binfile, 0L, SEEK_END); /* file pointer to end of file */
        codesize = (int) ftell(binfile);
        fseek(binfile, 0L, SEEK_SET); /* file pointer to start of file */

        if (csfile != NULL && csfile->module != NULL &&
            WriteFileBlock(&csfile->module->csection, binfile, codesize) != codesize) {
            ReportError (csfile, Err_FileIO);
        }

        fclose (binfile);
    } else {
        ReportError (csfile, Err_Syntax);
    }
}


/*!
 * \brief Evaluate expression in specified <expr> string and update result in $EVAL (assembler function) symbol
 * \param name
 * \param lineno
 * \param expr
 * \return NULL, if expression evaluation failed.
 */
symbol_t *
AsmEvalExpr (const char *name, int lineno, const void *expr)
{
    symbol_t *evalFnPtr = FindSymbol ("$EVAL", globalsymbols);
    sourcefile_t *stringFile;
    expression_t *postfixexpr;
    int exprevaluated = 0;
    long constant = 0;

    if (expr == NULL) {
        /* no expression specified */
        return NULL;
    }

    if ( (stringFile = CreateFileFromString(name, lineno, expr)) == NULL) {
        /* couldn't create temp. memory file for inline expression... */
        return NULL;
    } else {
        /* parse expression in string */
        GetSym(stringFile);
        if ((postfixexpr = ParseNumExpr (stringFile)) != NULL) {
            if (postfixexpr->rangetype & NOTEVALUABLE) {
                exprevaluated = 0;
                FreeNumExpr (postfixexpr);
            } else {
                constant = EvalFreeNumExpr (postfixexpr);
                exprevaluated = 1;
            }
        }

        ReleaseFile(stringFile);
    }

    if ( (evalFnPtr != NULL) && (exprevaluated>0)) {
        evalFnPtr->symvalue = constant;
        return evalFnPtr;
    } else {
        return NULL;
    }
}
