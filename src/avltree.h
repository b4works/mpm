/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/

#if ! defined MPM_AVLTREE_HEADER_
#define MPM_AVLTREE_HEADER_

#include "datastructs.h"

void    *Find(const avltree_t *p, const void *key, compfunc_t symcmp);
void    *ReOrder(avltree_t *p, compfunc_t symcmp);
void    DeleteAll(avltree_t **p, void (*deldata)(void *));
void    DeleteNode(avltree_t **root, void *key, compfunc_t comp, void (*delkey)(void *));
void    InOrder(const avltree_t *p, void  (*action)(void *));
void    PreOrder(const avltree_t *p, void  (*action)(void *));
int     Insert(avltree_t **root, void *key, compfunc_t comp);
int     Move(avltree_t **p, avltree_t **newroot, compfunc_t symcmp);
int     Copy(const avltree_t *p, avltree_t **newroot, compfunc_t symcmp, void *(*create)(void *));

#endif
