/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"             /* compilation constant definitions */
#include "datastructs.h"        /* data structure definitions */
#include "avltree.h"            /* fucntions to manage balanced binary trees */
#include "symtables.h"          /* functions to access local/global/extern symbol table data */
#include "modules.h"            /* functions to manage compiled modules */
#include "libraries.h"          /* functions to manage libraries */
#include "z80.h"                /* functions to manage Z80 code auto-relocation */
#include "errors.h"             /* functions for error reporting and error message definitions */
#include "sourcefile.h"         /* functions for source file management, cross-platform filenames */
#include "memfile.h"            /* functions for management memory file */
#include "prsline.h"            /* functions for parsing assembler source code */
#include "pass.h"               /* functions for Pass 1 & 2, listing file output */
#include "options.h"            /* functions to cmd line arguments and display of help */
#include "objfile.h"            /* functions for core object file access */
#include "compile.h"            /* functions for pass1 + pass2 compilation */
#include "main.h"               /* global variable declarations */



/******************************************************************************
                          Global variables
 ******************************************************************************/


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/
static int CheckFileDependencies(const sourcefile_t *csfile, const char *objflnm, const struct stat *ofile);
static int TestAsmFile (const sourcefile_t *cfile, char *objfilename);
static int GetObjFileForModule (module_t *cmodule, char *objflnm);
static int AllocateStaticStructures(void);

static bool compiled;       /* if project files were compiled, then true */


/*!
 * \brief Evaluate the specified dependencies of the current source code file
 * \details
 * Called by TestAsmFile(), to evaluate the specified dependencies of the current
   source code file, by comparing the assembleupdates of the object file with each
   dependency file.
 * \param csfile pointer to current source (assembler) file
 * \param objflnm the name of the object file representing the assembler source file
 * \param ofile pointer to current stat structure that contains file date stamp
 * \return the following status is returned:

        1) return 1, if the dependency source file date stamp > object code file date stamp
           (a dependency is new newer than last compilation of source code)
        2) return 0, if object file is newer (or no dependencies).
        3) return -1 if or if dependency file doesn't exist.
 */
static int
CheckFileDependencies(const sourcefile_t *csfile, const char *objflnm, const struct stat *ofile)
{
    const filelist_t *dependfile;
    struct stat dpstat;

    if (csfile == NULL)
        return 0;

    dependfile = csfile->dependencies; /* get first dependency file in list */
    while(dependfile != NULL) {
        if (StatFile(dependfile->filename, gIncludePath, &dpstat) != -1) {
            /* dependency file status were fetched, is it older than object file?*/
            if (dpstat.st_mtime > ofile->st_mtime)
                /* dependency file is newer than object file, current source code file must be compiled.. */
            {
                if (verbose) {
                    printf ("Dependency '%s' > '%s', compile '%s'.\n", dependfile->filename, objflnm, MemfGetFilename(csfile->mf));
                }
                return 1;
            }
        } else {
            ReportIOError (dependfile->filename);
        }

        dependfile = dependfile->nextfile;
    }

    return 0; /* indicate that object file is newer than dependencies (no compilation needed) */
}


/*!
 * \brief Test whether to comple source code or to load object file
 * \details
   If -d option has been defined at the command line for Mpm, then evaluate whether
   current source code file is newer than object file. The following rules apply:

        1) return 1, if source code file date stamp > object code file date stamp
           (or only source code file exists)
        2) return 0, if object file is newer or if source file doesn't exist
        3) return -1 if source nor object file exist.
 *
 * \param cfile the current source file
 * \param objfilename optional object filename, if object file is to be validated
 * \return
 */
static int
TestAsmFile (const sourcefile_t *cfile, char *objfilename)
{
    struct stat afile;
    struct stat ofile;
    FILE *f;

    if (cfile == NULL)
        return -1;

    if (assembleupdates) {
        /* assemble only updated source files (and dependencies) */
        if (stat (MemfGetFilename(cfile->mf), &afile) == -1) {
            /* if source file not available (strange!), then try to load object file... */
            return GetObjFileForModule (cfile->module, objfilename);
        }

        /* if object file is not available, then compile source */
        if (stat (objfilename, &ofile) != -1 && afile.st_mtime <= ofile.st_mtime) {
            /* object file and source file available, evaluate which is newer... */
            /* source is older than object module, check if source file dependencies is newer than object file*/
            if (cfile->dependencies != NULL && CheckFileDependencies(cfile, objfilename, &ofile) == 1) {
                /* source code needs to be compiled - in all other cases use object file */
                return 1;
            }
            return GetObjFileForModule (cfile->module, objfilename);
        }
    } else {
        /* check Open source file, if no -d validation */
        if ((f = fopen (MemfGetFilename(cfile->mf), "rb")) != NULL) {
            fclose(f);
        } else {
            /* Object module is not found or source has recently been updated */
            ReportIOError (MemfGetFilename(cfile->mf));
            return -1;
        }
    }

    /* assemble if no assemble updates check or if object file not found */
    return 1;
}


/*!
 * \brief Load / cache object file into module and define module name
 * \details
 * Update global variable CODESIZE with size of object code if
 * link2binaryfile option is set.
 * \param cmodule
 * \param objflnm
 * \return always return 0, otherwise EOF if object file failed to load
 */
static int
GetObjFileForModule (module_t *cmodule, char *objflnm)
{
    char modulename[MAX_STRBUF_SIZE+1];
    const char *objwatermark;
    long fptr_modcode;
    long fptr_modname;
    int mnamesize;

    if ((cmodule->objfile = MemObjFileOpen(objflnm, &objwatermark, &cmodule->objlevel)) == NULL) {
        return MFEOF;
    }

    if (cmodule->objlevel > atoi(MPMOBJECTVERSION)) {
        /* object file level is newer than what current Mpm supports, abort */
        ReportRuntimeErrMsg(objflnm, GetErrorMessage(Err_Objectlevel));
        return MFEOF;
    }

    /* fetch ORG and assign to module, always - will be used during linking if this is first module */
    cmodule->origin = MemObjFileReadLong (cmodule->objfile);

    /* get file pointer to module name */
    fptr_modname = MemObjFileReadLong (cmodule->objfile);
    /* set file pointer to module name */
    if (MemfSeek (cmodule->objfile, fptr_modname) == MFEOF) {
        ReportIOError (objflnm);
        return MFEOF;
    }

    if ((mnamesize = MemfGetc (cmodule->objfile)) == MFEOF) {
        ReportIOError (objflnm);
        return MFEOF;
    }

    if (MemfRead (cmodule->objfile, modulename, (size_t) mnamesize) != mnamesize) {
        ReportIOError (objflnm);
        return MFEOF;
    }

    modulename[mnamesize] = '\0'; /* module name successfully read */
    if ((cmodule->mname = strclone(modulename)) == NULL) {
        ReportError (NULL, Err_Memory);
        return MFEOF;
    }

    /* pre-calculate size of linked binary, before actually linking it (needed when adding lib modules) */
    if (link2binaryfile == true) {
        MemfSeek (cmodule->objfile, (long) strlen(objwatermark) + 4+4+4+4+4);   /* set file pointer to point at module code pointer file pointer */
        fptr_modcode = MemObjFileReadLong (cmodule->objfile);                   /* get file pointer to module code */
        if (fptr_modcode != 0xffffffff) {
            MemfSeek (cmodule->objfile, fptr_modcode);                          /* set file pointer to module code */
            CODESIZE += (size_t) MemObjFileReadLong (cmodule->objfile);         /* read 32 bit integer length of module code */
        }
    }

    return 0;  /* indicate that file is not to be compiled, loading data from object file instead */

}


/* ------------------------------------------------------------------------------------------
   Allocate heap memory for static structures
   returns 1, if allocate successfully, otherwise 0
   ------------------------------------------------------------------------------------------ */
static int
AllocateStaticStructures(void)
{
    if (DefineDefSym (NULL, OS_ID, 1, &staticsymbols) == NULL) {
        return 0;
    }
#if MSWIN
    /* also add the "MSDOS" symbol for backward compatibility for legacy assembler projects */
    if (DefineDefSym (NULL, "MSDOS", 1, &staticsymbols) == NULL) {
        return 0;
    }
#endif
    /* get heap memory for text buffers */
    return AllocateTextBuffers();
}


static void
CompileModules (int argc, char *argv[], const char *projfilename)
{
    module_t *cmodule;
    symbol_t *asmfunction;
    filelist_t *moduledependencies = NULL;          /* pointer to list of module source file dependency files */
    char filenameArgument[MAX_FILENAME_SIZE+1];     /* temp variable for command line arguments and filenames */
    char *srcfilename;
    char *objfilename;                              /* an object filename, if OBJ > ASM */
    long projectfileptr = 0;
    int pathsepCount = 0;

    /* Module loop */
    while (true) {
        objfilename = NULL;
        moduledependencies = NULL; /* prepare for new list (old list assigned to previous current file) */

        if (projfilename == NULL) {
            if (argc == 0) {
                /* all arguments parsed */
                return;
            }

            if ((*argv)[0] != '-') {
                strncpy(filenameArgument, *argv, MAX_FILENAME_SIZE);
                --argc;
                ++argv;       /* get ready for next filename */
            } else {
                ReportError (NULL, Err_IllegalSrcfile);    /* Illegal source file name */
                return;
            }
        } else {
            /* get a module filename from project file, and optionally get the dependency files assigned to it... */
            projectfileptr = FetchModuleFilename(projfilename, projectfileptr, filenameArgument, &moduledependencies);
            if (strlen (filenameArgument) == 0) {
                /* failed to open project file or in previous loop iteration, the last filename from project file were fetched... */
                return;
            }
        }

        /* scan fetched filename backwards and truncate extension, if found, but before a pathname separator */
        for (int b=(int) strlen(filenameArgument)-1; b>=0; b--) {
            if (filenameArgument[b] == '\\' || filenameArgument[b] == '/') {
                pathsepCount++;    /* Ups, we've scanned past the short filename */
            }

            if (filenameArgument[b] == '.' && pathsepCount == 0) {
                filenameArgument[b] = '\0'; /* truncate file extension */
                break;
            }
        }

        if ((cmodule = NewModule ()) == NULL) {
            /* Create module data structures for new file */
            ReportError (NULL, Err_Memory);
            return;
        }

        if ((srcfilename = AdjustPlatformFilename(AddFileExtension((const char *) filenameArgument, srcext))) == NULL) {
            /* the most important filename failed to be created, abort mission */
            return;
        }

        /* Set current source file of current module */
        cmodule->cfile = Newfile (cmodule, NULL, srcfilename);
        free(srcfilename); /* release temp. allocated space for current source file name */

        if (cmodule->cfile == NULL) {
            ReportError (NULL, Err_Memory);
            return;
        }

        /* assign file dependencies, if defined in project file (or just NULL) */
        cmodule->cfile->dependencies = moduledependencies;

        if (writelistingfile == true) {
            cmodule->lstfile = CreateLstFile(MemfGetFilename(cmodule->cfile->mf), lstext);
        }

        if (writelistingfile == false && writesymtable == true) {
            /* Create symbol table in separate file (listing file not enabled) */
            cmodule->lstfile = CreateLstFile(MemfGetFilename(cmodule->cfile->mf), symext);
        }

        if (writelistingfile == true || writesymtable == true) {
            InitializeListingFile(cmodule->cfile);
        }

        objfilename = AddFileExtension((const char *) MemfGetFilename(cmodule->cfile->mf), objext);
        switch (TestAsmFile (cmodule->cfile, objfilename)) {
            case 1:
                /* Either assemble updates feature (-d) is turned off or source file is newer than object file... */
                oldPC = 0;

                /* copy all command line DEFINE symbols (-D xxx) into module local symbols */
                Copy (staticsymbols, &cmodule->localsymbols, (compfunc_t) cmpidstr, (void *(*)(void *)) CreateSymNode);

                /* Create standard '$YEAR' assembler function return value */
                if ( (asmfunction = DefineDefSym (cmodule->cfile, "$YEAR", 0, &globalsymbols)) != NULL ) asmfunction->type |= SYMFUNC;

                /* Create standard '$MONTH' assembler function return value  */
                if ( (asmfunction = DefineDefSym (cmodule->cfile, "$MONTH", 0, &globalsymbols)) != NULL ) asmfunction->type |= SYMFUNC;

                /* Create standard '$DAY' assembler function return value  */
                if ( (asmfunction = DefineDefSym (cmodule->cfile, "$DAY", 0, &globalsymbols)) != NULL ) asmfunction->type |= SYMFUNC;

                /* Create standard '$EVAL' assembler function return value  */
                if ( (asmfunction = DefineDefSym (cmodule->cfile, "$EVAL", 0, &globalsymbols)) != NULL ) asmfunction->type |= SYMFUNC;

                /* Create standard '$HOUR' assembler function return value  */
                if ( (asmfunction = DefineDefSym (cmodule->cfile, "$HOUR", 0, &globalsymbols)) != NULL ) asmfunction->type |= SYMFUNC;

                /* Create standard '$MINUTE' assembler function return value  */
                if ( (asmfunction = DefineDefSym (cmodule->cfile, "$MINUTE", 0, &globalsymbols)) != NULL ) asmfunction->type |= SYMFUNC;

                /* Create standard '$SECOND' assembler function return value  */
                if ( (asmfunction = DefineDefSym (cmodule->cfile, "$SECOND", 0, &globalsymbols)) != NULL ) asmfunction->type |= SYMFUNC;

                /* Create standard '$MPMVERSION' assembler function return value  */
                if ( (asmfunction = DefineDefSym (cmodule->cfile, "$MPMVERSION", 0, &globalsymbols)) != NULL ) asmfunction->type |= SYMFUNC;

                /* Create standard '$PC' assembler function and 'ASMPC' identifier in global symbol table */
                CreateAsmPc(cmodule->cfile);

                if (writeobjectfile == true) {
                    cmodule->objfile = MemObjFileCreate(objfilename);
                    FreeIdentifier (&objfilename);
                }

                if (asmerror == true) {
                    /* abort assembly if any initial errors appeared */
                    FreeIdentifier (&objfilename);
                    return;
                }

                compiled = AssembleSourceFile(cmodule);

                if (link2binaryfile == true && writeobjectfile == true && cmodule->objfile != NULL) {
                    /* object file no longer needed in memory, when we're linking - use cached expressions */
                    MemfFree(cmodule->objfile);
                    cmodule->objfile = NULL;
                }

                if (writelistingfile == true || writesymtable == true) {
                    CloseListingFile(cmodule->lstfile);
                    cmodule->lstfile = NULL;
                }

                /* all non-declared symbols can be deleted now */
                DeleteAll (&cmodule->notdeclsymbols, (void (*)(void *)) FreeSym);
                /* copy fetched global symbols to module container (symbols to be restored during linking) */
                ReferenceGlobalSymbols(cmodule);
                /* copy fetched library symbols to module container (used to load external modules during linking) */
                ReferenceLibrarySymbols(cmodule);
                /* then reset global symbol table for next module parsing */
                DeleteAll (&globalsymbols, (void (*)(void *)) FreeSym);

                if (verbose) {
                    /* separate module texts */
                    putchar ('\n');
                }
                break;

            case 0:
                /* if object file is newer (or no dependencies) */
                if (verbose == true) {
                    printf ("OBJ > ASM, use '%s'\n", objfilename);
                }
                break;

            default:
                /* file open error - stop assembler */
                FreeIdentifier (&objfilename);
                return;
        }

        FreeIdentifier (&objfilename);

    } /* module loop */

}


/******************************************************************************
                               Public functions
 ******************************************************************************/



/*!
 * \brief Release various heap memory of dynamically allocated structures
 */
void
FreeAllocatedMemory(void)
{
    FreeTextBuffers();

    DeleteAll (&globalsymbols, (void (*)(void *)) FreeSym);
    DeleteAll (&staticsymbols, (void (*)(void *)) FreeSym);

    if (modules != NULL) {
        ReleaseModules ();          /* Release module information (symbols, etc.) */
    }

    if (cachedincludefiles != NULL) {
        ReleaseCachedIncludeFiles ();
    }

    ReleaseLibraries();             /* Release library information */

    if (z80autorelocate == true) {
        FreeRelocTable();
    }

    ReleasePathInfo();              /* release collected path info (as defined from env. and cmd.line) */
}



/*!
 * \brief Main assembler functionality
 * \param argc count of command line arguments
 * \param argv array of pointers to strings containing command line arguments
 */
void
compile (int argc, char *argv[])
{
    const char *projfilename = NULL;

    FILE *testfile;
    libfilename = NULL;
    globalsymbols = NULL;           /* global identifier tree initialized (none) */
    staticsymbols = NULL;           /* static identifier tree initialized (none) */
    cachedincludefiles = NULL;      /* cached include file tree initialized (none) */

    CODESIZE = 0;
    compiled = false;               /* preset flag to nothing compiled (yet..) */

    if ( !AllocateStaticStructures() ) {
        FreeAllocatedMemory();
        /* Fatal error */
        exit(1);
    }

    /* parse all options and finally project filename (if specified) */
    while (--argc > 0 && asmerror == false) {
        /* Get options first */
        ++argv;

        if ((*argv)[0] == '-') {
            SetAsmFlag (*argv + 1);
        } else {
            if ((*argv)[0] == '@') {
                projfilename = AdjustPlatformFilename( *argv + 1 );
            }
            break;
        }
    }

    if (asmerror == true)
        /* an error happened during option parsing */
        return;

    if (argc == 0 && projfilename == NULL) {
        ReportError (NULL, Err_SrcfileMissing);
        return;
    }

    if (createlibrary == true) {
        /* creating object files is imperative for creating libraries */
        writeobjectfile = true;
    }

    if (assembleupdates == true) {
        /* creating object files is imperative when -d or -a option is used (compile ASM if newer than OBJ file) */
        writeobjectfile = true;
    }

    if (verbose == true) {
        /* display status messages of select assembler options */
        display_options ();
    }

    if (useothersrcext == false) {
        /* use ".asm" as default source file extension */
        strncpy (srcext, asmext, 5);
    }

    /* parse command line arguments and compile all modules derived from it */
    CompileModules(argc, argv, projfilename);

    if (asmerror == true) {
        /* abort any further steps, if errors appeared during assembly of modules */
        return;
    }

    if (compiled == false) {
        if (verbose == true) {
            puts("Nothing compiled - all files are up to date.");
        }

        /* all object files compiled, but is binary available? */
        if ( createlibrary == true && libfilename != NULL) {
            if ((testfile = fopen(libfilename, "r")) != NULL) {
                fclose(testfile);
            } else {
                /* if library file doesn't exist, then it needs to be generated! */
                compiled = true;
            }
        }

        if (link2binaryfile == true && assembleupdates == true && ExistBinFile() == false) {
            /* a previous compilation of this project might not have generated */
            /* the linked executable binary (without -b option) */
            /* so, if the binary doesn't exist, create it now... */
            if (verbose) {
                puts("Executable binary is missing, create it..");
            }
            compiled = true;
        }
    }

    writesymtable = uselistingfile = false;

    if (createlibrary == true) {
        PopulateLibrary (libfilename, modules->first);

        if (asmerror == true) {
            /* error occurred while creating library, remove partial file */
            remove(libfilename);
        }

        if (libfilename != NULL) {
            free (libfilename);
        }
        libfilename = NULL;
    }

    if (writeglobaldeffile == true) {
        WriteGlobalDefFile ();
    }

    if (link2binaryfile == true) {
        /* a module was re-compiled with a newer object file in the project */
        /* or a missing executable binary needs to be generated */
        LinkModules ();
        if (asmerror == true) {
            /* linking failed */
            return;
        }

        /* No errors after linking, write compiled binary, etc */
        if (writeaddrmapfile == true) {
            WriteMapFile ();
        }

        CreateBinFile ();

        if (writeihexfile == true) {
            CreateIntelHexFile();
        }

        if (crc32file == true) {
            CreateCrc32File();
        }

        if (writesha256file == true) {
            CreateSha256File();
        }
    }
}
