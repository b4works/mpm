/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/



/* ----------------------------------------------------------------------------------------- */
/* assembler definitions and constants                                                       */

#define MPM_COPYRIGHTMSG "[M]ultiple [P]rocessor [M]odule Assembler V1.9.14"
#define VERSION_NUMBER 1914

#define DEFAULT_OPTIONS "Default flag options: -Map -njvsadbgAC"

#define ENVNAME_INCLUDEPATH "MPM_INCLUDEPATH"
#define ENVNAME_LIBRARYPATH "MPM_LIBPATH"
#define ENVNAME_STDLIBRARY "MPM_STDLIBRARY"

#define LONESHA256_STATIC

/* Listing file output: generate max X bytes per line, with assembler on same line, else extended on next line */
#define LSTFILE_BYTESLINE 8

/* ----------------------------------------------------------------------------------------- */

#if MSWIN || mswin
#define OS_ID "MSWIN"
#define DIRSEP 0x5C         /* "\" */
#define ENVPATHSEP 0x3B     /* ";" */
#endif

#if UNIX || unix
#define OS_ID "UNIX"
#define DIRSEP 0x2F         /* "/" */
#define ENVPATHSEP 0x3A     /* ":" */
#endif

#define MAX_LINE_BUFFER_SIZE 4096
#define MAX_STRBUF_SIZE 255
#define MAX_FILENAME_SIZE 254
#define MAX_NAME_SIZE 254
#define MAX_EXPR_SIZE 254 /* length of expression in object file max 254 + null = 1 byte */
