/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/

#if ! defined MPM_ERRORS_HEADER_
#define MPM_ERRORS_HEADER_

#include "datastructs.h"    /* base symbol data structures and routines that manages a symbol table */

typedef enum {
    Err_None,                       /* 0,  "No errors detected" */
    Err_FileIO,                     /* 1,  "File open/read error" */
    Err_Syntax,                     /* 2,  "Syntax error" */
    Err_SymNotDefined,              /* 3,  "symbol not defined" */
    Err_Memory,                     /* 4,  "Not enough memory" */
    Err_IntegerRange,               /* 5,  "Integer out of range" */
    Err_ExprSyntax,                 /* 6,  "Syntax error in expression" */
    Err_ExprBracket,                /* 7,  "Right bracket missing" */
    Err_ExprOutOfRange,             /* 8,  "Out of range" */
    Err_SrcfileMissing,             /* 9,  "Source filename missing" */
    Err_IllegalOption,              /* 10, "Illegal or unknown option" */
    Err_UnknownIdent,               /* 11, "Unknown identifier" */
    Err_IllegalIdent,               /* 12, "Illegal label/identifier" */
    Err_SymDefined,                 /* 13, "symbol already defined" */
    Err_ModNameDefined,             /* 14, "Module name already defined" */
    Err_ModNameMissing,             /* 15, "Module name not defined" */
    Err_LibReference,               /* 16, "Library reference not found" */
    Err_SymDeclLocal,               /* 17, "symbol already declared local" */
    Err_SymDeclGlobal,              /* 18, "symbol already declared global" */
    Err_SymDeclExtern,              /* 19, "symbol already declared external" */
    Err_NoArguments,                /* 20, "No command line arguments" */
    Err_IllegalSrcfile,             /* 21, "Illegal source filename" */
    Err_SymDeclGlobalModule,        /* 22, "symbol declared global in another module" */
    Err_SymRedeclaration,           /* 23, "Re-declaration not allowed" */
    Err_OrgDefined,                 /* 24, "ORG already defined" */
    Err_PcRelRange,                 /* 25, "PC-Relative address is out of range" */
    Err_Objectfile,                 /* 26, "Not an Mpm object file" */
    Err_Objectlevel,                /* 27, "Loaded object file level newer than supported by Mpm" */
    Err_SymResvName,                /* 28, "Reserved name" */
    Err_LibfileOpen,                /* 29, "Couldn't open library file" */
    Err_Libfile,                    /* 30, "Not a library file" */
    Err_EnvVariable,                /* 31, "Environment variable not defined" */
    Err_IncludeFile,                /* 32, "Cannot include file recursively" */
    Err_OrgNotDefined,              /* 33, "ORG address not yet defined for project" */
    Err_ExprTooBig,                 /* 34, "Expression > 255 characters" */
    Err_MacroDefined,               /* 35, "Macro has already been defined" */
    Err_UnknownMacroParameter,      /* 36, "Unknown Macro parameter name" */
    Err_DuplicateMacroParameter,    /* 37, "Duplicate Macro parameter name" */
    Err_UnknownMacroFunction,       /* 38, "Unknown Macro Function name" */
    Warn_NoOrgDefined,              /* 39, "Warning: No ORG specified - using 0 as base address" */
    Err_totalMessages
} mpmerror_t;


/* declare global functions */
void ReportWarning (const char *filename, const int lineno, const int warnno);
void ReportWarningMessage (const char *warningmsg);
void ReportSrcAsmMessage  (const sourcefile_t *srcf, const char *message);
void ReportRuntimeErrMsg (const char *filename, const char *message);
void ReportError (sourcefile_t *file, const int errnum);
void ReportIOError (const char *filename);
char *GetErrorMessage(const mpmerror_t mpmerrno);

#endif
