/****************************************************************************************************
 *
 *   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *                       
 *                       IIIIII    888888888888        000000000        888888888888        000000000
 *                        IIII   8888888888888888    0000000000000    8888888888888888    0000000000000
 *                        IIII   8888        8888  0000         0000  8888        8888  0000         0000
 *                        IIII     888888888888    0000         0000    888888888888    0000         0000
 *                        IIII   8888        8888  0000         0000  8888        8888  0000         0000
 *                        IIII   8888888888888888    0000000000000    8888888888888888    0000000000000
 *                       IIIIII    888888888888        000000000        888888888888        000000000
 *
  *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************************************/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "config.h"
#include "datastructs.h"
#include "exprprsr.h"
#include "pass.h"
#include "asmdrctv.h"
#include "errors.h"
#include "prsline.h"
#include "memfile.h"
#include "i8080.h"


/******************************************************************************
                    Local functions and variables
 ******************************************************************************/

static void ADD (sourcefile_t *csfile);
static void DAD (sourcefile_t *csfile);
static void ADI (sourcefile_t *csfile);
static void ACI (sourcefile_t *csfile);
static void ADC (sourcefile_t *csfile);
static void DCR (sourcefile_t *csfile);
static void DCX (sourcefile_t *csfile);
static void IN (sourcefile_t *csfile);
static void INR (sourcefile_t *csfile);
static void INX (sourcefile_t *csfile);
static void MOV (sourcefile_t *csfile);
static void LDA (sourcefile_t *csfile);
static void STA (sourcefile_t *csfile);
static void LDAX (sourcefile_t *csfile);
static void LXI (sourcefile_t *csfile);
static void LHLD (sourcefile_t *csfile);
static void SHLD (sourcefile_t *csfile);
static void STAX (sourcefile_t *csfile);
static void MVI (sourcefile_t *csfile);
static void OUT (sourcefile_t *csfile);
static void SBB (sourcefile_t *csfile);
static void SBI (sourcefile_t *csfile);
static void RST (sourcefile_t *csfile);
static void ANA (sourcefile_t *csfile);
static void ANI (sourcefile_t *csfile);
static void CALL (sourcefile_t *csfile);
static void CNZ (sourcefile_t *csfile);
static void CZ (sourcefile_t *csfile);
static void CNC (sourcefile_t *csfile);
static void CC (sourcefile_t *csfile);
static void CPO (sourcefile_t *csfile);
static void CPE (sourcefile_t *csfile);
static void CP (sourcefile_t *csfile);
static void CM (sourcefile_t *csfile);
static void CMP (sourcefile_t *csfile);
static void CPI (sourcefile_t *csfile);
static void JMP (sourcefile_t *csfile);
static void JNZ (sourcefile_t *csfile);
static void JZ (sourcefile_t *csfile);
static void JNC (sourcefile_t *csfile);
static void JC (sourcefile_t *csfile);
static void JPO (sourcefile_t *csfile);
static void JPE (sourcefile_t *csfile);
static void JP (sourcefile_t *csfile);
static void JM (sourcefile_t *csfile);
static void ORA (sourcefile_t *csfile);
static void ORI (sourcefile_t *csfile);
static void CCF (const sourcefile_t *csfile);
static void CPL (const sourcefile_t *csfile);
static void DAA (const sourcefile_t *csfile);
static void DI (const sourcefile_t *csfile);
static void EI (const sourcefile_t *csfile);
static void HALT (const sourcefile_t *csfile);
static void NOP (const sourcefile_t *csfile);
static void RLA (const sourcefile_t *csfile);
static void RLCA (const sourcefile_t *csfile);
static void RRA (const sourcefile_t *csfile);
static void RRCA (const sourcefile_t *csfile);
static void SCF (const sourcefile_t *csfile);
static void SPHL (const sourcefile_t *csfile);
static void XCHG (const sourcefile_t *csfile);
static void XTHL (const sourcefile_t *csfile);
static void PCHL (const sourcefile_t *csfile);
static void RET (const sourcefile_t *csfile);
static void RNZ (const sourcefile_t *csfile);
static void RZ (const sourcefile_t *csfile);
static void RNC (const sourcefile_t *csfile);
static void RC (const sourcefile_t *csfile);
static void RPO (const sourcefile_t *csfile);
static void RPE (const sourcefile_t *csfile);
static void RP (const sourcefile_t *csfile);
static void RM (const sourcefile_t *csfile);
static void POP (sourcefile_t *csfile);
static void PUSH (sourcefile_t *csfile);
static void SUB (sourcefile_t *csfile);
static void SUI (sourcefile_t *csfile);
static void XRA (sourcefile_t *csfile);
static void XRI (sourcefile_t *csfile);
static void IncDec16bit(sourcefile_t *csfile, int opcode);
static void CallJpCC (sourcefile_t *csfile, int opcodecc, enum i80cc cc);
static void PushPop_instr (sourcefile_t *csfile, int opcode);
static void ArithLog8bitReg (sourcefile_t *csfile, int opcode);
static void ArithLog8bitIm (sourcefile_t *csfile, int opcode);
static enum i80r CheckReg8Mnem(const sourcefile_t *csfile);
static enum i80rr CheckReg16Mnem(const sourcefile_t *csfile);
static void IncDec8bit (sourcefile_t *csfile, int opcode);
static void OpcodeImmediateByte (sourcefile_t *csfile, int opcode);
static void OpcodeImmediateWord (sourcefile_t *csfile, int opcode);

/* ------------------------------------------------------------------------------
   Pre-sorted array of Intel 8080 instruction mnemonics.
   ------------------------------------------------------------------------------ */
static idf_t i8080idents[] = {
    {"ACI", {.mpf = ACI}},
    {"ADC", {.mpf = ADC}},
    {"ADD", {.mpf = ADD}},
    {"ADI", {.mpf = ADI}},
    {"ANA", {.mpf = ANA}},
    {"ANI", {.mpf = ANI}},
    {"CALL", {.mpf = CALL}},
    {"CC",  {.mpf = CC}},
    {"CM",  {.mpf = CM}},
    {"CMA",  {.cmpf = CPL}},
    {"CMC", {.cmpf = CCF}},
    {"CMP", {.mpf = CMP}},
    {"CNC", {.mpf = CNC}},
    {"CNZ", {.mpf = CNZ}},
    {"CP", {.mpf = CP}},
    {"CPE", {.mpf = CPE}},
    {"CPI", {.mpf = CPI}},
    {"CPO", {.mpf = CPO}},
    {"CZ", {.mpf = CZ}},
    {"DAA", {.cmpf = DAA}},
    {"DAD", {.mpf = DAD}},
    {"DCR", {.mpf = DCR}},
    {"DCX", {.mpf = DCX}},
    {"DI", {.cmpf = DI}},
    {"EI", {.cmpf = EI}},
    {"HLT", {.cmpf = HALT}},
    {"IN", {.mpf = IN}},
    {"INR", {.mpf = INR}},
    {"INX", {.mpf = INX}},
    {"JC", {.mpf = JC}},
    {"JM", {.mpf = JM}},
    {"JMP", {.mpf = JMP}},
    {"JNC", {.mpf = JNC}},
    {"JNZ", {.mpf = JNZ}},
    {"JP", {.mpf = JP}},
    {"JPE", {.mpf = JPE}},
    {"JPO", {.mpf = JPO}},
    {"JZ", {.mpf = JZ}},    
    {"LDA", {.mpf = LDA}},
    {"LDAX", {.mpf = LDAX}},
    {"LHLD", {.mpf = LHLD}},
    {"LXI", {.mpf = LXI}},
    {"MOV", {.mpf = MOV}},
    {"MVI", {.mpf = MVI}},
    {"NOP", {.cmpf = NOP}},
    {"ORA", {.mpf = ORA}},
    {"ORI", {.mpf = ORI}},
    {"OUT", {.mpf = OUT}},
    {"PCHL", {.cmpf = PCHL}},
    {"POP", {.mpf = POP}},
    {"PUSH", {.mpf = PUSH}},
    {"RAL", {.cmpf = RLA}},
    {"RAR", {.cmpf = RRA}},
    {"RC", {.cmpf = RC}},
    {"RET", {.cmpf = RET}},
    {"RLC", {.cmpf = RLCA}},
    {"RM", {.cmpf = RM}},
    {"RNC", {.cmpf = RNC}},
    {"RNZ", {.cmpf = RNZ}},
    {"RP", {.cmpf = RP}},
    {"RPE", {.cmpf = RPE}},
    {"RPO", {.cmpf = RPO}},
    {"RRC", {.cmpf = RRCA}},
    {"RST", {.mpf = RST}},
    {"RZ", {.cmpf = RZ}},
    {"SBB", {.mpf = SBB}},
    {"SBI", {.mpf = SBI}},
    {"SHLD", {.mpf = SHLD}},
    {"SPHL", {.cmpf = SPHL}},
    {"STA", {.mpf = STA}},
    {"STAX", {.mpf = STAX}},
    {"STC", {.cmpf = SCF}},
    {"SUB", {.mpf = SUB}},
    {"SUI", {.mpf = SUI}},
    {"XCHG", {.cmpf = XCHG}},
    {"XRA", {.mpf = XRA}},
    {"XRI", {.mpf = XRI}},
    {"XTHL", {.cmpf = XTHL}}
};


/*!
 * \brief validate i8080 8bit register mnemonic
 * \details
 * Via current (source) file identifier, validate 8080 8bit register
    A, B, C, D, E, H, L or M mnemonic
 *
 * \param csfile
 * \return return a recognized enumerated type, or <i80r_unknown>
 */
static enum i80r
CheckReg8Mnem (const sourcefile_t *csfile)
{
    if (csfile->sym != name)
        return i80r_unknown;

    if (csfile->identlen == 1) {
            switch (csfile->ident[0]) {
                case 'A':
                    return i80r_a;
                case 'H':
                    return i80r_h;
                case 'B':
                    return i80r_b;
                case 'L':
                    return i80r_l;
                case 'C':
                    return i80r_c;
                case 'D':
                    return i80r_d;
                case 'E':
                    return i80r_e;
                case 'M':
                    return i80r_m; /* (HL) */
                default:
                    return i80r_unknown;
            }
    }

    return i80r_unknown;
}


/*!
 * \brief validate i8080 16bit register mnemonic
 * \details
 * Via current (source) file identifier, validate i8080 16bit register
    B, D, H, PSW, SP

 * \param csfile
 * \return return a recognized enumerated type, or <i80rr_unknown>
 */
static enum i80rr
CheckReg16Mnem (const sourcefile_t *csfile)
{
    if (csfile->identlen > 3) {
        return i80rr_unknown;
    }

    switch (csfile->ident[0]) {
        case 'H':
            if (csfile->ident[1] == 0) {
                return i80r_hl;
            }
            break;

        case 'B':
            if (csfile->ident[1] == 0) {
                return i80r_bc;
            }
            break;

        case 'D':
            if (csfile->ident[1] == 0) {
                return i80r_de;
            }
            break;

        case 'S':
            if (csfile->ident[1] == 'P') {
                return i80r_sp;
            }
            break;

        default:
            if (strcmp(csfile->ident,"PSW") == 0) {
                return i80r_psw;
            }
    }

    return i80rr_unknown;
}


/*!
 * \brief Stream Opcode and then process immediate 8bit expression
 * \param csfile
 * \param opcode
 */
static void
OpcodeImmediateByte (sourcefile_t *csfile, int opcode)
{
    StreamCodeByte (csfile, (unsigned char) opcode);
    GetSym(csfile);
    ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 1);
}


/*!
 * \brief Stream Opcode and then process immediate 16bit expression
 * \param csfile
 * \param opcode
 */
static void
OpcodeImmediateWord (sourcefile_t *csfile, int opcode)
{
    StreamCodeByte (csfile, (unsigned char) opcode);
    GetSym(csfile);
    ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, 1);
}


/*!
 * \brief Parsing & code generation of 8bit "<instr> [A,]xxx" for ADD, ADC, SBC, SUB, AND, OR, XOR & CP instructions
 * \param csfile
 * \param opcode
 */
static void
ArithLog8bitReg (sourcefile_t *csfile, int opcode)
{
    long srcreg;
    const unsigned char *ptr = MemfTellPtr(csfile->mf);

    if (GetSym(csfile) == name && CheckReg8Mnem (csfile) == i80r_a) {
        GetSym(csfile);
        if (csfile->sym == comma) {
            /* parsed optional "A," */
            ptr = MemfTellPtr(csfile->mf);
        }
    }

    /* optional "A," may have been parsed, otherwise just be after 'XXX' mnemonic */
    MemfSeekPtr(csfile->mf, ptr);
    csfile->sym = nil;
    csfile->eol = false;
    GetSym(csfile);

    /* parse 8bit source register */
    srcreg = CheckReg8Mnem (csfile);
    if (srcreg == i80r_unknown) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }

    /* xxx  A,r */
    srcreg &= 7;
    StreamCodeByte (csfile, (unsigned char) (128 + opcode * 8 + srcreg));
}


/*!
 * \brief Parsing & code generation of 8bit "<instr> [A,] N" for ADD, ADC, SBC, SUB, AND, OR, XOR & CP instructions
 * \param csfile
 * \param opcode
 */
static void
ArithLog8bitIm (sourcefile_t *csfile, int opcode)
{
    const unsigned char *ptr = MemfTellPtr(csfile->mf);

    if (GetSym(csfile) == name && CheckReg8Mnem (csfile) == i80r_a) {
        GetSym(csfile);
        if (csfile->sym == comma) {
            /* parsed optional "A," */
            ptr = MemfTellPtr(csfile->mf);
        }
    }

    /* optional "A," may have been parsed, otherwise just be after 'XXX' mnemonic */
    MemfSeekPtr(csfile->mf, ptr);
    csfile->sym = nil;
    csfile->eol = false;

    /* xxx  A,n */
    OpcodeImmediateByte(csfile, 192 + opcode * 8 + 6);
}


static void
ADD (sourcefile_t *csfile)
{
    ArithLog8bitReg(csfile, 0);
}

/*!
 * \brief 8080: ADI n
 * \details Z80: ADD A,n
 * \param csfile
 */
static void
ADI (sourcefile_t *csfile)
{
    ArithLog8bitIm(csfile, 0);
}


static void
ADC (sourcefile_t *csfile)
{
    ArithLog8bitReg(csfile, 1);
}

/*!
 * \brief 8080: ACI n
 * \details Z80: ADC A,n
 * \param csfile
 */
static void
ACI (sourcefile_t *csfile)
{
    ArithLog8bitIm(csfile, 1);
}


static void
SUB (sourcefile_t *csfile)
{
    ArithLog8bitReg (csfile, 2);
}

/*!
 * \brief 8080: SUI n
 * \details Z80: SUB A,n
 * \param csfile
 */
static void
SUI (sourcefile_t *csfile)
{
    ArithLog8bitIm(csfile, 2);
}


/*!
 * \brief 8080: SBB r
 * \details Z80: SBC A,r|(HL)
 * \param csfile
 */
static void
SBB (sourcefile_t *csfile)
{
    ArithLog8bitReg (csfile, 3);
}

/*!
 * \brief 8080: SBI n
 * \details Z80: SBC A,n
 * \param csfile
 */
static void
SBI (sourcefile_t *csfile)
{
    ArithLog8bitIm (csfile, 3);
}


/*!
 * \brief 8080: ANA r
 * \details Z80: AND A,r|(HL)
 * \param csfile
 */
static void
ANA (sourcefile_t *csfile)
{
    ArithLog8bitReg (csfile, 4);
}

/*!
 * \brief 8080: ANI n
 * \details Z80: AND A,n
 * \param csfile
 */
static void
ANI (sourcefile_t *csfile)
{
    ArithLog8bitIm (csfile, 4);
}


/*!
 * \brief 8080: XRA r
 * \details Z80: XOR A,r|(HL)
 * \param csfile
 */
static void
XRA (sourcefile_t *csfile)
{
    ArithLog8bitReg (csfile, 5);
}

/*!
 * \brief 8080: XRI n
 * \details Z80: XOR A,n
 * \param csfile
 */
static void
XRI (sourcefile_t *csfile)
{
    ArithLog8bitIm (csfile, 5);
}


/*!
 * \brief 8080: ORA r
 * \details Z80: OR A,r|(HL)
 * \param csfile
 */
static void
ORA (sourcefile_t *csfile)
{
    ArithLog8bitReg (csfile, 6);
}

/*!
 * \brief 8080: ORI n
 * \details Z80: OR A,n
 * \param csfile
 */
static void
ORI (sourcefile_t *csfile)
{
    ArithLog8bitIm (csfile, 6);
}


/*!
 * \brief 8080: CMP r
 * \details Z80: CP A,r|(HL)
 * \param csfile
 */
static void
CMP (sourcefile_t *csfile)
{
    ArithLog8bitReg (csfile, 7);
}


/*!
 * \brief 8080: DAD B | D | H | SP
 * \details Z80: ADD HL, BC | DE | HL | SP
 * \param csfile
 */
static void
DAD (sourcefile_t *csfile)
{
    enum i80rr destreg;

    GetSym(csfile);
    switch ( (destreg = CheckReg16Mnem (csfile))) {
        case i80r_bc:
        case i80r_de:
        case i80r_hl:
        case i80r_sp:
            StreamCodeByte (csfile, (unsigned char) (destreg * 16 + 9));
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief 8080: CPI n
 * \details Z80: CP A,n
 * \param csfile
 */
static void
CPI (sourcefile_t *csfile)
{
    ArithLog8bitIm (csfile, 7);
}


/*!
 * \brief 8080: INR/DCR r8 | M
 * \details Z80: INC/DEC r8 | (HL)
 * \param csfile
 * \param opcode
 */
static void
IncDec8bit (sourcefile_t *csfile, int opcode)
{
    enum i80r r8;

    GetSym(csfile);
    if ((r8 = CheckReg8Mnem (csfile)) == i80r_unknown) {
        /* unknown register */
        ReportError (csfile, Err_IllegalIdent);
        return;
    }

    /* INC/DEC r */
    StreamCodeByte (csfile, (unsigned char) (r8 * 8) + (unsigned char) opcode);
}


/*!
 * \brief 8080: INR r8 | M
 * \details Z80: INC r8 | (HL)
 * \param csfile
 * \param opcode
 */
static void
INR (sourcefile_t *csfile)
{
    IncDec8bit(csfile, 4);
}


/*!
 * \brief 8080: DCR r8 | M
 * \details Z80: DCR r8 | (HL)
 * \param csfile
 * \param opcode
 */
static void
DCR (sourcefile_t *csfile)
{
    IncDec8bit(csfile, 5);
}


static void
IncDec16bit(sourcefile_t *csfile, int opcode)
{
    enum i80rr reg16;

    GetSym(csfile);
    switch ((reg16 = CheckReg16Mnem(csfile))) {
        case i80rr_unknown:
        case i80r_psw:
            ReportError (csfile, Err_IllegalIdent);
            break;

        default:
            StreamCodeByte (csfile, (unsigned char) (opcode) + (unsigned char) reg16 * 16);
            break;
    }
}


/*!
 * \brief 8080: INX B | D | H | SP
 * \details Z80: INC BC | DE | HL | SP
 * \param csfile
 * \param opcode
 */
static void
INX (sourcefile_t *csfile)
{
    IncDec16bit(csfile, 3);
}


/*!
 * \brief 8080: DXC B | D | H | SP
 * \details Z80: DEC BC | DE | HL | SP
 * \param csfile
 * \param opcode
 */
static void
DCX (sourcefile_t *csfile)
{
    IncDec16bit(csfile, 0x0B);
}


static void
NOP (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0);
}


/*!
 * \brief 8080: HLT
 * \details Z80: HALT
 * \param csfile
 */
static void
HALT (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 118);
}


/*!
 * \brief 8080: MOV r,r or MOV r,M or MOV M,r
 * \details Z80: LD r,r or LD r,(HL) or LD (HL),r
 * \param csfile
 */
static void
MOV (sourcefile_t *csfile)
{
    enum i80r sourcereg;
    enum i80r destreg;

    GetSym(csfile);
    switch (destreg = CheckReg8Mnem (csfile)) {
        case i80r_m:
            /* MOV M,r  /  LD (HL),r */
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            GetSym(csfile);
            if ((sourcereg = CheckReg8Mnem (csfile)) == i80r_unknown) {
                ReportError (csfile, Err_IllegalIdent);
                return;
            }
        
            StreamCodeByte (csfile, (unsigned char) (112 + sourcereg));
            break;

        case i80r_unknown:
            ReportError (csfile, Err_IllegalIdent);
            break;

        default:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            GetSym(csfile);
            if ((sourcereg = CheckReg8Mnem (csfile)) == i80r_m) {
                /* MOV r,M  /  LD  r,(HL) */
                StreamCodeByte (csfile, (unsigned char) (64 + destreg * 8 + 6));
            } else {
                sourcereg &= 7;
                destreg &= 7;

                /* MOV r,r  /  LD r,r */
                StreamCodeByte (csfile, (unsigned char) (64 + destreg * 8 + sourcereg));
            }
    }
}


/*!
 * \brief 8080: LDA nn
 * \details Z80: LD A,(nn)
 * \param csfile
 */
static void
LDA (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0x3A);
}

/*!
 * \brief 8080: STA nn
 * \details Z80: LD (nn),A
 * \param csfile
 */
static void
STA (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0x32);
}


/*!
 * \brief 8080: LDAX B | D
 * \details Z80: LD A,(BC|DE)
 * \param csfile
 */
static void
LDAX (sourcefile_t *csfile)
{
    GetSym(csfile);
    switch (CheckReg16Mnem (csfile)) {
        case i80r_bc:
            /* LDAX B  or  LD A,(BC) */
            StreamCodeByte (csfile, 10);
            break;

        case i80r_de:
            /* LDAX D  or  LD A,(DE) */
            StreamCodeByte (csfile, 26);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief 8080: LXI B | D | H | SP,nn
 * \details Z80: LD BC|DE|HL|SP,nn
 * \param csfile
 */
static void
LXI (sourcefile_t *csfile)
{
    enum i80rr destreg;

    GetSym(csfile);
    switch ( (destreg = CheckReg16Mnem (csfile))) {
        case i80r_bc:
        case i80r_de:
        case i80r_hl:
        case i80r_sp:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            OpcodeImmediateWord(csfile, (int) (destreg * 16 + 1));
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief 8080: LHLD nn
 * \details Z80: LD HL,(nn)
 * \param csfile
 */
static void
LHLD (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0x2A);
}


/*!
 * \brief 8080: SHLD nn
 * \details Z80: LD (nn),HL
 * \param csfile
 */
static void
SHLD (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0x22);
}


/*!
 * \brief 8080: STAX B | D
 * \details Z80: LD (BC|DE),A
 * \param csfile
 */
static void
STAX (sourcefile_t *csfile)
{
    GetSym(csfile);
    switch (CheckReg16Mnem (csfile)) {
        case i80r_bc:
            /* STAX B  or  LD (BC),A */
            StreamCodeByte (csfile, 0x02);
            break;

        case i80r_de:
            /* STAX D  or  LD (DE),A */
            StreamCodeByte (csfile, 0x12);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief 8080: MVI r,n or MOV M,n
 * \details Z80: LD r,n or LD (HL),r
 * \param csfile
 */
static void
MVI (sourcefile_t *csfile)
{
    enum i80r destreg;

    GetSym(csfile);
    if ((destreg = CheckReg8Mnem (csfile)) == i80r_unknown) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }

    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    /* MVI r,n  |  LD r|(HL),n  */
    destreg &= 7;
    OpcodeImmediateByte(csfile, (int) (destreg * 8 + 6));
}


/*!
 * \brief 8080: IN n
 * \details Z80: IN A,(n)
 * \param csfile
 */
static void
IN (sourcefile_t *csfile)
{
    OpcodeImmediateByte(csfile, 0xDB);
}


/*!
 * \brief 8080: OUT n
 * \details Z80: OUT (n),A
 * \param csfile
 */
static void
OUT (sourcefile_t *csfile)
{
    OpcodeImmediateByte(csfile, 0xD3);
}


/*!
 * \brief 8080: CMA
 * \details Z80: CPL
 * \param csfile
 */
static void
CPL (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 47);
}


/*!
 * \brief 8080: RAL
 * \details Z80: RLA
 * \param csfile
 */
static void
RLA (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 23);
}


/*!
 * \brief 8080: RAR
 * \details Z80: RRA
 * \param csfile
 */
static void
RRA (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 31);
}


/*!
 * \brief 8080: RRC
 * \details Z80: RRCA
 * \param csfile
 */
static void
RRCA (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 15);
}


/*!
 * \brief 8080: RLC
 * \details Z80: RLCA
 * \param csfile
 */
static void
RLCA (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 7);
}


/*!
 * \brief 8080: PUSH/POP B | D | H | PSW
 * \details Z80: PUSH/POP BC | DE | HL | AF
 * \param csfile
 * \param opcode
 */
static void
PushPop_instr (sourcefile_t *csfile, int opcode)
{
    enum i80rr qq;

    if (GetSym(csfile) != name) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    switch ((qq = CheckReg16Mnem (csfile))) {
        case i80r_bc:
        case i80r_de:
        case i80r_hl:
            StreamCodeByte (csfile, (unsigned char) opcode + (unsigned char) qq * 16);
            break;

        case i80r_psw:
            StreamCodeByte (csfile, (unsigned char) (opcode + 48));
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


static void
POP (sourcefile_t *csfile)
{
    PushPop_instr (csfile, 0xC1);
}


static void
PUSH (sourcefile_t *csfile)
{
    PushPop_instr (csfile, 0xC5);
}


static void
RST (sourcefile_t *csfile)
{
    int rstVector;

    GetSym(csfile);
    rstVector = (int) ParseMnemConstant(csfile);

    /* 8080 notation */
    if (rstVector >= 0 && rstVector <= 7) {
        /* RST 0 ... 7 */
        StreamCodeByte (csfile, (unsigned char) (199 + rstVector*8));
        return;
    }

    /* allow Z80 notation */
    if ((rstVector >= 0 && rstVector <= 56) && (rstVector % 8 == 0)) {
        /* RST 00H ... 38H */
        StreamCodeByte (csfile, (unsigned char) (199 + rstVector));
        return;
    }


    ReportError (csfile, Err_IntegerRange);
}


/*!
 * \brief Generate 1st opcode for either CALL cc | JP cc
 * \details 16bit address opcodes is generated by caller
 * \param csfile
 * \param opcodecc
 * \param cc the condition, eg. Z, NZ
 */
static void
CallJpCC (sourcefile_t *csfile, int opcodecc, enum i80cc cc)
{
    StreamCodeByte (csfile, (unsigned char) opcodecc + (unsigned char) cc * 8);
    GetSym(csfile); /* prepare for address expression */
}


static void
CALL (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xCD);
}

/*!
 * \brief 8080: CNZ nn
 * \details Z80: CALL NZ,nn
 * \param csfile
 */
static void
CNZ (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC4 + i80f_nz * 8);
}

/*!
 * \brief 8080: CZ nn
 * \details Z80: CALL Z,nn
 * \param csfile
 */
static void
CZ (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC4 + i80f_z * 8);
}

/*!
 * \brief 8080: CNC nn
 * \details Z80: CALL NC,nn
 * \param csfile
 */
static void
CNC (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC4 + i80f_nc * 8);
}

/*!
 * \brief 8080: CC nn
 * \details Z80: CALL C,nn
 * \param csfile
 */
static void
CC (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC4 + i80f_c * 8);
}

/*!
 * \brief 8080: CPO nn
 * \details Z80: CALL PO,nn
 * \param csfile
 */
static void
CPO (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC4 + i80f_po * 8);
}

/*!
 * \brief 8080: CPE nn
 * \details Z80: CALL PE,nn
 * \param csfile
 */
static void
CPE (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC4 + i80f_pe * 8);
}

/*!
 * \brief 8080: CP nn
 * \details Z80: CALL P,nn
 * \param csfile
 */
static void
CP (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC4 + i80f_p * 8);
}

/*!
 * \brief 8080: CM nn
 * \details Z80: CALL M,nn
 * \param csfile
 */
static void
CM (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC4 + i80f_m * 8);
}


/*!
 * \brief 8080: JMP nn
 * \details Z80: JP nn
 * \param csfile
 */
static void
JMP (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC3);
}

/*!
 * \brief 8080: JNZ nn
 * \details Z80: JP NZ,nn
 * \param csfile
 */
static void
JNZ (sourcefile_t *csfile)
{
    CallJpCC(csfile, 0xC2, i80f_nz);
    ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, 1);
}

/*!
 * \brief 8080: JZ nn
 * \details Z80: JP Z,nn
 * \param csfile
 */
static void
JZ (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC2 + i80f_z * 8);
}

/*!
 * \brief 8080: JNC nn
 * \details Z80: JP NC,nn
 * \param csfile
 */
static void
JNC (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC2 + i80f_nc * 8);
}

/*!
 * \brief 8080: JC nn
 * \details Z80: JP C,nn
 * \param csfile
 */
static void
JC (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC2 + i80f_c * 8);
}

/*!
 * \brief 8080: JPO nn
 * \details Z80: JP PO,nn
 * \param csfile
 */
static void
JPO (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC2 + i80f_po * 8);
}

/*!
 * \brief 8080: JPE nn
 * \details Z80: JP PE,nn
 * \param csfile
 */
static void
JPE (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC2 + i80f_pe * 8);
}

/*!
 * \brief 8080: JP nn
 * \details Z80: JP P,nn
 * \param csfile
 */
static void
JP (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC2 + i80f_p * 8);
}

/*!
 * \brief 8080: JM nn
 * \details Z80: JP M,nn
 * \param csfile
 */
static void
JM (sourcefile_t *csfile)
{
    OpcodeImmediateWord(csfile, 0xC2 + i80f_m * 8);
}

/*!
 * \brief 8080: RNZ
 * \details Z80: RET NZ
 * \param csfile
 */
static void
RNZ (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xC0);
}


/*!
 * \brief 8080: RZ
 * \details Z80: RET Z
 * \param csfile
 */
static void
RZ (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xC8);
}


static void
RET (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xC9);
}


/*!
 * \brief 8080: RNC
 * \details Z80: RET NC
 * \param csfile
 */
static void
RNC (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xD0);
}


/*!
 * \brief 8080: RC
 * \details Z80: RET C
 * \param csfile
 */
static void
RC (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xD8);
}


/*!
 * \brief 8080: RPO
 * \details Z80: RET PO
 * \param csfile
 */
static void
RPO (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xE0);
}


/*!
 * \brief 8080: RPE
 * \details Z80: RET PE
 * \param csfile
 */
static void
RPE (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xE8);
}


/*!
 * \brief 8080: RP
 * \details Z80: RET P
 * \param csfile
 */
static void
RP (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xF0);
}


/*!
 * \brief 8080: RM
 * \details Z80: RET M
 * \param csfile
 */
static void
RM (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xF8);
}


/*!
 * \brief 8080: CMC
 * \details Z80: CCF
 * \param csfile
 */
static void
CCF (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 63);
}


/*!
 * \brief 8080: STC
 * \details Z80: SCF
 * \param csfile
 */
static void
SCF (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 55);
}


static void
DI (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 243);
}


static void
EI (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 251);
}


static void
DAA (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 39);
}


/*!
 * \brief 8080: SPHL
 * \details Z80: LD  SP,HL
 * \param csfile
 */
static void
SPHL (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 249);
}


/*!
 * \brief 8080: XCHG
 * \details Z80: EX DE,HL
 * \param csfile
 */
static void
XCHG (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xEB);
}


/*!
 * \brief 8080: XTHL
 * \details Z80: EX (SP),HL
 * \param csfile
 */
static void
XTHL (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xE3);
}


/*!
 * \brief 8080: PCHL
 * \details Z80: JP (HL)
 * \param csfile
 */
static void
PCHL (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 233);
}


/******************************************************************************
                               Public functions
 ******************************************************************************/


/*!
 * \brief Return pointer to function that parses Intel 8080 mnemonic source line and generates code
 * \param st symbol type
 * \param id pointer to identifier
 * \return pointer to mnemonic parser function
 */
mnemprsrfunc
Lookupi8080Mnemonic(enum symbols st, const char *id)
{
    return LookupIdentifier (id, st, (identfunc_t *) i8080idents, sizeof(i8080idents)/sizeof(idf_t));
}
