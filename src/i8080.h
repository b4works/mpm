/****************************************************************************************************
 *
 *   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM                            
 *                       
 *                       IIIIII    888888888888        000000000        888888888888        000000000
 *                        IIII   8888888888888888    0000000000000    8888888888888888    0000000000000
 *                        IIII   8888        8888  0000         0000  8888        8888  0000         0000
 *                        IIII     888888888888    0000         0000    888888888888    0000         0000
 *                        IIII   8888        8888  0000         0000  8888        8888  0000         0000
 *                        IIII   8888888888888888    0000000000000    8888888888888888    0000000000000
 *                       IIIIII    888888888888        000000000        888888888888        000000000
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************************************/

#if ! defined MPM_IZ80Z80_HEADER_
#define MPM_IZ80Z80_HEADER_

/* globally available functions */
#include "datastructs.h"

/* i8080 8bit register enumerations */
enum i80r  { i80r_b = 0,
             i80r_c = 1,
             i80r_d = 2,
             i80r_e = 3,
             i80r_h = 4,
             i80r_l = 5,
             i80r_m = 6, /* (HL) */
             i80r_a = 7,
             i80r_unknown = 127
           };

/* i8080 16bit register enumerations */
enum i80rr { i80r_bc = 0, i80r_de = 1, i80r_hl = 2, i80r_sp = 3, i80r_psw = 4, i80rr_unknown = 127};

/* i8080 indirect address enumerations */
enum i80ia { i80ia_bc = i80r_bc,
             i80ia_de = i80r_de,
             i80ia_hl = i80r_hl,
             i80ia_nn = 127,
             i80ia_unknown = 128
           };

/* i8080 conditional flag enumerations */
enum i80cc { i80f_nz, i80f_z, i80f_nc, i80f_c, i80f_po, i80f_pe, i80f_p, i80f_m, i80f_unknown};

mnemprsrfunc Lookupi8080Mnemonic(enum symbols st, const char *id);

#endif
