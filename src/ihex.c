/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <stdint.h>
#include "config.h"
#include "datastructs.h"
#include "main.h"
#include "options.h"
#include "modules.h"
#include "memfile.h"
#include "sourcefile.h"
#include "errors.h"


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/


/*
    Write block of data in Intel Hex file format

    format of one line:
        :llaaaattddd…cc\r\n
    where
        :    = each line starts with a colon
        ll   = number of data bytes stored in this line
        aaaa = address of data
        tt   = type of line: 0 -> data, 1 -> oef, 4 -> upper 16 bit of address (if > 64k)
        ddd… = data
        cc   = 2's complement of checksum of ll, aaaa, tt and data
        \r\n = dos line end

    --------------------------------------------------------------------------------

    Copyright  (c)  Günter Woigk 2014 - 2014
                    mailto:kio@little-bat.de

    write_intel_hex() is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

    Permission to use, copy, modify, distribute, and sell this software and
    its documentation for any purpose is hereby granted without fee, provided
    that the above copyright notice appear in all copies and that both that
    copyright notice and this permission notice appear in supporting
    documentation, and that the name of the copyright holder not be used
    in advertising or publicity pertaining to distribution of the software
    without specific, written prior permission.  The copyright holder makes no
    representations about the suitability of this software for any purpose.
    It is provided "as is" without express or implied warranty.

    THE COPYRIGHT HOLDER DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
    INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
    EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY SPECIAL, INDIRECT OR
    CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
    DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
    TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

    --------------------------------------------------------------------------------

    Gunther Strube, mailto:hello@bits4fun.net, December 2014:
    Original implementation, http://k1.spdns.de/Develop/Projects/zasm/Source/helpers.cpp

    Changed implementation to FILE* API and fprintf().
    Comments and integer definition changed to use C99 standard.
*/

static void write_intel_hex(FILE *ihexfile, uint32_t addr, uint8_t *bu, uint32_t sz )
{
    uint16_t n;
    uint8_t checksum;

    if(sz==0) return;

    /* do we cross a 16 bit boundary? */

    while( (addr>>16) != ((addr+sz-1)>>16) )
    {
        n = (uint16_t) 0x10000;         /* bytes left in current 16-bit address block */

        write_intel_hex(ihexfile, addr, bu, n);

        addr += n;
        bu   += n;
        sz   -= n;
    }

    /*
        do we need an extended address block?
        note: we do not test for addr&0xFFFF==0 as well because
        caller might have skipped some bytes at start of block.
        therefore this block may be written unneccessarily multiple times.
    */

    if( addr>>16 )
    {
        checksum = - 2 - 0 - 0 - 4 - (addr>>24) - (addr>>16);
        fprintf (ihexfile, ":02000004%04X%02X\r\n", (uint16_t)(addr>>16), checksum);
    }

    /* store data: */

    while( sz )
    {
        n = (sz < 32U) ? sz: 32;                                /* bytes dumped in this line */

        fprintf (ihexfile, ":%02X%04X00", n, (uint16_t)(addr)); /* ":", len, address, type */
        checksum = - n - addr - (addr>>8) - 0;          /* checksum */

        addr += n;
        sz   -= n;

        while(n--)                                              /* write data bytes */
        {
            fprintf (ihexfile, "%02X", *bu);
            checksum -= *bu++;
        }

        fprintf (ihexfile, "%02X\r\n", checksum);      /* write 2's complement of checksum */
    }
}



/******************************************************************************
                               Public functions
 ******************************************************************************/

char *CreateHexfilename(void)
{
    char *tmpstr = NULL;

    if (expl_binflnm == true) {
        /* create intel hex output filename using predefined filename from command line for generated binary, */
        tmpstr = AddFileExtension( (const char *) binfilename, ".ihx");
    } else {
        /* create intel hex output filename, based on project filename */
        if (modules != NULL && modules->first != NULL && modules->first->cfile != NULL)
            tmpstr = AddFileExtension( MemfGetFilename(modules->first->cfile->mf), ".ihx");
    }

    return tmpstr;
}


void WriteHexFile(char *filename, unsigned char *codebase, size_t length)
{
    FILE *ihexfile;

    ihexfile = fopen (AdjustPlatformFilename(filename), "w");    /* text output to xxxxx.hex */
    if (ihexfile != NULL) {
        write_intel_hex(ihexfile, 0, codebase, (uint32_t) length);
        fprintf (ihexfile, ":00000001FF\r\n");
        fclose (ihexfile);
    } else {
        ReportIOError (filename);
    }
}
