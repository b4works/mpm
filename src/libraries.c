/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#include "datastructs.h"
#include "avltree.h"
#include "main.h"
#include "options.h"
#include "symtables.h"
#include "objfile.h"
#include "libraries.h"
#include "modules.h"
#include "errors.h"
#include "memfile.h"
#include "sourcefile.h"
#include "pass.h"
#include "prsline.h"


/******************************************************************************
                          Global variables
 ******************************************************************************/
libraries_t *libraries = NULL;


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/
static int cmpmodname (const char *modname, const libobject_t *p);
static int cmplibnames (const libobject_t *kptr, const libobject_t *p);
static int LinkLibModule (const libfile_t *library, const long module_basefptr, const char *modname);
static void LoadLibraryIndex(avltree_t **libindex, libfile_t *curlib);
static void ReleaseIndexObj(libobject_t *libidxobj);
static void ReleaseLibrariesIndex (avltree_t **librarytree);
static bool ValidateLibraryFile(char *filename, const pathlist_t *pathlist, const char **libversion);
static libobject_t *FindLibModule (const char *modname);
static libobject_t *AllocLibObject (void);
static libfile_t *NewLibrary (libraries_t **liblist);
static libfile_t *AllocLibFile (void);
static libraries_t *AllocLibHdr (void);
static memfile_t *MemLibFileCreate(char *libfname);

static avltree_t *libraryindex = NULL;



/**
 * @brief LinkLibModule
 * @param library
 * @param module_basefptr
 * @param modname
 * @return  0, successfully linked this LIB module to the application modules list.
            Err_Memory, if memory was not available for allocation of this LIB module object.
            Err_MaxCodeSize, if code buffer max was reached during linking of this LIB module.
 */
static int
LinkLibModule (const libfile_t *library, const long module_basefptr, const char *modname)
{
    int link_error;
    char *mname;
    module_t *libmodule = NewModule ();

    if (libmodule != NULL) {
        /* create new module for library */
        mname = strclone(modname);   /* make a Copy of module name */
        if (mname != NULL) {
            libmodule->mname = mname;
            /* the library file */
            libmodule->cfile = Newfile (libmodule, NULL, library->libfilename);

            if (verbose) {
                printf ("Linking library module <%s>\n", modname);
            }

            /* link library module & read names */
            link_error = LinkObjfileModule (libmodule, library->mf, module_basefptr);
        } else {
            ReportError (NULL, Err_Memory);
            link_error = Err_Memory;
        }
    } else {
        ReportError (NULL, Err_Memory);
        link_error = Err_Memory;
    }

    return link_error;
}


static int
cmpmodname (const char *modname, const libobject_t *p)
{
    return strcmp (modname, p->libname);
}


static libobject_t *
FindLibModule (const char *modname)    /* pointer to module name in object file */
{
    if (libraryindex == NULL) {
        return NULL;
    } else {
        return Find (libraryindex, modname, (compfunc_t) cmpmodname);
    }
}


static int
cmplibnames (const libobject_t *kptr, const libobject_t *p)
{
    return strcmp (kptr->libname, p->libname);
}


static libobject_t *
AddLibIndexObj(avltree_t **libindex, libfile_t *curlib, const char *modname, long objfile_baseptr)
{
    libobject_t *newlibobj = AllocLibObject();

    if (newlibobj != NULL) {
        newlibobj->libname = strclone(modname);  /* Allocate area for a new module name */
        if (newlibobj->libname == NULL) {
            ReleaseIndexObj(newlibobj);
            newlibobj = NULL;
        } else {
            newlibobj->library = curlib;                /* reference to library file containing library module */
            newlibobj->modulestart = objfile_baseptr;   /* file pointer to base of linkable object in library file */

            /* Insert new library index object into LibraryIndex AVL tree */
            if (Insert (libindex, newlibobj, (compfunc_t) cmplibnames) == 0) {
                ReleaseIndexObj(newlibobj);
                newlibobj = NULL;
            }
        }
    }

    if (newlibobj == NULL) {
        ReportError (NULL, Err_Memory);
    }

    return newlibobj;
}


static void
LoadLibraryIndex(avltree_t **libindex, libfile_t *curlib)
{
    long currentlibmodule;
    long nextlibmodule;
    long modulesize;
    long fptr_mname;
    long objfile_base;
    char mname[MAX_NAME_SIZE];
    libobject_t *foundlib;

    if (curlib->mf == NULL) {
        /* no cached library file! */
        return;
    }

    /* offset pointer into first available module in library */
    currentlibmodule = (long) strlen(curlib->libwatermark);

    do {
        /* scan all available active modules in library file */
        do {
            MemfSeek(curlib->mf, currentlibmodule);             /* point at beginning of a object file module block */
            nextlibmodule = MemObjFileReadLong (curlib->mf);    /* get file pointer to next module in library */
            modulesize = MemObjFileReadLong (curlib->mf);       /* get size of current module */
        } while (modulesize == 0 && nextlibmodule != 0xffffffff);

        if (modulesize != 0) {
            /* point at module name file pointer            [ object file  ....................... ]
             * (past <Next Object File> & <Object File Length> & <Object file Watermark> & <ORG address>)
             */
            objfile_base = currentlibmodule + 4 + 4;
            MemfSeek(curlib->mf, objfile_base + (int) strlen(curlib->libwatermark) + 4);

            fptr_mname = MemObjFileReadLong (curlib->mf);       /* get module name file pointer */
            MemfSeek(curlib->mf, objfile_base + fptr_mname);    /* point at module name */
            MemObjFileReadString (curlib->mf, mname);           /* read module name into buffer variable */

            /* check for lib module in library index */
            if ((foundlib = FindLibModule(mname)) == NULL) {
                /* This Library module was not found in the index, Insert library module definition */
                AddLibIndexObj(libindex, curlib, mname, objfile_base);
            } else {
                /* a library module has already been loaded into the index */
                /* issue a warning message and replace existing library module info with new info */
                printf ("Warning: Duplicate objects not allowed in Library Index!\n"
                        "Object '%s' from library file '%s'\nwill be replaced with object from file '%s'\n",
                        foundlib->libname, foundlib->library->libfilename, curlib->libfilename);

                /* module name is the same, but other attributes need to be replaced */
                foundlib->library = curlib;             /* point to new library file */
                foundlib->modulestart = objfile_base;   /* file pointer to library object in new library file */
            }
        }

        currentlibmodule = nextlibmodule;
    } while (nextlibmodule != 0xffffffff && asmerror == false); /* scan all available active modules in library file */
}


static void
ReleaseIndexObj(libobject_t *libidxobj)
{
    if (libidxobj == NULL) {
        return;
    }

    if (libidxobj->libname != NULL) {  /* release allocated space for library module name */
        free(libidxobj->libname);      /* library file information is released in ReleaseLibraries() */
    }

    free(libidxobj);
}


static void
ReleaseLibrariesIndex (avltree_t **librarytree)
{
    DeleteAll (librarytree, (void (*)(void *)) ReleaseIndexObj);
    *librarytree = NULL;
}


/*!
 * \brief Allocate, initialize and append new library to linked list of libraries
 * \details The linked list is managed automatically
 * \param liblist byref pointer to first library in list (or NULL if list is empty)
 * \return pointer to new (initialized) library
 */
static libfile_t *
NewLibrary (libraries_t **liblist)
{
    libfile_t *newl;

    if ( *liblist == NULL ) {
        *liblist = AllocLibHdr ();
        if (*liblist == NULL) {
            return NULL;
        }
    }

    if ((newl = AllocLibFile ()) == NULL) {
        return NULL;
    }

    if ((*liblist)->firstlib == NULL) {
        (*liblist)->firstlib = newl;
        (*liblist)->currlib = newl;             /* First library added to list */
    } else {
        (*liblist)->currlib->nextlib = newl;    /* current/last library points now at new current */
        (*liblist)->currlib = newl;             /* pointer to current module updated */
    }

    return newl;
}


static libraries_t *
AllocLibHdr (void)
{
    libraries_t *lb = (libraries_t *) malloc (sizeof (libraries_t));

    if (lb != NULL) {
        lb->firstlib = NULL;
        lb->currlib = NULL;   /* library header initialised */
    }

    return lb;
}


static libfile_t *
AllocLibFile (void)
{
    libfile_t *lf = (libfile_t *) malloc (sizeof (libfile_t));

    if (lf != NULL) {
        lf->nextlib = NULL;
        lf->libfilename = NULL;
        lf->libwatermark = NULL;
        lf->mf = NULL;
    }

    return lf;
}


static libobject_t *
AllocLibObject (void)
{
    libobject_t *lo = (libobject_t *) malloc (sizeof (libobject_t));

    if (lo != NULL) {
        lo->libname = NULL;           /* name of library module (the LIB reference name) */
        lo->library = NULL;           /* pointer to library file information */
        lo->modulestart = 0;          /* base pointer of beginning of object module inside library file */
    }

    return lo;
}


/*!
 * \brief Create (memory) library template with host filename, preset with library file watermark
 * \details As library generation progresses, the library (memory) file is streamed with contents
 * \param objfname
 * \return pointer to memory library file (or NULL allocation failed)
 */
static memfile_t *
MemLibFileCreate(char *libfname)
{
    /* library file output is with initial 4K space, incremented by 2K block */
    memfile_t *mlibf = MemfNew (AdjustPlatformFilename(libfname), 4096, 2048);

    if (mlibf == NULL) {
        ReportRuntimeErrMsg (NULL, "Failed to create library memory file");
    } else {
        /* Create relocatable library Mpm header watermark */
        MemfWrite(mlibf, MPMLIBRARYHEADER, SIZEOF_MPMLIBHDR);
    }

    return mlibf;
}


/*!
 * \brief Open the specified file and evaluate that it is an Mpm generated library file
 * \details
 *  If successfully validated, true is returned to caller
 *  and a pointer to the library file watermark type string is returned.
 *
 *  If the library file couldn't be opened or is not recognized,
 *  false and NULL watermark is returned.
 *  The routine also reports errors to the global error system for file I/O and
 *  unrecognized library file.
 * \param filename
 * \param pathlist
 * \param libversion
 * \return
 */
static bool
ValidateLibraryFile(char *filename, const pathlist_t *pathlist, const char **libversion)
{
    char watermark[64];
    *libversion = NULL;
    FILE *libf = OpenFile (filename, pathlist, false);

    if (libf == NULL) {
        ReportIOError (filename);
        return false;
    } else {
        if (fread (watermark, SIZEOF_MPMLIBHDR, 1U, libf) == 1U) {  /* try to read Mpm library file watermark */
            watermark[SIZEOF_MPMLIBHDR] = '\0';

            if (strcmp (watermark, MPMLIBRARYHEADER) == 0) {
                *libversion = MPMLIBRARYHEADER;                /* found Mpm library file */
            } else {
                /* library file was not recognized */
                ReportRuntimeErrMsg (filename, GetErrorMessage(Err_Libfile));
            }
        }
    }

    fclose (libf);
    return (libversion != NULL) ? true: false;
}


/******************************************************************************
                               Public functions
 ******************************************************************************/


/*!
 * \brief Create a library file (-x option), containing concatanated object file modules.
 * \details
 *  The current linked list of modules is scanned and for each module
 *  the object file (already in memory) is appended to the library file.
 * \param libraryfilename
 * \param curmodule
 */
void
PopulateLibrary (char *libraryfilename, const module_t *curmodule)
{
    memfile_t *mobjf;
    memfile_t *mlibfile;

    /* create library (memory) file... */
    if ((mlibfile = MemLibFileCreate (libraryfilename)) == NULL) {
        return;
    }

    if (verbose) {
        puts ("Creating library...");
    }

    do {
        if ((mobjf = curmodule->objfile) != NULL) {
            if (verbose) {
                printf ("<%s> module at %04lX.\n", mobjf->fname, MemfTell (mlibfile));
            }

            if (curmodule->nextmodule == NULL) {
                MemObjFileWriteLong (mlibfile, -1);    /* this is the last module, 0xffffffff */
            } else {
                MemObjFileWriteLong (mlibfile, MemfTell(mlibfile) + 4 + 4 + mobjf->filesize);  /* file pointer to next module */
            }

            /* size of this module */
            MemObjFileWriteLong (mlibfile, mobjf->filesize);
            /* append module to library file */
            MemfWrite(mlibfile, (const char *) mobjf->filedata, (size_t) mobjf->filesize);
        }

        curmodule = curmodule->nextmodule;
    } while (curmodule != NULL);

    MemObjFileFlush(mlibfile); /* flush memory library file to host filing system */
    MemfFree(mlibfile);
}


char *
CreateLibfileName (const char *filename)
{
    char *libraryfilename = NULL;

    if (strlen (filename) > 0) {
        libraryfilename = AddFileExtension(filename, libext);
    } else {
        if ((filename = getenv (ENVNAME_STDLIBRARY)) != NULL) {
            libraryfilename = AddFileExtension(filename, libext);
        }
    }

    if (libraryfilename == NULL) {
        ReportRuntimeErrMsg (NULL, "Failed to create library filename - insufficient memory");
    }

    return libraryfilename;
}


/*!
 * \brief The -l option will validate the library for correctness and register it in the linked list of libraries
 * \details
 * Once the library was successfully validated, it is cached as a memory file, and the global flag uselibraries
 * is set to true when the specificed library were registered.
 * \param filename the filename of the library to register
 */
void
RegisterLibfile (const char *filename)
{
    const char *libwatermark;
    libfile_t *newlib;
    memfile_t *lmf = NULL;
    char *f = NULL;

    if (filename != NULL && strlen (filename) > 0) {
        f = AddFileExtension(filename, libext);
        if (f == NULL) {
            return;
        }
    } else {
        filename = getenv (ENVNAME_STDLIBRARY);
        if (filename != NULL) {
            f = AddFileExtension(filename, libext);
            if (f == NULL) {
                return;
            }
        }
    }

    /* Does library file exist, and does it contain the correct watermark? */
    if (ValidateLibraryFile(f, gLibraryPath, &libwatermark) == false) {
        free(f); /* discard previously allocated library filename */
        return;
    }

    /* load library file into memory */
    if ((lmf = CacheObjFile(f, gLibraryPath)) == NULL) {
        free(f);    /* discard previously allocated library filename */
        return;     /* library file failed to be cached in memory */
    }

    /* Library file has been recognised, append it to linked list of libraries */
    if ((newlib = NewLibrary (&libraries)) != NULL) {
        newlib->libfilename = f;
        newlib->libwatermark = libwatermark;
        newlib->mf = lmf;
        uselibraries = true;
    } else {
        ReportError (NULL, Err_Memory);
        free(f); /* discard previously allocated library filename */
    }
}


void
IndexLibraries(const libraries_t *libs)
{
    libfile_t *curlib;

    if (libs != NULL) {
        curlib = libs->firstlib;

        if (verbose) {
            puts ("Indexing libraries...");
        }

        do {
            LoadLibraryIndex(&libraryindex, curlib);
            curlib = curlib->nextlib;
        } while(curlib != NULL);     /* until all library files are parsed */
    }
}


void
ReleaseLibraries (void)
{
    libfile_t *tmpptr;
    libfile_t *curptr;

    if (libraries == NULL) {
        return;
    }

    curptr = libraries->firstlib;
    /* until all libraries are released */
    while (curptr != NULL) {
        if (curptr->libfilename != NULL) {
            free (curptr->libfilename);
        }
        MemfFree(curptr->mf);       /* release cache memory file of library */

        tmpptr = curptr;
        curptr = curptr->nextlib;
        free (tmpptr);              /* release library */
    }

    free (libraries);               /* release library header of old linked list */
    libraries = NULL;

    ReleaseLibrariesIndex (&libraryindex);
}


/**
   Find reference to this LIB module name in library index and append it to the linked
   list of application modules. Then, parse linked LIB module for own LIB references
   and link them too (recursive).

 * @brief SearchLibraries
 * @param modname
 * @return
            0, successfully found and linked this LIB module into the modules list.
            Err_LibReference, if LIB reference was not found.
            Err_Memory, if memory was not available for allocation of LIB module objects.
            Err_MaxCodeSize, if code buffer max was reached during linking of LIB modules.
 */
int
SearchLibraries (const char *modname)
{
    const libobject_t *foundlib = FindLibModule (modname);    /* search for library module name in library index */

    if (foundlib != NULL) {
        return LinkLibModule (foundlib->library, foundlib->modulestart, modname);
    } else {
        return Err_LibReference;
    }
}


/*!
 * \brief
 * For each LIB name reference in this object file, look it up in the library index
 * and automatically append it to the linked list of modules.
 *
 * \param mobjf         pointer to object/library memory file
 * \param fptr_base     base file pointer of object (library) module file
 * \param nextname      relative file pointer to start of LIB name section of object file
 * \param endnames      relative file pointer to end of LIB name section of object file
 * \return
 */
int
LinkObjFileLibModules (memfile_t *mobjf, const long fptr_base, long nextname, const long endnames)
{
    long mnl;
    char modname[MAX_NAME_SIZE];

    if (mobjf == NULL) {
        return -1; /* library file failed to be cached in memory */
    }

    do {
        MemfSeek(mobjf, fptr_base + nextname);     /* set file pointer to point at library name declarations */
        MemObjFileReadString (mobjf, modname);     /* read library reference name */

        mnl = (long) strlen (modname);
        nextname += (1 + mnl);        /* remember module pointer to next name in this object module */

        if (FindSymbol (modname, globalsymbols) == NULL && SearchLibraries (modname) != 0) {
            printf("Warning: LIB reference '%s' was not found in libraries.\n", modname);
        }
    } while (nextname < endnames);

    return 0;
}
