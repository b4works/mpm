/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "config.h"
#include "datastructs.h"
#include "main.h"
#include "options.h"
#include "errors.h"
#include "asmdrctv.h"
#include "sourcefile.h"
#include "memfile.h"
#include "prsline.h"
#include "pass.h"
#include "symtables.h"
#include "avltree.h"
#include "macros.h"


/******************************************************************************
                           Local functions and variables
 ******************************************************************************/

static macro_t *AllocMacroNode (void);
static macro_t *CreateMacro (sourcefile_t *csfile, const char *macroname, int argc, char *argv[]);
static macrofunction_t *AllocMacroFunction (macro_t *macrodef);
static macrofuncexpr_t *AllocMacroFunctionExpression (void);
static macrovariable_t *AllocMacroVariable (macro_t *macrodef, macrovartype_t t);
static macrovariable_t *CreateMacroConstant(macro_t *macrodef, const char *varName);
static macrovariable_t *CreateMacroVariable(macro_t *macrodef, const char *varName, const char *varValue, int defaultValueSize, const char *defaultValue, macrovartype_t t);
static macrovariable_t *LookupMacroVariable(const avltree_t *parameterNvps, const char *macroName);
static macrovariable_t **argvClone (macro_t *macrodef, int argc, char *argv[]);
static macrobody_t *AllocMacroBody (void);
static macrotext_t *AllocMacroBodyText (void);
static macrotext_t *ScanMacroVariables(sourcefile_t *csfile, macro_t *macrodef, const char *line);
static macrotext_t *GenerateMacroLineOutput(const macro_t *macro, const macrotext_t *macline, int *currentLineSize);
static macroFnPtr LookupMacroFunction(const char *fnIdentifier);
static int compmacroname (const char *identifier, const macro_t *p);
static int cmpmacronodenames (const macro_t *kptr, const macro_t *p);
static int ParseMacroBody(sourcefile_t *csfile, macro_t *macrodef);
static int AddMacroBodyLine(macro_t *macrodef, macrotext_t * const textline);
static int AddComponentText(macrotext_t **firstTextComponent, macrotext_t **currentTextComponent, const char *sourceline, int strlength);
static int AddComponentMacroVariable( macrotext_t **firstTextComponent,macrotext_t **currentTextComponent,macrovariable_t *macroParameter);
static int AddComponentMacroFunction( sourcefile_t *csfile, macro_t *macrodef, macrotext_t **firstTextComponent, macrotext_t **currentTextComponent);
static int ParseMacroVariable(sourcefile_t *csfile, macro_t *macrodef, macrotext_t **firstTextComponent, macrotext_t **currentTextComponent);
static int CreateMacroNvpLookup(avltree_t **parameterNvps, int totalParameters, macrovariable_t **parameters, sourcefile_t *csfile);
static int AssignMacroArguments(sourcefile_t *csfile, const macro_t *macro);
static int GenerateMacroOutput(macro_t *macro, macrobody_t **macrooutput);
static int MacroFnCmp (const char *idptr, const macrofnlookup_t *macfn);
static int GetTotalCharsInMacroLine(const macrotext_t *currentcomponent);
static void WriteMacroCall2ListingFile(sourcefile_t *csfile, const macro_t *macroDef);
static void FreeMacroNvpVariable(macrovariable_t *nvp);
static void FreeMacroVariables(const int totalparameters, macrovariable_t *nvp[]);
static void FreeMacroBody(macrobody_t *macrotext);
static void FreeMacroBodyLine(macrotext_t *textline);
static void FreeMacroFunction(macrofunction_t *macroFunction);
static void ReportMacroErrorMsg  (const char *macroname, int lineno, const char *message);
static void UpdateMacroConstant(const macro_t *macro, const char *constantName, int value);
static char *Expr2DecimalConstant(const macrofuncexpr_t *fnargs);
static char *Expr2BinaryConstant(const macrofuncexpr_t *fnargs);
static char *Expr2HexConstant(const macrofuncexpr_t *fnargs);
static char *FetchMacroParameter(const macrofuncexpr_t *fnargs);
static char *VerifyMacroParameter(const macrofuncexpr_t *fnargs);
static char *FindEndOfMacroExpr(char *endOfExpr);
static unsigned char *FindNextLine (sourcefile_t *csfile);
static unsigned char *FindEndOfMacroArguments(sourcefile_t *csf);
static includefile_t *CreateMacroIncludeFile(sourcefile_t *csfile, macro_t *macro);
static symbol_t *EvalExpr(const macro_t *macro, const macrotext_t *expression);


/* macro function table lookup */
static size_t total_macrofunctions = 3;
static macrofnlookup_t macrofunctionlist[] = {
    {"B",  (macroFnPtr) Expr2BinaryConstant},
    {"H",  (macroFnPtr) Expr2HexConstant},
    {"MACARGV", (macroFnPtr) FetchMacroParameter},
};


static int
compmacroname (const char *identifier, const macro_t *p)
{
    return strcmp (identifier, p->macroname);
}


static int
compmacronvpname (const char *identifier, const macrovariable_t *p)
{
    return strcmp (identifier, p->name);
}


static int
cmpmacronodenames (const macro_t *kptr, const macro_t *p)
{
    return strcmp (kptr->macroname, p->macroname);
}


static int
cmpmacronodenvpnames (const macrovariable_t *kptr, const macrovariable_t *p)
{
    return strcmp (kptr->name, p->name);
}


static macrovariable_t *
AllocMacroVariable (macro_t *macrodef, macrovartype_t t)
{
    macrovariable_t *mp = (macrovariable_t *) malloc (sizeof (macrovariable_t));

    if (mp != NULL) {
        mp->type = t;
        mp->macroref = macrodef;
        mp->name = NULL;
        mp->value = NULL;
        mp->defaultvalue = NULL;
        mp->pvLen = 0;
        mp->dfPvLen = 0;
    }

    return mp;
}


static macro_t *
AllocMacroNode (void)
{
    macro_t *newmacro = (macro_t *) malloc (sizeof (macro_t));

    if (newmacro != NULL) {
        newmacro->macroname = NULL;

        /* by default, no macro parameters are specified */
        newmacro->instanceCounter = 0;
        newmacro->totalParameters = 0;
        newmacro->lineNo = 0;
        newmacro->parameters = NULL;
        newmacro->variableNvps = NULL;

        /* no text body yet */
        newmacro->macrotext = NULL;
        newmacro->currentline = NULL;
    }

    return newmacro;
}


static macrobody_t *
AllocMacroBody (void)
{
    macrobody_t *mb = (macrobody_t *) malloc (sizeof (macrobody_t));

    if (mb != NULL) {
        mb->textline = NULL;
        mb->nextmacroline = NULL;
    }

    return mb;
}


static macrotext_t *
AllocMacroBodyText (void)
{
    macrotext_t *newtext = (macrotext_t *) malloc (sizeof (macrotext_t));

    if (newtext != NULL) {
        newtext->text = NULL;
        newtext->textLen = 0;
        newtext->macrovariable = NULL;
        newtext->macrofunction = NULL;
        newtext->nextmacrotext = NULL;
    }

    return newtext;
}


static macrofunction_t *
AllocMacroFunction (macro_t *macrodef)
{
    macrofunction_t *newfunc = (macrofunction_t *) malloc (sizeof (macrofunction_t));

    if (newfunc != NULL) {
        newfunc->result = NULL;
        newfunc->fnPtr = NULL;
        newfunc->functionargs = AllocMacroFunctionExpression();
        if (newfunc->functionargs != NULL) {
            /* function arguments (expression) will be added when parsed */
            newfunc->functionargs->macroref = macrodef;
        } else {
            /* ups, allocation error */
            free(newfunc);
            newfunc = NULL;
        }
    }

    return newfunc;
}



static macrofuncexpr_t *
AllocMacroFunctionExpression (void)
{
    macrofuncexpr_t *newfuncexpr = (macrofuncexpr_t *) malloc (sizeof (macrofuncexpr_t));

    if (newfuncexpr != NULL) {
        newfuncexpr->macroref = NULL;
        newfuncexpr->expression = NULL;
    }

    return newfuncexpr;
}



/*!
 * \brief Report error that occurs during macro body expansion (not yet an Mpm file that is parsed during pass 1)
 * \param macroname
 * \param lineno
 * \param message
 */
static void
ReportMacroErrorMsg  (const char *macroname, int lineno, const char *message)
{
    static char errstr[512]; /* enough space for big module names and error message... */

    asmerror = true;

    snprintf(errstr, 511, "In macro '%s', at line %d, ", macroname, lineno);
    strncat(errstr, message, 511);

    fprintf (stderr, "%s\n", errstr);
}



/*!
 * \brief Evaluate expanded macro component expression and return pointer to $EVAL assembler function symbol
 * \details
 *     Helper function for
        FetchMacroParameter(), Expr2DecimalConstant(),
        Expr2HexConstant() & Expr2BinaryConstant().

 * \param macro
 * \param expression
 * \return
 */
static symbol_t *
EvalExpr(const macro_t *macro, const macrotext_t *expression)
{
    /* first scan current linked list of macro body line to know total size of line output */
    int exprStrSize;
    macrotext_t *expressionOutput;
    symbol_t *evalsym;

    if ( (expressionOutput = GenerateMacroLineOutput(macro, expression, &exprStrSize)) == NULL) {
        /* heap memory exhausted! */
        ReportMacroErrorMsg(macro->macroname, macro->lineNo, GetErrorMessage(Err_Memory));
        return NULL;
    }

    if ((evalsym = AsmEvalExpr(macro->macroname, macro->lineNo, expressionOutput->text)) == NULL) {
        ReportMacroErrorMsg(macro->macroname, macro->lineNo, GetErrorMessage(Err_SymNotDefined));
    }

    FreeMacroBodyLine(expressionOutput); /* free temp. allocated text output */

    return evalsym;
}


/*!
 * \brief Macro Function: Fetch Macro Parameter value through generic interface
 * \details Evaluate optional expression to identify index of macro parameter (default is 0).
 * \param fnargs
 * \return
 * Returns pointer to macro parameter value, or NULL if not found.
 * Returns NULL if macro parameter index is out of range or no memory memory
 * to allocate memory for return string
 */
static char *
FetchMacroParameter(const macrofuncexpr_t *fnargs)
{
    const symbol_t *evalsym = NULL;
    const macrovariable_t *mcvar;
    int macroParameterIndex;

    if (fnargs->expression != NULL) {
        evalsym = EvalExpr(fnargs->macroref, fnargs->expression);
        macroParameterIndex = (int) evalsym->symvalue;
    } else {
        /* no arguments specified, default is first macro parameter */
        macroParameterIndex = 0;
    }

    if (fnargs->macroref->totalParameters == 0) {
        /* no arguments are defined for this macro, report 'Unknown parameter' */
        ReportMacroErrorMsg(fnargs->macroref->macroname, fnargs->macroref->lineNo, GetErrorMessage(Err_UnknownMacroParameter));
        return NULL;
    }

    /* check if parameter index is within range of available macro parameters */
    if ( (macroParameterIndex >= 0) && (macroParameterIndex < fnargs->macroref->totalParameters) ) {
        mcvar = fnargs->macroref->parameters[macroParameterIndex];
        if (mcvar->value != NULL) {
            return strclone(mcvar->value);
        } else {
            if (mcvar->defaultvalue != NULL) {
                return strclone(mcvar->defaultvalue);
            } else {
                return strclone(""); /* the parameter has no value, return pointer to an empty string */
            }
        }
    } else {
        ReportMacroErrorMsg(fnargs->macroref->macroname, fnargs->macroref->lineNo, GetErrorMessage(Err_IntegerRange));
        return NULL;
    }
}


/* ----------------------------------------------------------------
    char *VerifyMacroParameter(macrofuncexpr_t *fnargs)

    Macro Function:
    Verify Macro Parameter to be specified at macro call.
    Evaluate optional expression to identify index of macro
    parameter (default is 0).

    Returns pointer to "1" if parameter were specified, or "0".

    Returns NULL if macro parameter index is out of range or
    no memory available to allocate pointer to "1" or "0"
   ---------------------------------------------------------------- */
static char *
VerifyMacroParameter(const macrofuncexpr_t *fnargs)
{
    const symbol_t *evalsym = NULL;
    const macrovariable_t *mcvar;
    int macroParameterIndex;

    if (fnargs->expression != NULL) {
        evalsym = EvalExpr(fnargs->macroref, fnargs->expression);
        macroParameterIndex = (int) evalsym->symvalue;
    } else {
        /* no arguments specified, default is first macro parameter */
        macroParameterIndex = 0;
    }

    if (fnargs->macroref->totalParameters == 0) {
        /* no arguments are defined for this macro, report 'Unknown parameter' */
        ReportMacroErrorMsg(fnargs->macroref->macroname, fnargs->macroref->lineNo, GetErrorMessage(Err_UnknownMacroParameter));
        return NULL;
    }

    /* check if parameter index is within range of available macro parameters */
    if ( (macroParameterIndex >= 0) && (macroParameterIndex < fnargs->macroref->totalParameters) ) {
        mcvar = fnargs->macroref->parameters[macroParameterIndex];
        if (mcvar->value != NULL) {
            return strclone("1");
        } else {
            return strclone("0"); /* the parameter was not specified */
        }
    } else {
        ReportMacroErrorMsg(fnargs->macroref->macroname, fnargs->macroref->lineNo, GetErrorMessage(Err_IntegerRange));
        return NULL;
    }
}


/* ----------------------------------------------------------------
    char *Expr2DecimalConstant(macrofuncexpr_t *fnargs)

    Macro Function:
    Evaluate expanded macro component expression and return as result
    a decimal text constant.

    Returns pointer to "0" if no expression was specified, or
    NULL if a syntax error occurred.
   ---------------------------------------------------------------- */
static char *
Expr2DecimalConstant(const macrofuncexpr_t *fnargs)
{
    const symbol_t *evalsym;
    char buf[32];

    if (fnargs->expression != NULL) {
        evalsym = EvalExpr(fnargs->macroref, fnargs->expression);
        if (evalsym != NULL) {
            snprintf(buf, 31, "%ld", evalsym->symvalue);
            return strclone(buf); /* return evaluated output as allocated string on heap */
        } else {
            return NULL;
        }
    } else {
        /* No expression was specified, return a "0" result */
        return strclone("0");
    }
}



/* ----------------------------------------------------------------
    char *Expr2HexConstant(macrofuncexpr_t *fnargs)

    Macro Function:
    Evaluate expanded macro component expression and return as result
    a hexadecimal text constant.

    Returns pointer to "0" if no expression was specified, or
    NULL if a syntax error occurred.
   ---------------------------------------------------------------- */
static char *
Expr2HexConstant(const macrofuncexpr_t *fnargs)
{
    const symbol_t *evalsym;
    char buf[32];

    if (fnargs->expression != NULL) {
        evalsym = EvalExpr(fnargs->macroref, fnargs->expression);
        if (evalsym != NULL) {
            snprintf(buf, 31, "0%lxh", evalsym->symvalue);
            return strclone(buf); /* return evaluated output as allocated string on heap */
        } else {
            return NULL;
        }
    } else {
        /* No expression was specified, return a "0" result */
        return strclone("0");
    }
}



/* ----------------------------------------------------------------
    char *Expr2BinaryConstant(macrofuncexpr_t *fnargs)

    Macro Function:
    Evaluate expanded macro component expression and return as result
    a binary decimal text constant.

    Returns pointer to "0" if no expression was specified, or
    NULL if a syntax error occurred.
   ---------------------------------------------------------------- */
static char *
Expr2BinaryConstant(const macrofuncexpr_t *fnargs)
{
    const symbol_t *evalsym;
    char buf[72];

    if (fnargs->expression != NULL) {
        evalsym = EvalExpr(fnargs->macroref, fnargs->expression);
        if (evalsym != NULL) {
            strncpy(buf,"0", 71);
            ltoasc( evalsym->symvalue, buf+1, 69, 2);
            strncat(buf,"b", 71);
            return strclone(buf); /* return evaluated output as allocated string on heap */
        } else {
            return NULL;
        }
    } else {
        /* No expression was specified, return a "0" result */
        return strclone("0");
    }
}


/*!
 * \brief From current position in memory file, scan complete source code line for beginning of next line
 * \details Current file position is maintained after line has been scanned
 * \param csfile
 * \return pointer to first character after line feed (next line)
 */
static unsigned char *
FindNextLine (sourcefile_t *csfile)
{
    const unsigned char *startlineptr = MemfTellPtr (csfile->mf); /* remember file position of beginning of line */
    unsigned char *nextlineptr;
    int c;

    while( ((c = GetChar(csfile)) !='\n') && (c != MFEOF) );

    /* file position of beginning of next line identified */
    nextlineptr = MemfTellPtr (csfile->mf);

    /* resume file position */
    MemfSeekPtr(csfile->mf, startlineptr);

    /* and let caller know the position of next line */
    return nextlineptr;
}



/* --------------------------------------------------------------------------
    macrovariable_t *
    CreateMacroConstant(macro_t *macrodef, char *varName)

    Create Macro Constant in the macro index which will be populated
    when the macro is executed. The constant is created in the initialisation
    of a new macro body to be parsed.

    <varName> is always specified.

    returns pointer to new macro constant, or NULL if heap memory exhausted
   -------------------------------------------------------------------------- */
static macrovariable_t *
CreateMacroConstant(macro_t *macrodef, const char *varName)
{
    macrovariable_t *macroConstant = CreateMacroVariable(macrodef, varName, NULL, 0, NULL, constant);

    if (macroConstant != NULL) {
        if ( Insert (&macrodef->variableNvps, macroConstant, (compfunc_t) cmpmacronodenvpnames) == 0) {
            ReportError (NULL, Err_Memory);
            FreeMacroNvpVariable(macroConstant);
            macroConstant = NULL;
        }
    } else {
        ReportError (NULL, Err_Memory);
    }

    return macroConstant;
}



/* --------------------------------------------------------------------------
    static macrovariable_t *
    CreateMacroVariable(macro_t *macrodef
                         char *parameterName,
                         char *parametervalue,
                         int defaultValueSize,
                         char *defaultValue
                         macrovartype_t t
                        )

    Allocate a new instance of a macro variable  and populate it with a
    name/value/default values.

    <parameterName> is always specified.
    <parameterValue> is optional (may be NULL)
    <defaultValue> is optional (may be NULL)

    if <parametervalue> == NULL and defaultValueSize > 0, pre-allocate space
    for parameter value (caller will later copy value into buffer).

    returns pointer to new macroparameter, or NULL if heap memory exhausted
   -------------------------------------------------------------------------- */
static macrovariable_t *
CreateMacroVariable(macro_t *macrodef, const char *varName, const char *varValue, int defaultValueSize, const char *defaultValue, macrovartype_t t)
{
    macrovariable_t *mp = AllocMacroVariable(macrodef, t);

    if (mp == NULL) {
        return NULL;
    }

    mp->name = strclone(varName);
    if (mp->name != NULL) {
        trim(mp->name);
    } else {
        free (mp);
        return NULL;
    }

    if ( (varValue == NULL) && (defaultValueSize > 0) ) {
        /* allocate default space for value string (to be copied into buffer later) */
        mp->value = AllocIdentifier((size_t) defaultValueSize);
        if (mp->value == NULL) {
            free(mp->name);
            free (mp);
            return NULL;
        }
    } else {
        if ( varValue != NULL ) {
            mp->value = strclone(varValue);
            if (mp->value != NULL) {
                trim(mp->value);
                mp->pvLen = (int) strlen(mp->value);
            } else {
                free(mp->name);
                free (mp);
                return NULL;
            }
        }
    }

    if (defaultValue != NULL) {
        mp->defaultvalue = strclone(defaultValue);
        if (mp->defaultvalue != NULL) {
            trim(mp->defaultvalue);
            mp->dfPvLen = (int) strlen(mp->defaultvalue);

            /* NULL'ify empty string for default value */
            if (mp->dfPvLen == 0) {
                free(mp->defaultvalue);
                mp->defaultvalue = NULL;
            }

        } else {
            if (mp->value != NULL) free(mp->value);
            free(mp->name);
            free (mp);
            mp = NULL;
        }
    }

    return mp;
}



/* --------------------------------------------------------------------------
    int
    CreateMacroNvpLookup(avltree_t **variableNvps, int totalParameters, macrovariable_t **parameters)

    Create a binary tree of the macro parameter name/value pairs for fast lookup access.
    returns 1 if tree was successfully created, or 0 if memory was exhausted
   -------------------------------------------------------------------------- */
static int
CreateMacroNvpLookup(avltree_t **variableNvps, int totalParameters, macrovariable_t **parameters, sourcefile_t *csfile)
{
    int createdStatus = 1;

    if (totalParameters == 0) {
        /* ignore when no parameters */
        return createdStatus;
    }

    for (int mnvp = 0; mnvp<totalParameters; mnvp++) {
        if (LookupMacroVariable(*variableNvps, parameters[mnvp]->name) == NULL) {
            if ( (createdStatus = Insert (variableNvps, parameters[mnvp], (compfunc_t) cmpmacronodenvpnames)) == 0) {
                ReportError (NULL, Err_Memory);
                return createdStatus;
            }
        } else {
            ReportError (csfile, Err_DuplicateMacroParameter);
            createdStatus = 0;
            break;
        }
    }

    return createdStatus;
}



/* --------------------------------------------------------------------------
    macro_t *CreateMacro (char *macroname, int argc, char *argv[])

    return pointer to allocated macro definition, or NULL if memory was exhausted
   -------------------------------------------------------------------------- */
static macro_t *
CreateMacro (sourcefile_t *csfile, const char *macroname, int argc, char *argv[])
{
    macro_t *newmacro;

    if ((newmacro = AllocMacroNode ()) == NULL) {
        /* Create area for a new macro structure */
        ReportError (NULL, Err_Memory);
        return NULL;
    }

    /* pre-define the "MACINDEX" macro constant in macro definition */
    if ( CreateMacroConstant(newmacro, MACRO_INSTANCECOUNTER_NAME) == NULL) {
        FreeMacro (newmacro);                    /* Ups no more memory left.. */
        FreeMacroNameParameters(argc, argv);     /* also release duplicated macro parameter strings */
        return NULL;
    }

    /* pre-define the "MACARGC" macro constant in macro definition */
    if ( CreateMacroConstant(newmacro, MACRO_INSTANCEARGCOUNTER_NAME) == NULL) {
        FreeMacro (newmacro);                    /* Ups no more memory left.. */
        FreeMacroNameParameters(argc, argv);     /* also release duplicated macro parameter strings */
        return NULL;
    }

    newmacro->macroname = strclone(macroname);
    if (newmacro->macroname == NULL) {
        FreeMacro (newmacro);                    /* Ups no more memory left.. */
        FreeMacroNameParameters(argc, argv);     /* also release duplicated macro parameter strings */

        ReportError (NULL, Err_Memory);
        return NULL;
    }

    if (argc > 0) {
        /* macro has parameters specified */
        newmacro->totalParameters = argc;
        newmacro->parameters = argvClone(newmacro, argc, argv);
        if (newmacro->parameters == NULL) {
            FreeMacroNameParameters(argc, argv);     /* also release duplicated macro parameter strings */
            FreeMacro (newmacro);

            ReportError (NULL, Err_Memory);
            return NULL;
        }

        if (CreateMacroNvpLookup(&newmacro->variableNvps, argc, newmacro->parameters, csfile) == 0) {
            /* the avltree might have been partially created.. */
            FreeMacroVariables(argc, newmacro->parameters);
            FreeMacroNameParameters(argc, argv);     /* also release duplicated macro parameter strings */
            FreeMacro (newmacro);

            return NULL;
        }
    }

    return newmacro;
}



/* --------------------------------------------------------------------------
    void FreeMacroNvpParameter(macrovariable_t *nvp)

    Free allocated memory of macro Name/Value variable
   -------------------------------------------------------------------------- */
static void
FreeMacroNvpVariable(macrovariable_t *nvp)
{
    if (nvp != NULL) {
        if (nvp->name != NULL) free(nvp->name);
        if (nvp->value != NULL) free(nvp->value);
        if (nvp->defaultvalue != NULL) free(nvp->defaultvalue);

        free(nvp);
    }
}



/* --------------------------------------------------------------------------
    void FreeMacroParameters(const int totalparameters, macrovariable_t *nvp[])

    Free allocated array memory of <totalparameters> macro parameters
    (Name/Value pairs of pointer to string).
   -------------------------------------------------------------------------- */
static void
FreeMacroVariables(const int totalparameters, macrovariable_t *nvp[])
{
    if (totalparameters > 0) {
        for (int mp=0; mp<totalparameters; mp++) {
            FreeMacroNvpVariable(nvp[mp]);
        }
    }
}



/* --------------------------------------------------------------------------
    void FreeMacroFunction(macrofunction_t *macroFunction)

    Free allocated ressource of a macro function object
   -------------------------------------------------------------------------- */
static void
FreeMacroFunction(macrofunction_t *macroFunction)
{
    if (macroFunction != NULL) {
        if (macroFunction->functionargs != NULL) {
            FreeMacroBodyLine(macroFunction->functionargs->expression);
            free(macroFunction->functionargs);
        }

        if (macroFunction->result != NULL) {
            free(macroFunction->result);
        }

        free(macroFunction);
    }
}



/* --------------------------------------------------------------------------
    void FreeMacroBodyLine(macrotext_t *textline)

    Free allocated ressource of the current linked list of macro body text
    line components.
   -------------------------------------------------------------------------- */
static void
FreeMacroBodyLine(macrotext_t *textline)
{
    macrotext_t *next_macrotextptr;

    while (textline != NULL) {
        next_macrotextptr = textline->nextmacrotext;

        if (textline->text != NULL) {
            /* release only text string of a line, not the macro parameter (macro NVP's be released later) */
            free(textline->text);
        }

        FreeMacroFunction(textline->macrofunction);
        free(textline);

        textline = next_macrotextptr;
    }
}



/* --------------------------------------------------------------------------
    void FreeMacroBody(macrobody_t *macrotext)

    Free allocated ressource of the macro body text with place holders
    to macro parameters.
   -------------------------------------------------------------------------- */
static void
FreeMacroBody(macrobody_t *macrotext)
{
    macrobody_t *next_macrotext;

    while (macrotext != NULL) {
        next_macrotext = macrotext->nextmacroline;
        FreeMacroBodyLine(macrotext->textline);

        free(macrotext); /* complete line released */
        macrotext = next_macrotext;
    }
}


/*!
 * \brief Add a macro body line (linked-list of text components) to existing macro body
 * \param macrodef
 * \param textline
 * \return 1 if line was added successfully, or 0 if an error occurred
 */
static int
AddMacroBodyLine(macro_t *macrodef, macrotext_t *const textline)
{
    macrobody_t *lastline;
    int lineAppendedStatus = 0;

    if (macrodef == NULL || textline == NULL)
        return 0;

    if ((lastline = AllocMacroBody()) != NULL) {
        lastline->textline = textline;

        if (macrodef->currentline == NULL) {
            /* first line is added */
            macrodef->currentline = lastline;
            macrodef->macrotext = lastline;
        } else {
            /* new line appended to existing macro body */
            macrodef->currentline->nextmacroline = lastline;
            macrodef->currentline = lastline;
        }

        lineAppendedStatus++;
    }

    return lineAppendedStatus;
}


/*!
 * \brief Append a text component to current macro body line
 * \details if <sourceline> argument is NULL, only space is allocated for the text.
 * Caller is responsible for copying the text into the macrotext_t.text buffer
 *
 * \param firstTextComponent
 * \param currentTextComponent
 * \param sourceline
 * \param strlength
 * \return 1 if text component was added successfully, or 0 if an error occurred
 */
static int
AddComponentText(macrotext_t **firstTextComponent, macrotext_t **currentTextComponent, const char *sourceline, int strlength)
{
    macrotext_t *newTextComponent = AllocMacroBodyText();

    if (newTextComponent == NULL) {
        return 0;
    }

    if ( (newTextComponent->text = AllocIdentifier((size_t) strlength+1)) == NULL) {
        /* heap memory exhausted! */
        free(newTextComponent);
        return 0;
    }

    newTextComponent->textLen = strlength;
    if (sourceline != NULL) {
        /* if source line is specified, copy it, otherwise let the caller copy contents... */
        strncpy(newTextComponent->text, sourceline, (size_t) strlength);
    }

    if (*firstTextComponent == NULL) {
        /* first component is added */
        *firstTextComponent = newTextComponent;
        *currentTextComponent = newTextComponent;
    } else {
        /* text component appended to existing line */
        (*currentTextComponent)->nextmacrotext = newTextComponent;
        *currentTextComponent = newTextComponent;
    }

    return 1;
}



/* --------------------------------------------------------------------------
    int
    AddComponentMacroVariable(  macrotext_t **firstTextComponent,
                                macrotext_t **currentTextComponent,
                                macrovariable_t *macroParameter)

    Append a macro parameter reference component to current macro body line.

    returns 1 if component was added successfully, or 0 if an error occurred
   -------------------------------------------------------------------------- */
static int
AddComponentMacroVariable( macrotext_t **firstTextComponent,
                           macrotext_t **currentTextComponent,
                           macrovariable_t *macroParameter)
{
    macrotext_t *newTextComponent = AllocMacroBodyText();
    int componentAppendedStatus = 0;

    if (newTextComponent != NULL) {
        componentAppendedStatus = 1;
        newTextComponent->macrovariable = macroParameter;

        if (*firstTextComponent == NULL) {
            /* first component is added */
            *firstTextComponent = newTextComponent;
            *currentTextComponent = newTextComponent;
        } else {
            /* component appended to existing line */
            (*currentTextComponent)->nextmacrotext = newTextComponent;
            *currentTextComponent = newTextComponent;
        }
    }

    return componentAppendedStatus;
}



/* --------------------------------------------------------------------------
    int MacroFnCmp (const char *idptr, const macrofnlookup_t *macfn)

    Comparison helper function of bsearch() to identify macro function name.

    returns pointer to macro function element in lookup table
   -------------------------------------------------------------------------- */
static int
MacroFnCmp (const char *idptr, const macrofnlookup_t *macfn)
{
    return strcmp (idptr, macfn->macrofnid);
}


/*!
 * \brief Returns an exectubale macro function pointer, based on parsed name specified in a macro body
 * \param fnIdentifier
 * \return
 */
static macroFnPtr
LookupMacroFunction(const char *fnIdentifier)
{
    const macrofnlookup_t *foundfn;

    foundfn = (macrofnlookup_t *) bsearch (fnIdentifier, macrofunctionlist, total_macrofunctions, sizeof (macrofnlookup_t), (compfunc_t) MacroFnCmp);
    if (foundfn == NULL) {
        return NULL;
    } else {
        return foundfn->fnPtr;
    }
}



/* --------------------------------------------------------------------------
    char *FindEndOfMacroExpr(char *endOfExpr)

    returns pointer to matching end of expression of macro function,
    or end-of-line (null-terminator)
   -------------------------------------------------------------------------- */
static char *
FindEndOfMacroExpr(char *endOfExpr)
{
    while ( (*endOfExpr != ']') && (*endOfExpr != '\0') ) {
        switch(*endOfExpr) {
            case '[':
                /* scan past sub-expression */
                endOfExpr = FindEndOfMacroExpr(endOfExpr+1) + 1;
                break;

            case ']':
                /* found matching end-of-expression */
                break;

            default:
                endOfExpr++;
        }
    }

    return endOfExpr;
}


/* --------------------------------------------------------------------------
    unsigned char *FindEndOfMacroDefArguments(char *endOfExpr)

    Scan macro call arguments for end-bracket ")" of arguments.

    returns pointer to matching ")", or end-of-line (null-terminator)
   -------------------------------------------------------------------------- */
static unsigned char *
FindEndOfMacroArguments(sourcefile_t *csf)
{
    int c;

    while ( ((c=GetChar(csf)) != ')') && (c != '\n') ) {
        if (c == '(') {
            /* scan past sub-expression */
           FindEndOfMacroArguments(csf);
        }
    }

    return MemfTellPrevCharPtr (csf->mf);
}


/* --------------------------------------------------------------------------
    Parse the macro function name and optional [] expression and return
    a macro function object that contains executable service function
    and linked list of expanded macro parameter expression.

    returns pointer to macro function object or NULL if an error occurred
   -------------------------------------------------------------------------- */
static macrofunction_t *
ParseMacroFunction(sourcefile_t *csfile, macro_t *macrodef)
{
    macrofunction_t *mcFunc = AllocMacroFunction(macrodef);
    macrotext_t *fnArgExpr;
    int macroFnIdent;
    char *startOfExpr;
    char *endOfExpr;
    char *lptr = NULL;

    if (mcFunc == NULL) {
        ReportError (NULL, Err_Memory);
        return NULL;
    }

    macroFnIdent = GetChar(csfile);
    switch(macroFnIdent) {
        case '$':
            /* Generic Parameter Macro function alias, \$$ */
            mcFunc->fnPtr = (macroFnPtr) FetchMacroParameter;
            lptr = (char *) MemfTellPtr(csfile->mf);
            GetSym(csfile); /* fetch left expression bracket (optional) */
            break;

        case '?':
            mcFunc->fnPtr = (macroFnPtr) VerifyMacroParameter;
            lptr = (char *) MemfTellPtr(csfile->mf);
            GetSym(csfile); /* fetch left expression bracket (optional) */
            break;

        default:
            MemfUngetc(csfile->mf);
            if (GetSym(csfile) == name) {
                /* \$xxx, get pointer to macro function */
                mcFunc->fnPtr = (macroFnPtr) LookupMacroFunction(csfile->ident);
                lptr = (char *) MemfTellPtr(csfile->mf);
                GetSym(csfile); /* fetch left expression bracket */
            } else {
                if (strlen(csfile->ident) == 0) {
                    /* no name, which means \$, the expression -> decimal constant function evaluation */
                    mcFunc->fnPtr = (macroFnPtr) Expr2DecimalConstant;
                }
            }
    }

    if (mcFunc->fnPtr == NULL) {
        /* Macro function name specified in source code is unknown */
        ReportError (csfile, Err_UnknownMacroFunction);
        FreeMacroFunction(mcFunc);
        mcFunc = NULL;
    } else {
        /* parse expression and assign to macro function object */
        if (csfile->sym == lexpr) {
            startOfExpr = (char *) MemfTellPtr(csfile->mf);

            /* use recursive scan to find matching right bracket of macro function expression */
            endOfExpr = FindEndOfMacroExpr(startOfExpr);
            if (*endOfExpr == '\0') {
                /* matching right bracket of expression was not found */
                ReportError (csfile, Err_Syntax);
                FreeMacroFunction(mcFunc);
                mcFunc = NULL;
            } else {
                /* encapsulate expression as only visible line, for isolated syntax scanning of expression */
                *endOfExpr = '\0';

                if ((fnArgExpr = ScanMacroVariables(csfile, macrodef, startOfExpr)) != NULL) {
                    /* macro function expression successfully parsed and organized as linked list */
                    mcFunc->functionargs->expression = fnArgExpr;
                } else {
                    /* an error occurred, release macro function resource */
                    FreeMacroFunction(mcFunc);
                    mcFunc = NULL;
                }

                *endOfExpr = ']';
                ++endOfExpr;

                /* restore line parser file pointer to end of this macro function expression */
                MemfSeekPtr(csfile->mf, (unsigned char *) endOfExpr);
            }
        } else {
            // restore file pointer to exactly the char after \$
            MemfSeekPtr(csfile->mf, (unsigned char *) lptr);
        }
    }

    return mcFunc;
}



/* --------------------------------------------------------------------------
    Append a macro function reference component to current macro body line.
    The \$ identifier has been parsed, to optionally read function name and
    expression surrounded by [].

    returns 1 if component was added successfully, or 0 if an error occurred
   -------------------------------------------------------------------------- */
static int
AddComponentMacroFunction( sourcefile_t *csfile,
                           macro_t *macrodef,
                           macrotext_t **firstTextComponent,
                           macrotext_t **currentTextComponent)
{
    int componentAppendedStatus = 0;
    macrotext_t *newFunctionComponent;
    macrofunction_t *mcFunc = ParseMacroFunction(csfile, macrodef);

    if ((newFunctionComponent = AllocMacroBodyText()) == NULL) {
        ReportError (NULL, Err_Memory);
        if (mcFunc != NULL)
            FreeMacroFunction(mcFunc);

        return 0;
    }

    if (mcFunc == NULL) {
        /* An error occurred while parsing the macro function (already reported) */
        FreeMacroBodyLine(newFunctionComponent);
    } else {
        /* add function container */
        componentAppendedStatus = 1;
        newFunctionComponent->macrofunction = mcFunc;

        if (*firstTextComponent == NULL) {
            /* first component is added */
            *firstTextComponent = newFunctionComponent;
            *currentTextComponent = newFunctionComponent;
        } else {
            /* component appended to existing line */
            (*currentTextComponent)->nextmacrotext = newFunctionComponent;
            *currentTextComponent = newFunctionComponent;
        }
    }

    return componentAppendedStatus;
}



/* --------------------------------------------------------------------------
    macrovariable_t **argvClone (macro_t *macrodef, int argc, char *argv[])

    Allocate dynamically from the heap to move the simple macro parameter name
    argument list (the argv[]), into the new array of macro parameters pointing
    to each macro parameter name and value (each macro name and value is a string
    which was also dynamically allocated).

    Macro parameter syntax is name [= value] , ...

    <value> can be anything, until "," or end of line (optionally ")" ).

    returns pointer to array of macro parameters, or NULL if memory was not available
   -------------------------------------------------------------------------- */
static macrovariable_t **
argvClone (macro_t *macrodef, int argc, char *argv[])
{
    macrovariable_t **pArray = (macrovariable_t **) malloc(sizeof(macrovariable_t *) * (size_t) argc);
    macrovariable_t *macroparameter;
    char *defaultParValue;

    if (pArray != NULL) {
        for (int macroArg=0; macroArg < argc; macroArg++ ) {
            if ( (defaultParValue = strchr (argv[macroArg], '=')) != NULL) {
                /* default parameter specified, separate name and default parameter strings */
                *defaultParValue++ = '\0';
                macroparameter = CreateMacroVariable(macrodef, argv[macroArg], NULL, 0, defaultParValue, callparameter);
            } else {
                macroparameter = CreateMacroVariable(macrodef, argv[macroArg], NULL, 0, NULL, callparameter);
            }

            if (macroparameter != NULL) {
                pArray[macroArg] = macroparameter;
            } else {
                /* Heap memory exhausted - release what was allocated, then exit... */
                FreeMacroVariables(macroArg, pArray);
                free(pArray);
                pArray = NULL;
                break;
            }
        }
    }

    return pArray;
}



/* --------------------------------------------------------------------------
    static macrovariable_t *
    LookupMacroParameter(avltree_t *varNvps, char *macroName)

    Find the Macro Variable Name/Value Pair (NVP), searching by name.

    returns point to macro NVP, or NULL if if parameter name wasn't found
   -------------------------------------------------------------------------- */
static macrovariable_t *
LookupMacroVariable(const avltree_t *varNvps, const char *macroName)
{
    return Find (varNvps, macroName, (compfunc_t) compmacronvpname);
}



/* --------------------------------------------------------------------------
    Parse the Macro variable name \xxx or macro function \$xxx[] at current
    source code file position and apply & reference it into the current linked
    list of macro body line.

    returns 1 if macro variable was parsed successfully and referenced,
    or 0 if an error occurred
   -------------------------------------------------------------------------- */
static int
ParseMacroVariable(sourcefile_t *csfile,
                   macro_t *macrodef,
                   macrotext_t **firstTextComponent,
                   macrotext_t **currentTextComponent)
{
    int parsedStatus = 0;
    macrovariable_t *macroVariable;

    switch (GetChar(csfile)) {
        case '@':
            /* \@ is alias for pre-defined "MACINDEX" Macro Constant NVP */
            csfile->sym = name;
            snprintf(csfile->ident, MAX_NAME_SIZE-1, "%s", MACRO_INSTANCECOUNTER_NAME);
            break;

        case '%':
            /* \% is alias for pre-defined "MACARGC" Macro Constant NVP */
            csfile->sym = name;
            snprintf(csfile->ident, MAX_NAME_SIZE-1, "%s", MACRO_INSTANCEARGCOUNTER_NAME);
            break;

        case '$':
            /* macro function identifier. Optional name follows with [] expression enclosure */
            return AddComponentMacroFunction(csfile, macrodef, firstTextComponent, currentTextComponent);

        case '\\':
            /* double symbol substitution identifier just means single '\' character (no variable) */
            return AddComponentText(firstTextComponent, currentTextComponent, "\\\\", 2);

        default:
            MemfUngetc(csfile->mf);
            GetSym(csfile);
    }

    if (csfile->sym != name) {
        /* macro variable was not identified as a name */
        ReportError (csfile, Err_Syntax);
        return parsedStatus;
    }

    /* Fetched macro variable name, absorb optional trailing \ of macro variable name, if specified */
    if (GetChar(csfile) != '\\')
        MemfUngetc(csfile->mf);

    if ( (macroVariable = LookupMacroVariable(macrodef->variableNvps, csfile->ident)) != NULL) {
        /* add macro variable component to macro body line */
        if ( (parsedStatus = AddComponentMacroVariable(firstTextComponent, currentTextComponent, macroVariable)) == 0) {
            ReportError (NULL, Err_Memory);
        }
    } else {
        /* macro variable was not found in index, create it as a local macro body variable (possibly a label) */
        if ( (macroVariable = CreateMacroVariable(macrodef, csfile->ident, NULL, 0, NULL, localvariable)) == NULL) {
            ReportError (NULL, Err_Memory);
            return parsedStatus;
        }

        if ( Insert (&macrodef->variableNvps, macroVariable, (compfunc_t) cmpmacronodenvpnames) ) {
            /* add macro variable component to macro body line */
            if ( (parsedStatus = AddComponentMacroVariable(firstTextComponent, currentTextComponent, macroVariable)) == 0) {
                ReportError (NULL, Err_Memory);
            } else {
                parsedStatus = 1;
            }
        } else {
            ReportError (NULL, Err_Memory);
            FreeMacroNvpVariable(macroVariable);
        }
    }

    return parsedStatus;
}


/*!
 * \brief Generate macro body text line (one or multiple components) and scan for macro variables
 * \param macrodef
 * \param sourceline
 * \return returns pointer to linked list if current line if parsed successfully, or NULL if an error occurred
 */
static macrotext_t *
ScanMacroVariables(sourcefile_t *csfile, macro_t *macrodef, const char *sourceline)
{
    macrotext_t *firstTextComponent = NULL;
    macrotext_t *currentTextComponent = NULL;
    char *comment;
    char nullchar = 0;
    char *macroVarName;
    int srclinelen;
    int strlength;

    if (macrodef == NULL || sourceline == NULL)
        return NULL;

    if ((comment = strchr (sourceline, ';')) != NULL) {
        /* only scan for macro variables before comment separator */
        /* the comment is converted to a linefeed */
        nullchar = *(comment+1);
        *comment = '\n';
        *(comment+1) = '\0';
    }

    csfile->eol = false;

    do {
        /* check for macro variables */
        macroVarName = strchr (sourceline, '\\');

        if (macroVarName == NULL) {
            /* no Macro variable found, add current text component as-is to macro body (if any text is left) */
            srclinelen = (int) strlen(sourceline);
            if (srclinelen > 0 && AddComponentText(&firstTextComponent, &currentTextComponent, sourceline, srclinelen) == 0) {
                /* no room for text component */
                ReportError (NULL, Err_Memory);
                FreeMacroBodyLine(firstTextComponent);
                currentTextComponent = firstTextComponent = NULL;
            }
        } else {
            /* store current text component until parameter, if any */
            strlength = (int) (macroVarName - sourceline);
            if (strlength > 0 && AddComponentText(&firstTextComponent, &currentTextComponent, sourceline, strlength) == 0) {
                /* no room for text component */
                ReportError (NULL, Err_Memory);
                FreeMacroBodyLine(firstTextComponent);
                currentTextComponent = firstTextComponent = NULL;
                return NULL;
            }

            /* then store macro variable component */
            /* set global memory file pointer for GetSym() - at first char of macro variable name */
            ++macroVarName;
            MemfSeekPtr(csfile->mf, (unsigned char *) macroVarName);
            ParseMacroVariable(csfile, macrodef, &firstTextComponent, &currentTextComponent);

            /* rest of source line is just after macro variable... */
            sourceline = (char *) MemfTellPtr (csfile->mf);
        }
    } while ( macroVarName != NULL );

    if (comment != NULL) {
        /* restore comment separator (and character just after) */
        *comment = ';';
        *(comment+1) = nullchar;
    }

    /* return pointer to first component of linked list of macro-substituted text, or NULL if allocation failed */
    return firstTextComponent;
}


/* --------------------------------------------------------------------------
    Parses source code lines until an ENDM, MACEND or ENDMACRO directive
    is encountered.

    The source code is scanned for macro variables and macro functions which
    is substituted with placeholder data structures so it becomes easy to
    execute the entire macro body with real parameter arguments and identified
    local macro variables.

    returns 1 if macro text body was parsed successfully, or 0 if an error occurred
   -------------------------------------------------------------------------- */
static int
ParseMacroBody(sourcefile_t *csfile, macro_t *macrodef)
{
    macrotext_t *scannedVariables;
    unsigned char *nextlineptr;
    unsigned char origchar;
    int parsedStatus = 1;

    /* parse macro body until 'ENDM', 'MACEND' or 'ENDMACRO', worst case to end of file */
    while (parsedStatus == 1 && !MemfEof (csfile->mf)) {

        /* keep up-to-date with line counter while parsing macro body */
        ++csfile->lineno;

        csfile->lineptr = MemfTellPtr(csfile->mf);
        nextlineptr = FindNextLine (csfile);   /* know the position of next line */

        /* fetch first identifier on line to check for end of macro */
        if (GetSym(csfile) == name && LookupDirective(csfile->sym, csfile->ident) == &EndMacro) {
            writeline = false;    /* don't write current source line to listing file (ENDEF MACRO line) */
            break;
        }

        /* check for one or more macro variable in line buffer and generate macro body accordingly */
        /* only scan current line if no previous error occurred */
        if (parsedStatus == 1) {
            MemfSeekPtr(csfile->mf, csfile->lineptr);
            origchar = *nextlineptr;

            /* define current line, null-terminated */
            *nextlineptr = '\0';

            /* parse current source code line and append to linked list -> add to macro body.. */
            scannedVariables = ScanMacroVariables(csfile, macrodef, (char *) csfile->lineptr);
            /* if parsing failed or no insufficient were available, parseStatus = 0 */
            parsedStatus = AddMacroBodyLine(macrodef, scannedVariables);
            if (parsedStatus == 0) {
                /* scanned variable line failed to be added to macro body - release it */
                ReportError (NULL, Err_Memory);
                FreeMacroBodyLine(scannedVariables);
            }

            /* restore original character, so parsing may be continued */
            *nextlineptr = origchar;
        }

        MemfSeekPtr(csfile->mf, nextlineptr);   /* get ready for next line in macro body definition */
    }

    return parsedStatus; /* if 0 is returned, one of the macro body lines reported an error */
}


/*!
 * \details
 * Scan current macro line (linked list) to obtain the total chars used by all
 * components of that line (whether static text, macro variable or macro function)
 *
 * \param currentcomponent
 * \return total no. of characters to be used for macro line output. -1 if an error occurred
 */
static int
GetTotalCharsInMacroLine(const macrotext_t *currentcomponent)
{
    int currentLineSize = 0;

    while(currentcomponent != NULL) {
        if (currentcomponent->macrovariable != NULL) {
            /* this component refers to a macro variable... fetch it's argument value text size (if available) */
            if (currentcomponent->macrovariable->value != NULL) {
                currentLineSize += currentcomponent->macrovariable->pvLen;
            } else {
                if (currentcomponent->macrovariable->defaultvalue != NULL) {
                    currentLineSize += currentcomponent->macrovariable->dfPvLen;
                }
            }
        } else {
            if (currentcomponent->macrofunction != NULL) {
                if (currentcomponent->macrofunction->result == NULL) {
                    /* a macro function component is referenced, execute it and cache it's output */
                    currentcomponent->macrofunction->result = currentcomponent->macrofunction->fnPtr(currentcomponent->macrofunction->functionargs);
                }
                if (currentcomponent->macrofunction->result != NULL) {
                    currentLineSize += strlen(currentcomponent->macrofunction->result);
                } else {
                    /* An error occurred */
                    return -1;
                }
            } else {
                /* this component refers to a piece of text and it's length */
                currentLineSize += currentcomponent->textLen;
            }
        }

        currentcomponent = currentcomponent->nextmacrotext;
    }

    return currentLineSize;
}


/*!
 * \brief Generate a single line of macro source code output
 * \details
 * Generate a single line of macro source code output, based on current <line> input and
 * also return the total size for the output string.
 * \param macro
 * \param line
 * \param currentLineSize
 *
 * \return
 * a pointer to the generated text line as a macrotext_t->text field the length of the
 * output string in byref argument <currentLineSize>
 * Returns NULL if there were memory allocation problems
 */
static macrotext_t *
GenerateMacroLineOutput(const macro_t *macro, const macrotext_t *macline, int *currentLineSize)
{
    macrotext_t *macroLineOutput = NULL;
    char *lineptr;

    /* first scan current linked list of macro body line to know total size of line output */
    *currentLineSize = GetTotalCharsInMacroLine(macline);
    if (*currentLineSize == -1) {
        /* a macro function returned an error... */
        return NULL;
    }

    /* stream current linked list macro line as static text into new, single macro text line */
    if (AddComponentText(&macroLineOutput, &macroLineOutput, NULL, *currentLineSize) == 0) {
        ReportMacroErrorMsg(macro->macroname, macro->lineNo, GetErrorMessage(Err_Memory));
        return NULL;
    }

    lineptr = macroLineOutput->text;
    while(macline != NULL) {
        if (macline->macrovariable != NULL) {
            /* this component refers to a macro variable... fetch it's argument value text (if available) */
            if (macline->macrovariable->value != NULL) {
                strncat(lineptr, macline->macrovariable->value, (size_t) macline->macrovariable->pvLen);
                lineptr += macline->macrovariable->pvLen;
            } else {
                if (macline->macrovariable->defaultvalue != NULL) {
                    strncat(lineptr, macline->macrovariable->defaultvalue, (size_t) macline->macrovariable->dfPvLen);
                    lineptr += macline->macrovariable->dfPvLen;
                }
            }
        } else {
            if ((macline->macrofunction != NULL) && (macline->macrofunction->result != NULL)) {
                strncat(lineptr, macline->macrofunction->result, (size_t) macroLineOutput->textLen);
                lineptr += strlen(macline->macrofunction->result);

                /* remove cached result from previous macro function call that were done in GetTotalCharsInMacroLine() */
                free(macline->macrofunction->result);
                macline->macrofunction->result = NULL;
            } else {
                /* neither a macro variable nor macro function, ultimately, this component
                   refers to a piece of static text of the original macro body line */
                strncat(lineptr, macline->text, (size_t) macline->textLen);
                lineptr += macline->textLen;
            }
        }

        macline = macline->nextmacrotext;
    }

    return macroLineOutput;
}



/* ----------------------------------------------------------------------------------------
    int GenerateMacroOutput(macro_t *macro, macrobody_t **macrooutput)

    Generate macro source code output, based on variables and macro directives inside
    the macro definition body.

    returns
        total text size of linked list of generated source code output
        0, if macro output reported parsing problems or insufficient memory
   ---------------------------------------------------------------------------------------- */
static int
GenerateMacroOutput(macro_t *macro, macrobody_t **macrooutput)
{
    int currentLineSize;
    int macroBodyTextSize = 0;
    macrotext_t *macroLineOutput;
    macrobody_t *appendedline;
    macrobody_t *currentoutputline = NULL;

    /* first line in macro body */
    macro->lineNo = 0;
    macro->currentline = macro->macrotext;

    /* macro output always starts from scratch */
    *macrooutput = NULL;

    while (macro->currentline != NULL) {
        macro->lineNo++;

        if ( (macroLineOutput = GenerateMacroLineOutput(macro, macro->currentline->textline, &currentLineSize)) == NULL) {
            /* heap memory exhausted or execution error in macro function */
            FreeMacroBody(*macrooutput);
            *macrooutput = NULL;
            return 0;
        }

        /* append new macro line to total macro output */
        if ((appendedline = AllocMacroBody()) != NULL) {
            macroBodyTextSize += currentLineSize;
            appendedline->textline = macroLineOutput;

            if (currentoutputline == NULL) {
                /* first line is added */
                *macrooutput = appendedline;
                currentoutputline = appendedline;
            } else {
                /* new line appended to macro body output */
                currentoutputline->nextmacroline = appendedline;
                currentoutputline = appendedline;
            }
        } else {
            /* heap memory exhausted! */
            ReportMacroErrorMsg(macro->macroname, 0, GetErrorMessage(Err_Memory));
            FreeMacroBodyLine(macroLineOutput);
            FreeMacroBody(*macrooutput);
            *macrooutput = NULL;
            return 0;
        }

        /* get ready to build next macro source code line output */
        macro->currentline = macro->currentline->nextmacroline;
    }

    return macroBodyTextSize;
}


/*!
 * \brief Parse macro arguments (if any) from current source code line and assign them to the macro definition parameter list
 * \param csfile
 * \param macro
 * \return 1 if arguments were successfully assigned, or 0 if an error occurred
 */
static int
AssignMacroArguments(sourcefile_t *csfile, const macro_t *macro)
{
    int currentParIdx;
    int totalParameters;
    const unsigned char *lineptr;
    macrovariable_t *macroParameter;
    char macroArgValue[MAX_NAME_SIZE+1];

    if (macro->totalParameters == 0) {
        UpdateMacroConstant(macro, MACRO_INSTANCEARGCOUNTER_NAME, 0);

        /* no parameters were specfied in macro definition, so nothing to assign... */
        return 1;
    }

    /* Reset any previous assigned macro arguments (from previous macro execution) */
    for (currentParIdx = 0; currentParIdx < macro->totalParameters; currentParIdx++) {
        macroParameter = macro->parameters[currentParIdx];

        // only remove previous parameter, if any (default parameter stays..)
        if (macroParameter->value != NULL) {
            free(macroParameter->value);
            macroParameter->value = NULL;
            macroParameter->pvLen = 0;
        }
    }

    /* get ready to receive first macro argument */
    macroArgValue[0] = '\0';
    currentParIdx = totalParameters = 0;
    csfile->sym = nil;

    lineptr = MemfTellPtr (csfile->mf);
    GetSym(csfile);

    /* eliminate the optional bracket around macro arguments, if specified... */
    if (csfile->sym == lparen) {
        if ( !DiscardMacroArgBrackets(csfile) ) {
            return 0; /* missing right-bracked, error reported, exit... */
        } else {
            csfile->sym = nil;
        }
    } else {
        MemfSeekPtr(csfile->mf, lineptr);
    }

    /* Collect arguments */
    while ( (currentParIdx < macro->totalParameters) && (csfile->sym != newline) ) {
        macroParameter = macro->parameters[currentParIdx];

        lineptr = MemfTellPtr (csfile->mf);
        GetSym(csfile);

        switch(csfile->sym) {
            case comma:
            case newline:
                /* the current (or final) argument has been fetched, store it in argument array (trimmed) */
                if (strlen(trim(macroArgValue)) > 0) {
                    if ( (macroParameter->value = strclone(macroArgValue)) == NULL ) {
                        ReportError (NULL, Err_Memory);
                        return 0;
                    } else {
                        macroParameter->pvLen = (int) strlen(macroArgValue);
                        totalParameters++;
                    }
                } else {
                    /* the current specified parameter was empty, any default value? */
                    if (macroParameter->defaultvalue != NULL) {
                        /* a default value also counts as a countable parameter */
                        totalParameters++;
                    }
                }

                /* get ready for next macro argument */
                macroArgValue[0] = '\0';
                currentParIdx++;
                break;

            default:
                switch(csfile->sym) {
                    case dquote:
                        fetchMacroArgumentString(csfile, macroArgValue, MAX_NAME_SIZE-1, '\"');
                        break;
                    case lexpr:
                        fetchMacroArgumentString(csfile, macroArgValue, MAX_NAME_SIZE-1, ']');
                        break;
                    default:
                        MemfSeekPtr(csfile->mf, lineptr);
                        fetchMacroArgument(csfile, macroArgValue, MAX_NAME_SIZE-1);
                        break;
                }

                trim(macroArgValue);
                break;
        }

        if (csfile->sym == newline) {
            /* push back EOL, since last parameter must be stored in newline case */
            MemfUngetc(csfile->mf);
            csfile->sym = nil;
            csfile->eol = false;
        }
    }

    /* register the total parameters used for the macro call */
    UpdateMacroConstant(macro, MACRO_INSTANCEARGCOUNTER_NAME, totalParameters);
    return 1;
}



/* ----------------------------------------------------------------------------------------
    includefile_t *CreateMacroIncludeFile(macro_t *macro)

    Read macro arguments (if any) and substitute them in the macro body. Execute any
    macro directives during processing of macro body and generate final output for
    main assembler pass 1 parsing.

    returns
        includefile_t reference to generated macro source code
        NULL, if macro execution reported parsing problems or insufficient memory
   ---------------------------------------------------------------------------------------- */
static includefile_t *
CreateMacroIncludeFile(sourcefile_t *csfile, macro_t *macro)
{
    char strbuf[MAX_STRBUF_SIZE];
    includefile_t *newinclfile;
    macrobody_t *macrooutput = NULL;
    unsigned char *fdbuffer = NULL;
    int lineno = 1;

    newinclfile = AllocIncludeFile();
    if (newinclfile == NULL) {
        ReportMacroErrorMsg(macro->macroname, 0, GetErrorMessage(Err_Memory));
        return NULL;
    }

    newinclfile->fname = strclone(macro->macroname);
    if (newinclfile->fname == NULL) {
        free(newinclfile);
        ReportMacroErrorMsg(macro->macroname, 0, GetErrorMessage(Err_Memory));
        return NULL;
    }

    if ( (newinclfile->filesize = GenerateMacroOutput(macro, &macrooutput)) == 0) {
        /* An error occurred while generating linked list of macro output */
        free(newinclfile->fname);
        free(newinclfile);
        FreeMacroBody(macrooutput);
        return NULL;
    }

    fdbuffer = (unsigned char *) calloc ( (size_t) newinclfile->filesize + 1, sizeof (char));
    if (fdbuffer == NULL) {
        free(newinclfile->fname);
        free(newinclfile);
        FreeMacroBody(macrooutput);
        ReportMacroErrorMsg(macro->macroname, 0, GetErrorMessage(Err_Memory));
        return NULL;
    } else {
        /* link the buffer to the include file */
        newinclfile->filedata = fdbuffer;

        /* stream macro source code into buffer */
        macro->currentline = macrooutput; /* point to first source code line */
        while (macro->currentline != NULL) {
            if (writeexpandedmacro == true && writelistingfile == true) {
                /* if macro expansion is enabled, copy line to listing file as well */
                snprintf (strbuf, MAX_STRBUF_SIZE, "%-4d  %08lX%14s>> %s", lineno++, oldPC, "", macro->currentline->textline->text);
                Stream2ListingFile(csfile->module, strbuf);
                UpdateListingFileLineCounter(csfile->module); /* Update list file line counter, write header on page boundary */
            }

            snprintf( (char *) fdbuffer, (size_t) newinclfile->filesize+1, "%s", macro->currentline->textline->text);

            /* point at end + 1 to receive next string */
            fdbuffer = fdbuffer + macro->currentline->textline->textLen;
            macro->currentline = macro->currentline->nextmacroline;
        }

        /* finally, release macro source code output (no longer used) */
        FreeMacroBody(macrooutput);

        return newinclfile;
    }
}



/*!
 * \brief Update Macro Constant <constantName> with new string constant (of specified value)
 * \param macro
 * \param constantName
 * \param value
 */
static void
UpdateMacroConstant(const macro_t *macro, const char *constantName, int value)
{
    char buf[16];
    macrovariable_t *macroInstanceCounter = LookupMacroVariable(macro->variableNvps, constantName);

    /* Generate Ascii representation of constant (in 16 character buffer) */
    snprintf(buf, 15, "%d", value);

    if (macroInstanceCounter != NULL) {
        if (macroInstanceCounter->value != NULL) {
            /* release prev. allocated constant string */
            free(macroInstanceCounter->value);
        }

        macroInstanceCounter->value = strclone(buf);
        if (macroInstanceCounter->value != NULL) {
            macroInstanceCounter->pvLen = (int) strlen(macroInstanceCounter->value);
        } else {
            macroInstanceCounter->pvLen = 0;
        }
    }
}



/* --------------------------------------------------------------------------
    void ExpandLocalMacroVariable(macrovariable_t *var)

    Called by InOrder() through ExpandLocalMacroVariables().
    Each macro variable in the macro index is iterated, and if it is
    of type <localvariable>, it's value is updated as follows:

    [macro-name] _ \@ _ [VarName]
   -------------------------------------------------------------------------- */
static
void ExpandLocalMacroVariable(macrovariable_t *var)
{
    char buf[10];

    if (var->type == localvariable) {
        /*
            Expand local macro variable using the macro body name and instance
            counter, as follows: [macro-name] _ \@ _ [VarName]
         */
        if (var->value != NULL) {
            /* release string of previous macro instance */
            free(var->value);
            var->value = NULL;
            var->pvLen = 0;
        }

        snprintf(buf, 9,"%d", var->macroref->instanceCounter);

        var->pvLen = (int) (strlen(var->macroref->macroname) + 1 + strlen(var->name) + 1 + strlen(buf));
        var->value = (char *) malloc ( (size_t) var->pvLen + 1);
        if (var->value != NULL) {
            snprintf(var->value, (size_t) var->pvLen + 1 , "%s_%s_%s", var->macroref->macroname, buf, var->name);
        } else {
            var->pvLen = 0;
        }
    }
}


/*!
 * \brief Expand local macro variables using the macro body name and instance counter, as follows: [macro-name] _ [VarName] _ \@
 * \param macro
 */
static void
ExpandLocalMacroVariables(const macro_t *macro)
{
    /* Iterate all macro variables of type <localvariable> and expand them with the unique instance name */
    InOrder (macro->variableNvps, (void (*)(void *)) ExpandLocalMacroVariable);
}


/*!
 * \brief Write the macro call to listing file (when macro is to be expanded in listing file)
 * \details The macro call parameters are written as they were collected via AssignMacroArguments()
 * \param macroDef
 */
static
void WriteMacroCall2ListingFile(sourcefile_t *csfile, const macro_t *macroDef)
{
    char strbuf[MAX_STRBUF_SIZE];

    /* use line buffer to build a string to be written to listing file */
    snprintf(line, MAX_LINE_BUFFER_SIZE-1, "%s (", macroDef->macroname);

    if (macroDef->totalParameters == 0) {
        /* line 0 defines the macro call */
        snprintf(strbuf, MAX_STRBUF_SIZE, "%-4d  %08lX%14s>> %s):\n", 0, oldPC, "", line);
        Stream2ListingFile(csfile->module, strbuf);
        UpdateListingFileLineCounter(csfile->module);  /* Update list file line counter, write header on page boundary */
        return;
    }

    for (int currentParIdx = 0; currentParIdx < macroDef->totalParameters; currentParIdx++) {
        if (macroDef->parameters[currentParIdx]->value != NULL) {
            strncat(line, "[", MAX_LINE_BUFFER_SIZE-1);
            strncat(line, macroDef->parameters[currentParIdx]->value, MAX_LINE_BUFFER_SIZE-1);
            strncat(line, "]", MAX_LINE_BUFFER_SIZE-1);
        }

        strncat(line, ",", MAX_LINE_BUFFER_SIZE-1);
    }

    // remove final "," (not a parameter separator) and complete it with EOL
    line[strlen(line)-1] = 0;

    /* line 0 defines the macro call */
    snprintf(strbuf, MAX_STRBUF_SIZE, "%-4d  %08lX%14s>> %s):\n", 0, oldPC, "", line);
    Stream2ListingFile(csfile->module, strbuf);
    UpdateListingFileLineCounter(csfile->module);
}



/******************************************************************************
                               Public functions
 ******************************************************************************/


/*!
 * \brief Discards optional brackets in current file, surrounding the macro argument definition or call
 * \details This is function is only called if a '(' were found after the macro name
 * \param csfile
 * \return
 * 0, if no matching end-bracket were found (Syntax error reported)
 * 1, if brackets have been successfully 'patched' with space.
 */
int DiscardMacroArgBrackets(sourcefile_t *csfile)
{
    unsigned char *fptr = MemfTellPtr (csfile->mf);
    unsigned char *lineptr = fptr;
    unsigned char *endOfArgs = FindEndOfMacroArguments(csfile);
    int foundBracket = 0;

    if (lineptr == NULL) {
        ReportError (csfile, Err_Syntax);
        return 0;
    }

    if (endOfArgs == NULL) {
        ReportError (csfile, Err_Syntax);
        return 0;
    }

    if (*endOfArgs != ')') {
        /* matching right bracket of expression was not found */
        ReportError (csfile, Err_Syntax);
    } else {
        /* discarded optional end of argument bracket */
        *endOfArgs = ' ';

        /* go backwards one or a few chars max. to blank '('. (add fail-safe - don't go beyond start of file) */
        while( (csfile->mf->filedata < lineptr) && (*lineptr != '(') )
            --lineptr;
        *lineptr = ' '; /* discarded optional beginning of argument bracket */
        foundBracket = 1;
    }

    /* restore file pointer just after '(' */
    MemfSeekPtr(csfile->mf, fptr);

    return foundBracket;
}


/*!
 * \brief Fetch default macro parameter value string, until terminator "," is encountered
 * \details Also stops fetching if comment indicator or end of line reached
 * \param string
 * \param maxsize
 */
void fetchMacroArgument(sourcefile_t *csfile, char *string, int maxsize)
{
    int ch;
    int strLength = 0;

    while ( (strLength < maxsize) && (!MemfEof (csfile->mf)) ) {
        ch = GetChar(csfile);
        if ( (ch == ';') || (ch == '\n') || (ch == MFEOF) ) {
            csfile->sym = newline;
            csfile->eol = true;
            *string = '\0';
            return;
        }

        if (ch != ',') {
            *string++ = (char) ch;
        } else {
            /* push back the terminator, which will be read by main macro parameter parser... */
            MemfUngetc(csfile->mf);
            break;
        }
    }

    *string = '\0';
}


/*!
 * \brief Fetch default macro parameter value string, that is defined using " " or [ ]
 * \details
 * <terminator> argument defines which type of string to read.
 * Also stops fetching if comment indicator or end of line reached
 * \param csfile
 * \param string
 * \param maxsize
 * \param terminator
 */
void fetchMacroArgumentString(sourcefile_t *csfile, char *string, int maxsize, char terminator)
{
    int ch;
    int strLength = 0;

    while ( (strLength < maxsize) && (!MemfEof (csfile->mf)) ) {
        ch = GetChar(csfile);
        if ( (ch == ';') || (ch == '\n') || (ch == MFEOF) ) {
            csfile->sym = newline;
            csfile->eol = true;
        } else {
            if (ch != terminator) {
                *string++ = (char) ch;
            } else {
                break;
            }
        }
    }

    *string = '\0';
}


/* --------------------------------------------------------------------------
    void FreeMacro (macro_t *node)

    Free allocated memory of macro definition
   -------------------------------------------------------------------------- */
void
FreeMacro (macro_t *node)
{
    if (node->macroname != NULL) {
        free(node->macroname);
    }

    DeleteAll (&node->variableNvps, (void (*)(void *)) FreeMacroNvpVariable);
    if (node->parameters != NULL) {
        free(node->parameters); /* release only array container, the Nvp's were free'ed by DeleteAll() */
    }

    FreeMacroBody(node->macrotext);
    free (node);
}


/* --------------------------------------------------------------------------
    void FreeMacroNameParameters(int argc, char *argv[])

    Free allocated array memory of <argc> macro name parameters (pointer to string).
   -------------------------------------------------------------------------- */
void FreeMacroNameParameters(int argc, char *argv[])
{
    if (argc > 0) {
        for (int mp=0; mp<argc; mp++) {
            free(argv[mp]);
        }
    }
}


/* --------------------------------------------------------------------------
    macro_t *FindMacro (char *identifier, avltree_t *treeptr)

    return pointer to found macro in a symbol tree, otherwise NULL if not found
   -------------------------------------------------------------------------- */
macro_t *
FindMacro (const char *identifier,      /* pointer to macro name to search for */
           const avltree_t * treeptr)   /* pointer to root of AVL tree */
{
    if (treeptr == NULL) {
        return NULL;
    } else {
        return Find (treeptr, identifier, (compfunc_t) compmacroname);
    }
}


/*!
 * \brief Parse macro definition and add it to current module of encountered macros
 * \param csfile
 * \param macroname
 * \param argc
 * \param argv
 */
void
ParseMacro(sourcefile_t *csfile, const char *macroname, int argc, char *argv[])
{
    macro_t *newmacro = CreateMacro (csfile, macroname, argc, argv);

    if (newmacro == NULL)
        return;

    csfile->parsingmacro = true;

    /* if successfully parsed, insert into macro definition tree of current module */
    if (ParseMacroBody(csfile, newmacro) == 1) {
        if (Insert (&csfile->module->macros, newmacro, (compfunc_t) cmpmacronodenames) == 0) {
            ReportError (NULL, Err_Memory);
            FreeMacro(newmacro);
        }
    } else {
        FreeMacro(newmacro);
    }

    csfile->parsingmacro = false;
}


/*!
 * \brief Execute the macro definition and return a new source file instance
 * \details
    A macro mnemonic was identified in current file line parsing.

    Execute the macro definition and return a new source file instance that
    contains the source code generated by the macro.

    The caller of this function may then provide this to pass 1 source code parsing.

 * \param csfile
 * \param macroDef
 * \return Returns NULL if an error occurred while generating source code output.
 */
sourcefile_t *
ExecuteMacro(sourcefile_t *csfile, macro_t *macroDef)
{
    sourcefile_t *macroInstance = NULL;
    const includefile_t *macroSourcecode;

    /* Update the instance ID (counter) of this executing macro */
    macroDef->instanceCounter++;
    UpdateMacroConstant(macroDef, MACRO_INSTANCECOUNTER_NAME, macroDef->instanceCounter);

    /* Expand local macro variables using the macro body name and instance
       counter, as follows: [macro-name] _ \@ _ [VarName] */
    ExpandLocalMacroVariables(macroDef);

    if (!AssignMacroArguments(csfile, macroDef)) {
        /* An error occurred while assigning arguments (from current line) to macro definition */
        return NULL;
    }

    if (writeexpandedmacro == true) {
        WriteMacroCall2ListingFile(csfile, macroDef);
    }

    if ((macroSourcecode = CreateMacroIncludeFile(csfile, macroDef)) == NULL) {
        /* parsing problems or no memory... */
        return NULL;
    } else {
        /* Macro source code generated, generate new "include file" that may be parsed by pass 1 */
        macroInstance = Newfile (csfile->module, csfile, macroDef->macroname);
        if (macroInstance != NULL) {
            /* link to generated macro source code */
            if (macroInstance->mf != NULL) {
                MemfLinkCachedFile (macroInstance->mf, macroSourcecode->filedata, macroSourcecode->filesize);
                macroInstance->includedfile = true;
            }
            macroInstance->macro = macroDef; /* refer to macro definition */
        } else {
            /* Memory exhausted - release just allocated macro source code */
            FreeCachedIncludeFile(macroSourcecode);
        }
    }

    return macroInstance;
}
