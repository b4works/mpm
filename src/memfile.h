/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#ifndef MPM_MEMFILE_H
#define MPM_MEMFILE_H

#include "datastructs.h"    /* core data structures used in Mpm */

/* global functions */
void MemfUngetc(memfile_t *const mf);
void MemfFree (memfile_t *const mf);
void MemfFreeCache (memfile_t *const mf);
unsigned char *MemfTellPtr(const memfile_t *mf);
unsigned char *MemfTellPrevCharPtr(const memfile_t *mf);
unsigned char *MemfCacheHostFile (memfile_t *mf, FILE *fd);
unsigned char *MemfCacheHostFileStream (memfile_t *mf, FILE *fd, size_t totalbytes);
char *MemfGetFilename(const memfile_t *mf);
int MemfLinkCachedFile (memfile_t *const mf, unsigned char *const cachedfile, long totalbytes);
int MemfGetc(memfile_t *const mf);
int MemfPutc(memfile_t *const mf, int c);
int MemfPuts(memfile_t *const mf, const char *str);
int MemfRead(memfile_t *const mf, char *buf, size_t len);
int MemfSeek(memfile_t *mf, long fptr);
int MemfSeekPtr(memfile_t *mf, const unsigned char *ptr);
int MemfSeekEnd(memfile_t *mf);
int MemfWrite(memfile_t *const mf, const char *buf, size_t len);
int MemfEof(const memfile_t *mf);
long MemfTell(const memfile_t *mf);

memfile_t *MemfNew (const char *filename, long initialsize, long resizeblock);

#define MFEOF -1

#endif

