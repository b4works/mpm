/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#if ! defined MPM_MODULES_HEADER_
#define MPM_MODULES_HEADER_

#include <stdint.h>
#include <stdio.h>
#include "datastructs.h"    /* base symbol data structures and routines that manages a symbol table */

/* global variables */
extern modules_t *modules;

/* global functions */
int LinkObjfileModule (module_t *cmodule, memfile_t *mobjf, const long fptr_base);
bool ExistBinFile(void);
module_t *NewModule (void);
void ReferenceGlobalSymbols(module_t *cmodule);
void ReferenceLibrarySymbols(module_t *cmodule);
void CreateBinFile (void);
void CreateCrc32File(void);
void CreateSha256File(void);
void CreateIntelHexFile(void);
void WriteGlobalDefFile (void);
void DefineOrigin (void);
void LoadModules (void);
void LinkModules (void);
void ReleaseLinkInfo (void);
void ReleaseModules (void);
void WriteXdefLabel (const symbol_t *node);
void WriteBuffer2HostFile (char *filename, const char *mode, const unsigned char *buffer, size_t length);
void WriteMapFile (void);
void WriteGlobalDefFile (void);

#endif
