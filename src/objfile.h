/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#if ! defined MPM_OBJFILE_HEADER_
#define MPM_OBJFILE_HEADER_

#include "datastructs.h"    /* base symbol data structures and routines that manages a symbol table */

/* MPM object file watermark and length */
#define MPMOBJECTVERSION "03"
#define MPMOBJECTHEADER  "MPMRMF" MPMOBJECTVERSION
#define SIZEOF_MPMOBJHDR 8

/* MPM object & library file watermark have always same length and version */
#define MPMLIBRARYHEADER "MPMLMF" MPMOBJECTVERSION
#define SIZEOF_MPMLIBHDR 8

int ObjFileWaterMark(const char *watermark);
int LibFileWaterMark(const char *watermark);
void MemObjFileFlush(const memfile_t *mf);
void MemObjFileWriteExpr (expression_t *pfixexpr, const unsigned char exprtype);
void MemObjFileWriteHeader(sourcefile_t *csfile, memfile_t *mobjf, size_t moduleCodesize);
void MemObjFileWriteLong (memfile_t *mobjf, long int32);
void MemObjFileReadString (memfile_t *mobjf, char *const string);
void MemObjFileReadSymbols (module_t *curmodule, memfile_t *mobjf, size_t orgaddr, long nextname, long endnames);
long MemObjFileReadLong (memfile_t *mf);
bool ObjFileExpressionRange(sourcefile_t *csfile, long exprvalue, unsigned char exprtype);
memfile_t *CacheObjFile(char *filename, const pathlist_t *pathlist);
memfile_t *MemObjFileOpen(char *filename, const char **objwatermark, int *objlevel);
memfile_t *GetExprMemObjfile(const expression_t *pfixexpr);
memfile_t *MemObjFileCreate(char *objfname);
memfile_t *MemObjFileLoadExpr(sourcefile_t *cfile, memfile_t *mobjf, int objfilelevel, unsigned char *type, symvalue_t *pc, size_t *patchcodeidx, long *nextexpr);

#endif
