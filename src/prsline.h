/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#if ! defined MPM_PRSLINE_HEADER_
#define MPM_PRSLINE_HEADER_

#include "datastructs.h"    /* base symbol data structures and routines that manages a symbol table */

/* global variables */
extern const char separators[];
extern char *line;

/* global functions */
mnemprsrfunc LookupCpuMnemonic(enum symbols st, const char *id);
void SetCpuAssembler(enum cpu cpuid, lineparserfunc cpulineparser, cpumnemfunc cpumnemlookup);
void ParseCpuSymbol (sourcefile_t *csfile, const bool interpret);
void GetLine (sourcefile_t *csfile, char * const linebuf, const unsigned char *);
char *trim(char *str);
char *substr(char *s, const char *find);
char *strclone (const char *s);
int AllocateTextBuffers(void);
int strnicmp (const char *s1, const char *s2, size_t n);
int ltoasc(long num, char* str, int len, int base);
int strrpl(const char *line, const char *search, const char *replace);
int GetChar (sourcefile_t *csfile);
int fGetChar (FILE *fptr);
void FreeTextBuffers(void);
void ParseLine (sourcefile_t *csfile, bool interpret);
void SkipLine (sourcefile_t *csfile);
void fSkipLine (FILE *fptr, bool *cstylecomment);
long GetConstant (const sourcefile_t *csfile, char *evalerr);
enum symbols GetSym (sourcefile_t *csfile);
enum cpu CpuAssembler(void);

#endif
