/***************************************************************************************************
 *
 *  MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *   MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *   MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *   MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMM       MMMM     PPPP              MMMM       MMMM
 *  MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ***************************************************************************************************/


#include <stdlib.h>
#include <string.h>
#include "errors.h"
#include "prsline.h"
#include "section.h"


/******************************************************************************
                          Global variables
 ******************************************************************************/



/******************************************************************************
                          Local functions and variables
 ******************************************************************************/

static section_t *AllocSectionInfo (void);
static void FreeSectionInfo (section_t *section);
static section_t *ReAllocSection (section_t *section, int newsize);


static void
FreeSectionInfo (section_t *section)
{
    if (section != NULL) {
        if (section->area != NULL) free(section->area);
        if (section->name != NULL) free(section->name);
        free(section);
    }
}


static section_t *
ReAllocSection (section_t *section, int newsize)
{
    section_t *newsection = NULL;

    if (section != NULL) {
        /* allocate a new section, increasing section size */
        newsection = NewSection(section->name, newsize);
        if (newsection != NULL) {
            /* migrate old section content into new */
            newsection->pc = section->pc;
            newsection->sizelimit = newsize;
            memcpy (newsection->area, section->area, (size_t) section->pc);
            FreeSectionInfo(section);
        }
    }

    return newsection;
}


static section_t *
AllocSectionInfo (void)
{
    section_t *newsection = (section_t *) malloc (sizeof (section_t));

    if (newsection != NULL) {
        newsection->area = NULL;
        newsection->name = NULL;
        newsection->pc = 0;
        newsection->sizelimit = 0;
    }

    return newsection;
}



/******************************************************************************
                               Public functions
 ******************************************************************************/


/*!
 * \brief Initialize a new section for compilation data of specified name, size
 * \param name the section name
 * \param size initial size of section code space
 * \return pointer to allocated section or NULL allocation failed
 */
section_t *
NewSection (const char *name, const int size)
{
    section_t *newsection = AllocSectionInfo();

    if (size <= 0 || name == NULL) {
        /* protect against incorrect arguments */
        if (newsection != NULL) free(newsection);
        newsection = NULL;
    }

    if (newsection != NULL) {
        newsection->sizelimit = size;

        newsection->area = (unsigned char *) calloc ((size_t) size, sizeof(unsigned char));
        if (newsection->area == NULL) {
            FreeSectionInfo(newsection);
            return NULL;
        }

        newsection->name = strclone(name);
        if (newsection->name == NULL) {
            FreeSectionInfo(newsection);
            return NULL;
        }
    }

    return newsection;
}


/*!
 * \brief Allocated memory to section is released to system
 * \param section
 */
void
FreeSection(section_t **section)
{
    if (*section != NULL) {
        FreeSectionInfo(*section);
        *section = NULL;
    }
}


/*!
 * \brief Write (append) byte to specified section
 * \details
 * If section code space is exhausted when byte is appended,
 * it is automatically expanded and pointer to section is updated.
 *
 * \param section by reference pointer to specified section
 * \param byte
 */
void
WriteByte(section_t **section, unsigned char byte)
{
    section_t *newsection;

    if (*section != NULL) {
        if ( (*section)->pc >= (*section)->sizelimit) {
            /* section space is exhausted, increase and re-allocate space for it */
            newsection = ReAllocSection (*section, (*section)->sizelimit + DEFAULTSECTIONSIZE);
            if (newsection != NULL) {
                *section = newsection; /* let caller get updated pointer to section */
            } else {
                ReportError (NULL, Err_Memory); /* report exhausted memory failure */
                return;
            }
        }

        /* Add byte to section */
        (*section)->area[(*section)->pc++] = byte;
    }
}


/*!
 * \brief Write (append) 16bit word in Least Significant Byte format to specified section
 * \param section
 * \param w
 */
void
WriteWord(section_t **section, unsigned short w)
{
    WriteByte(section, w & 0xFF);
    WriteByte(section, w >> 8);
}


/*!
 * \brief Write (append) 16bit word in Most Significant Byte format to specified section
 * \param section
 * \param w
 */
void
WriteWordMSB(section_t **section, unsigned short w)
{
    WriteByte(section, w >> 8);
    WriteByte(section, w & 0xFF);
}


/*!
 * \brief Write (append) 24bit integer in Least Significant Byte format to specified section
 * \param section
 * \param w
 */
void
WriteInt24(section_t **section, size_t lw)
{
    WriteByte(section, lw & 0xFF);
    WriteByte(section, (lw >> 8) & 0xFF);
    WriteByte(section, (lw >> 16) & 0xFF);
}


/*!
 * \brief Write (append) 24bit integer in Most Significant Byte format to specified section
 * \param section
 * \param w
 */
void
WriteInt24MSB(section_t **section, size_t lw)
{
    WriteByte(section, (lw >> 16) & 0xFF);
    WriteByte(section, (lw >> 8) & 0xFF);
    WriteByte(section, lw & 0xFF);
}


/*!
 * \brief Write (append) 32bit integer in Least Significant Byte format to specified section
 * \param section
 * \param w
 */
void
WriteInt32(section_t **section, size_t lw)
{
    WriteByte(section, lw & 0xFF);
    WriteByte(section, (lw >> 8) & 0xFF);
    WriteByte(section, (lw >> 16) & 0xFF);
    WriteByte(section, (lw >> 24) & 0xFF);
}


/*!
 * \brief Write (append) 32bit integer in Most Significant Byte format to specified section
 * \param section
 * \param w
 */
void
WriteInt32MSB(section_t **section, size_t lw)
{
    WriteByte(section, (lw >> 24) & 0xFF);
    WriteByte(section, (lw >> 16) & 0xFF);
    WriteByte(section, (lw >> 8) & 0xFF);
    WriteByte(section, lw & 0xFF);
}


/*!
 * \brief Get byte at specified code index in specified section
 * \param section
 * \param codeidx
 * \return
 * byte at specified index in section or 0 if index out of range or section is unavailable
 */
unsigned char
GetByte (const section_t *section, int codeidx)
{
    unsigned char byte = 0;

    if (section != NULL && codeidx >= 0 && codeidx < section->sizelimit) {
        byte = section->area[codeidx];
    }

    return byte;
}


/*!
 * \brief Update byte at specified code index in specified section
 * \param section
 * \param codeidx
 * \param byte
 */
void
UpdateByte (const section_t *section, int codeidx, unsigned char byte)
{
    if (section != NULL && codeidx >= 0 && codeidx < section->sizelimit) {
        section->area[codeidx] = byte;
    }
}


/*!
 * \brief Update 16bit word in Least Significant Byte format at specified code index in specified section
 * \param section
 * \param codeidx
 * \param w
 */
void
UpdateWord (const section_t *section, int codeidx, unsigned short w)
{
    UpdateByte(section, codeidx, w & 0xFF);
    UpdateByte(section, codeidx+1, w >> 8);
}


/*!
 * \brief Update 16bit word in Most Significant Byte format at specified code index in specified section
 * \param section
 * \param codeidx
 * \param w
 */
void
UpdateWordMSB (const section_t *section, int codeidx, unsigned short w)
{
    UpdateByte(section, codeidx, w >> 8);
    UpdateByte(section, codeidx+1, w & 0xFF);
}


/*!
 * \brief Update 24bit integer in Least Significant Byte format at specified code index in specified section
 * \param section
 * \param codeidx
 * \param w
 */
void
UpdateInt24 (const section_t *section, int codeidx, size_t lw)
{
    UpdateByte(section, codeidx, lw & 0xFF);
    UpdateByte(section, codeidx+1, (lw >> 8) & 0xFF);
    UpdateByte(section, codeidx+2, (lw >> 16) & 0xFF);
}


/*!
 * \brief Update 24bit integer in Most Significant Byte format at specified code index in specified section
 * \param section
 * \param codeidx
 * \param w
 */
void
UpdateInt24MSB (const section_t *section, int codeidx, size_t lw)
{
    UpdateByte(section, codeidx, (lw >> 16) & 0xFF);
    UpdateByte(section, codeidx+1, (lw >> 8) & 0xFF);
    UpdateByte(section, codeidx+2, lw & 0xFF);
}


/*!
 * \brief Update 32bit integer in Least Significant Byte format at specified code index in specified section
 * \param section
 * \param codeidx
 * \param w
 */
void
UpdateInt32 (const section_t *section, int codeidx, size_t lw)
{
    UpdateByte(section, codeidx, lw & 0xFF);
    UpdateByte(section, codeidx+1, (lw >> 8) & 0xFF);
    UpdateByte(section, codeidx+2, (lw >> 16) & 0xFF);
    UpdateByte(section, codeidx+3, (lw >> 24) & 0xFF);
}


/*!
 * \brief Update 32bit integer in Most Significant Byte format at specified code index in specified section
 * \param section
 * \param codeidx
 * \param w
 */
void
UpdateInt32MSB (const section_t *section, int codeidx, size_t lw)
{
    UpdateByte(section, codeidx, (lw >> 24) & 0xFF);
    UpdateByte(section, codeidx+1, (lw >> 16) & 0xFF);
    UpdateByte(section, codeidx+2, (lw >> 8) & 0xFF);
    UpdateByte(section, codeidx+3, lw & 0xFF);
}


/*!
 * \brief Write (append) code block to specified section
 * \details
 * If section code space would be exhausted when block is to be appended,
 * it is automatically pre-expanded with updated pointer to section.
 * \param section
 * \param code pointer to block to copy
 * \param codesize size of code to copy
 * \return if successfull, return codesize written, otherwise 0 (no memory to expand section)
 */
int
WriteBlock(section_t **section, const unsigned char *code, int codesize)
{
    section_t *newsection;

    if (*section == NULL) {
        return 0;
    }

    if ( (*section)->pc+codesize >= (*section)->sizelimit ) {
        /* section space cannot contain block size, re-allocate space for it and leave room for growth */
        newsection = ReAllocSection (*section, (*section)->sizelimit + codesize + DEFAULTSECTIONSIZE);
        if (newsection != NULL) {
            *section = newsection; /* let caller get updated pointer to section */
        } else {
            ReportError (NULL, Err_Memory); /* report exhausted memory failure */
            return 0;
        }
    }

    /* Append code block to section */
    memcpy( (*section)->area + (*section)->pc, code, (size_t) codesize);
    (*section)->pc += codesize;

    return codesize;
}


/*!
 * \brief Write (append) file block to specified section
 * \details
 * If section code space would be exhausted when block is to be appended,
 * it is automatically pre-expanded with updated pointer to section.
 * \param section
 * \param binfile opened file handle
 * \param codesize expected size of code to load into section
 * \return
 * returns size of block loaded into section or 0 if memory allocation error
 * (might be less than expected size if I/O error occurred)
 */
int
WriteFileBlock(section_t **section, FILE *binfile, int codesize)
{
    section_t *newsection;
    size_t blocksize = 0;

    if (*section != NULL) {
        if ( (*section)->pc+codesize >= (*section)->sizelimit ) {
            /* section space cannot contain block size, re-allocate space for it and leave room for growth */
            newsection = ReAllocSection (*section, (*section)->sizelimit + codesize + DEFAULTSECTIONSIZE);
            if (newsection != NULL) {
                *section = newsection; /* let caller get updated pointer to section */
            } else {
                ReportError (NULL, Err_Memory); /* report exhausted memory failure */
                return (int) blocksize;
            }
        }

        /* Append file block to section */
        blocksize = fread ((*section)->area + (*section)->pc, sizeof (char), (size_t) codesize, binfile);
        (*section)->pc += (int) blocksize;
    }

    return (int) blocksize;
}
