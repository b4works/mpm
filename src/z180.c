/****************************************************************************************************
 *
 *   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 *                         ZZZZZZZZZZZZZZ        1111       888888888888        000000000
 *                       ZZZZZZZZZZZZZZ          1111     8888888888888888    0000000000000
 *                               ZZZZ          111111     8888        8888  0000         0000
 *                             ZZZZ              1111       888888888888    0000         0000
 *                           ZZZZ                1111     8888        8888  0000         0000
 *                         ZZZZZZZZZZZZZZ      11111111   8888888888888888    0000000000000
 *                       ZZZZZZZZZZZZZZ        11111111     888888888888        000000000
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************************************/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "config.h"
#include "datastructs.h"
#include "exprprsr.h"
#include "asmdrctv.h"
#include "errors.h"
#include "pass.h"
#include "prsline.h"
#include "memfile.h"
#include "z80.h"
#include "z180.h"


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/


/* ------------------------------------------------------------------------------
   Pre-sorted array of Z180 instruction mnemonics (includes Z80 mnemonics and Z80 meta instructions).
   ------------------------------------------------------------------------------ */
static idf_t z180idents[] = {
    {"ADC", {.mpf = ADC}},
    {"ADD", {.mpf = ADD}},
    {"AND", {.mpf = AND}},
    {"BIT", {.mpf = BIT}},
    {"CALL", {.mpf = CALL}},
    {"CCF", {.cmpf = CCF}},
    {"CP", {.mpf = CP}},
    {"CPD", {.cmpf = CPD}},
    {"CPDR", {.cmpf = CPDR}},
    {"CPI", {.cmpf = CPI}},
    {"CPIR", {.cmpf = CPIR}},
    {"CPL",  {.cmpf = CPL}},
    {"DAA", {.cmpf = DAA}},
    {"DEC", {.mpf = DEC}},
    {"DI", {.cmpf = DI}},
    {"DJNZ", {.mpf = DJNZ}},
    {"EI", {.cmpf = EI}},
    {"EX", {.mpf = EX}},
    {"EXX", {.cmpf = EXX}},
    {"HALT", {.cmpf = HALT}},
    {"IM", {.mpf = IM}},
    {"IN", {.mpf = IN}},
    {"IN0", {.mpf = IN0}},
    {"INC", {.mpf = INC}},
    {"IND", {.cmpf = IND}},
    {"INDR", {.cmpf = INDR}},
    {"INI", {.cmpf = INI}},
    {"INIR", {.cmpf = INIR}},
    {"JP", {.mpf = JP}},
    {"JR", {.mpf = JR}},
    {"LD", {.mpf = LD}},
    {"LDD", {.cmpf = LDD}},
    {"LDDR", {.cmpf = LDDR}},
    {"LDI", {.cmpf = LDI}},
    {"LDIR", {.cmpf = LDIR}},
    {"MLT", {.mpf = MLT}},
    {"NEG", {.cmpf = NEG}},
    {"NOP", {.cmpf = NOP}},
    {"OR", {.mpf = OR}},
    {"OTDM", {.cmpf = OTDM}},
    {"OTDMR", {.cmpf = OTDMR}},
    {"OTDR", {.cmpf = OTDR}},
    {"OTIM", {.cmpf = OTIM}},
    {"OTIMR", {.cmpf = OTIMR}},
    {"OTIR", {.cmpf = OTIR}},
    {"OUT", {.mpf = OUT}},
    {"OUT0", {.mpf = OUT0}},
    {"OUTD", {.cmpf = OUTD}},
    {"OUTI", {.cmpf = OUTI}},
    {"POP", {.mpf = POP}},
    {"PUSH", {.mpf = PUSH}},
    {"RES", {.mpf = RES}},
    {"RET", {.mpf = RET}},
    {"RETI", {.cmpf = RETI}},
    {"RETN", {.cmpf = RETN}},
    {"RL", {.mpf = RL}},
    {"RLA", {.cmpf = RLA}},
    {"RLC", {.mpf = RLC}},
    {"RLCA", {.cmpf = RLCA}},
    {"RLD", {.cmpf = RLD}},
    {"RLW", {.mpf = RLW}},      /* Z80 meta instruction */
    {"RR", {.mpf = RR}},
    {"RRA", {.cmpf = RRA}},
    {"RRC", {.mpf = RRC}},
    {"RRCA", {.cmpf = RRCA}},
    {"RRD", {.cmpf = RRD}},
    {"RRW", {.mpf = RRW}},      /* Z80 meta instruction */
    {"RST", {.mpf = RST}},
    {"SBC", {.mpf = SBC}},
    {"SCF", {.cmpf = SCF}},
    {"SET", {.mpf = SET}},
    {"SLA", {.mpf = SLA}},
    {"SLAW", {.mpf = SLAW}},    /* Z80 meta instruction */
    {"SLL", {.mpf = SLL}},
    {"SLP", {.cmpf = SLP}},
    {"SRA", {.mpf = SRA}},
    {"SRAW", {.mpf = SRAW}},    /* Z80 meta instruction */
    {"SRL", {.mpf = SRL}},
    {"SRLW", {.mpf = SRLW}},    /* Z80 meta instruction */
    {"SUB", {.mpf = SUB}},
    {"SUBW", {.mpf = SUBW}},    /* Z80 meta instruction */
    {"TST", {.mpf = TST}},
    {"TSTIO", {.mpf = TSTIO}},
    {"XOR", {.mpf = XOR}}
};


static void
_TST (sourcefile_t *csfile)
{
    enum z80r reg;

    if (GetSym(csfile) == lparen) {
        if (CheckIndirectAddrMnem (csfile) == z80ia_hl) {
            StreamCodeByte (csfile, 0xED);
            StreamCodeByte (csfile, 0x34); /* xxx  A,(HL) */
        } else {
            /* only (hl) is allowed, the rest indirect registers are illegal */
            ReportError (csfile, Err_IllegalIdent);
        }
    } else {
        /* no indirect addressing, try to get an 8bit register */
        switch (reg = CheckReg8Mnem (csfile)) {
            case z80r_unknown:
                /* 8bit register wasn't found, try to evaluate an expression */
                StreamCodeByte (csfile, 0xED);
                StreamCodeByte (csfile, 0x64); /* xxx  A,n */
                ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 2);
                break;

            case z80r_b:
            case z80r_c:
            case z80r_d:
            case z80r_e:
            case z80r_h:
            case z80r_l:
            case z80r_f:
            case z80r_a:
                StreamCodeByte (csfile, 0xED);
                StreamCodeByte (csfile, (unsigned char) (reg * 8 | 0x04)); /* xxx  A,r */
                break;

            default:
                /* illegal 8bit register combinations */
                ReportError (csfile, Err_IllegalIdent);
                break;
        }
    }
}



/******************************************************************************
                               Public functions
 ******************************************************************************/

/*!
 * \brief Return pointer to function that parses Z180 mnemonic source line and generates code
 * \param st symbol type
 * \param id pointer to identifier
 * \return pointer to mnemonic parser function, or NULL if not found
 */
mnemprsrfunc
LookupZ180Mnemonic(enum symbols st, const char *id)
{
    return LookupIdentifier (id, st, (identfunc_t *) z180idents, sizeof(z180idents)/sizeof(idf_t));
}


void
MLT (sourcefile_t *csfile)
{
    enum z80rr reg16;

    GetSym(csfile);
    switch (reg16 = CheckReg16Mnem (csfile)) {
        case z80r_bc:
        case z80r_de:
        case z80r_hl:
        case z80r_sp:
            StreamCodeByte (csfile, 0xED);
            StreamCodeByte (csfile, (unsigned char) (16 * reg16) | 0x4c);
            break;

        default:
            /* incorrect 16 bit register, or not found */
            ReportError (csfile, Err_IllegalIdent);
            break;
    }
}


/*
 * Allow specification of "TST [A,]xxx"
 */
void
TST (sourcefile_t *csfile)
{
    const unsigned char *ptr = MemfTellPtr (csfile->mf);

    GetSym(csfile);
    if (csfile->sym == name && CheckReg8Mnem (csfile) == z80r_a && GetSym(csfile) == comma) {
        /* <instr> A, ... */
        _TST(csfile);
        return;
    }

    /* reparse and code generate (if possible) */
    csfile->sym = nil;
    csfile->eol = false;

    MemfSeekPtr(csfile->mf, ptr);
    _TST (csfile);
}


void
TSTIO (sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x74);
    GetSym(csfile);
    ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 2);
}


void
IN0 (sourcefile_t *csfile)
{
    enum z80r inreg;

    if (GetSym(csfile) != name) {
        ReportError (csfile, Err_Syntax);
    }

    switch (inreg = CheckReg8Mnem (csfile)) {
        case z80r_b:
        case z80r_c:
        case z80r_d:
        case z80r_e:
        case z80r_h:
        case z80r_l:
        case z80r_f:
        case z80r_a:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                break;
            }
            if (GetSym(csfile) != lparen) {
                ReportError (csfile, Err_Syntax);
                break;
            }

            GetSym(csfile);
            StreamCodeByte (csfile, 0xED);
            StreamCodeByte (csfile, (unsigned char) inreg * 8);
            if (ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 2) && csfile->sym != rparen) {
                ReportError (csfile, Err_Syntax);
            }
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            break;
    }
}


void
OUT0 (sourcefile_t *csfile)
{
    enum z80r inreg;
    int codeidx;

    if (GetSym(csfile) != lparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    GetSym(csfile);
    StreamCodeByte (csfile, 0xED);
    codeidx = GetStreamCodePC(csfile); /* remember patch index */
    StreamCodeByte (csfile, 0);        /* opcode will be updated when the register mnemonic is parsed below */
    if (!ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 2)) {
        return;
    }

    if (csfile->sym != rparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (GetSym(csfile) == comma) {
        GetSym(csfile);
        switch (inreg = CheckReg8Mnem (csfile)) {
            case z80r_b:
            case z80r_c:
            case z80r_d:
            case z80r_e:
            case z80r_h:
            case z80r_l:
            case z80r_f:
            case z80r_a:
                UpdateStreamCodeByte (csfile, codeidx, (int) (inreg * 8 | 1));
                return;

            default:
                ReportError (csfile, Err_IllegalIdent);
                return;
        }
    }

    ReportError (csfile, Err_Syntax);
}


void
SLP (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x76);
}


void
OTDM (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x8b);
}


void
OTDMR (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x9b);
}


void
OTIM (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x83);
}


void
OTIMR (const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xED);
    StreamCodeByte (csfile, 0x93);
}
