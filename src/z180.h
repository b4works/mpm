/****************************************************************************************************
 *
 *   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 *                         ZZZZZZZZZZZZZZ        1111       888888888888        000000000
 *                       ZZZZZZZZZZZZZZ          1111     8888888888888888    0000000000000
 *                               ZZZZ          111111     8888        8888  0000         0000
 *                             ZZZZ              1111       888888888888    0000         0000
 *                           ZZZZ                1111     8888        8888  0000         0000
 *                         ZZZZZZZZZZZZZZ      11111111   8888888888888888    0000000000000
 *                       ZZZZZZZZZZZZZZ        11111111     888888888888        000000000
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************************************/


#if ! defined MPM_Z180_HEADER_
#define MPM_Z180_HEADER_

#include "datastructs.h"

/* globally available functions */

mnemprsrfunc LookupZ180Mnemonic(enum symbols st, const char *id);
void MLT (sourcefile_t *csfile);
void TST (sourcefile_t *csfile);
void TSTIO (sourcefile_t *csfile);
void IN0 (sourcefile_t *csfile);
void OUT0 (sourcefile_t *csfile);
void SLP (const sourcefile_t *csfile);
void OTDM (const sourcefile_t *csfile);
void OTDMR (const sourcefile_t *csfile);
void OTIM (const sourcefile_t *csfile);
void OTIMR (const sourcefile_t *csfile);

#endif
