﻿/****************************************************************************************************
 *
 *   MMMMM       MMMMM   PPPPPPPPPPPPP     MMMMM       MMMMM
 *    MMMMMM   MMMMMM     PPPPPPPPPPPPPPP   MMMMMM   MMMMMM
 *    MMMMMMMMMMMMMMM     PPPP       PPPP   MMMMMMMMMMMMMMM
 *    MMMM MMMMM MMMM     PPPPPPPPPPPP      MMMM MMMMM MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *    MMMM       MMMM     PPPP              MMMM       MMMM
 *   MMMMMM     MMMMMM   PPPPPP            MMMMMM     MMMMMM
 *
 *                         ZZZZZZZZZZZZZZ    3333333333      888888888888        000000000
 *                       ZZZZZZZZZZZZZZ    33333333333333  8888888888888888    0000000000000
 *                               ZZZZ                3333  8888        8888  0000         0000
 *                             ZZZZ           333333333      888888888888    0000         0000
 *                           ZZZZ                    3333  8888        8888  0000         0000
 *                         ZZZZZZZZZZZZZZ  33333333333333  8888888888888888    0000000000000
 *                       ZZZZZZZZZZZZZZ      3333333333      888888888888        000000000
 *
 * Copyright (C) 1991-2024, Gunther Strube, hello@bits4fun.net
 *
 * This file is part of Mpm.
 * Mpm is free software; you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation - either version 2, or (at your option)
 * any later version.
 * Mpm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Mpm -
 * see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************************************/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "config.h"
#include "options.h"
#include "exprprsr.h"
#include "asmdrctv.h"
#include "pass.h"
#include "errors.h"
#include "prsline.h"
#include "memfile.h"
#include "z80.h"
#include "z180.h"
#include "z380.h"


/******************************************************************************
                          Local functions and variables
 ******************************************************************************/

static void DDIRinstr(const sourcefile_t *csfile, enum ddirmodes dm);
static void ArithLog8bitZ380 (sourcefile_t *csfile, int opcode);
static void ArithLog8bitIndrctZ380(sourcefile_t *csfile, int opcode, enum ddirmodes ddirmode);
static void AddSubSPnn(sourcefile_t *csfile, int opcode, enum ddirmodes ddirmode);
static void AddSubHLnn(sourcefile_t *csfile, int opcode, enum ddirmodes ddirmode);
static void ADCSBC_z380_ (sourcefile_t *csfile, int opc8bitreg, int opc16bitreg);
static void ADC_z380_ (sourcefile_t *csfile);
static void SBC_z380_ (sourcefile_t *csfile);
static void AND_z380_ (sourcefile_t *csfile);
static void OR_z380_ (sourcefile_t *csfile);
static void XOR_z380_ (sourcefile_t *csfile);
static void CP_z380_ (sourcefile_t *csfile);
static void SLL_z380_(const sourcefile_t *csfile);
static void BTEST(const sourcefile_t *csfile);
static void EXALL(const sourcefile_t *csfile);
static void EXXX(const sourcefile_t *csfile);
static void EXXY(const sourcefile_t *csfile);
static void INDW(const sourcefile_t *csfile);
static void INDRW(const sourcefile_t *csfile);
static void INIW(const sourcefile_t *csfile);
static void INIRW(const sourcefile_t *csfile);
static void LDIW(const sourcefile_t *csfile);
static void LDIRW(const sourcefile_t *csfile);
static void LDDW(const sourcefile_t *csfile);
static void LDDRW(const sourcefile_t *csfile);
static void MTEST(const sourcefile_t *csfile);
static void OTDRW(const sourcefile_t *csfile);
static void OTIRW(const sourcefile_t *csfile);
static void OUTA (sourcefile_t *csfile);
static void OUTAW (sourcefile_t *csfile);
static void OUTDW(const sourcefile_t *csfile);
static void OUTIW(const sourcefile_t *csfile);
static void RETB(const sourcefile_t *csfile);
static void CALR(sourcefile_t *csfile);
static void CPLW(sourcefile_t *csfile);
static void DECW(sourcefile_t *csfile);
static void DDIR(const sourcefile_t *csfile);
static void EXTS(sourcefile_t *csfile);
static void EXTSW(sourcefile_t *csfile);
static void INA(sourcefile_t *csfile);
static void INAW(sourcefile_t *csfile);
static void INCW(sourcefile_t *csfile);
static void INW(sourcefile_t *csfile);
static void NEGW(sourcefile_t *csfile);
static void OUT_z380_ (sourcefile_t *csfile);
static void OUTW(sourcefile_t *csfile);
static void SETC(sourcefile_t *csfile);
static void RESC(sourcefile_t *csfile);
static void SWAP (sourcefile_t *csfile);
static void PUSH_z380_ (sourcefile_t *csfile);
static void POP_z380_ (sourcefile_t *csfile);
static void PUSHPOP_z380_ (sourcefile_t *csfile, int opcode);
static void CallJpZ380 (sourcefile_t *csfile, int opcodenn, int opcodecc);
static void JP_z380_(sourcefile_t *csfile);
static void BitSrcRg8BitIndrct(sourcefile_t *csfile, int opcode, enum ddirmodes ddirmode);
static void ArLogInstrW(sourcefile_t *csfile, unsigned int opcodemask);
static void ArLogInstrIxIyW(sourcefile_t *csfile, unsigned int opcodemask, enum ddirmodes ddirmode);
static void ShiftRotInstrZ380 (sourcefile_t *csfile, int opcode);
static void ShiftRotInstrW(sourcefile_t *csfile, unsigned int opcodemask);
static void ShiftRotHlIxIyW(sourcefile_t *csfile, unsigned int opcodemask, enum ddirmodes ddirmode);
static void DivMultInstrIxIyW(sourcefile_t *csfile, unsigned int opcodemask, enum ddirmodes ddirmode);
static void DivMultW(sourcefile_t *csfile, unsigned int opcodemask);
static void IncDecIndrct8bitZ380 (sourcefile_t *csfile, int opcode, enum ddirmodes ddirmode);
static void IncDec_z380_(sourcefile_t *csfile, int opc8, int opc16);
static void DEC_z380_ (sourcefile_t *csfile);
static void INC_z380_ (sourcefile_t *csfile);
static void ADCW(sourcefile_t *csfile);
static void ADDW(sourcefile_t *csfile);
static void ANDW(sourcefile_t *csfile);
static void CALL_z380_(sourcefile_t *csfile);
static void CPW(sourcefile_t *csfile);
static void DIVUW(sourcefile_t *csfile);
static void LD_z380_8bitdstreg_indrct (sourcefile_t *csfile, enum ddirmodes ddirmode, enum z80r destreg);
static void LD_z380_index8bit_indrct (sourcefile_t *csfile, expression_t *idxoffsetexpr, enum z80ia destreg, enum ddirmodes ddirmode);
static void LD_z380_address_indrct (sourcefile_t *csfile, enum ddirmodes ddirmode);
static void LD_z380_16bitDestReg_SrcindDirect(sourcefile_t *csfile, enum ddirmodes ddirmode, enum z80rr dstreg16);
static void LD_z380_16bitDestReg_IndirectIXIYSP(sourcefile_t *csfile, enum ddirmodes ddirmode, enum z80rr dstreg16, enum z80ia srcreg16, int opcode);
static void LD_z380_IndirectIXIYSP_16bitSrcReg(sourcefile_t *csfile, enum ddirmodes ddirmode, expression_t *idxoffsetexpr, enum z80ia dstreg16, enum z80rr srcreg16, int opcode);
static void LD_z380_16bitDestReg_SrcDirect (sourcefile_t *csfile, enum z80rr destreg, enum ddirmodes ddirmode);
static void LD_z380_(sourcefile_t *csfile);
static void MULTUW(sourcefile_t *csfile);
static void MULTW(sourcefile_t *csfile);
static void ORW(sourcefile_t *csfile);
static void SBCW(sourcefile_t *csfile);
static void SUBW_z380_(sourcefile_t *csfile);
static void XORW(sourcefile_t *csfile);
static void RLCW(sourcefile_t *csfile);
static void RRCW(sourcefile_t *csfile);
static void RLW_z380_(sourcefile_t *csfile);
static void RRW_z380_(sourcefile_t *csfile);
static void SLAW_z380_(sourcefile_t *csfile);
static void SRAW_z380_(sourcefile_t *csfile);
static void SRLW_z380_(sourcefile_t *csfile);
static void LDCTL(sourcefile_t *csfile);
static void ADD_z380_(sourcefile_t *csfile);
static void SUB_z380_(sourcefile_t *csfile);
static void IM_z380_ (sourcefile_t *csfile);
static void DI_z380_(sourcefile_t *csfile);
static void EI_z380_(sourcefile_t *csfile);
static void EX_z380_(sourcefile_t *csfile);
static void DJNZ_z380_(sourcefile_t *csfile);
static void JR_z380_(sourcefile_t *csfile);
static void BIT_z380_(sourcefile_t *csfile);
static void RES_z380_(sourcefile_t *csfile);
static void SET_z380_(sourcefile_t *csfile);
static void RLC_z380_ (sourcefile_t *csfile);
static void RRC_z380_ (sourcefile_t *csfile);
static void RL_z380_ (sourcefile_t *csfile);
static void RR_z380_ (sourcefile_t *csfile);
static void SLA_z380_ (sourcefile_t *csfile);
static void SRA_z380_ (sourcefile_t *csfile);
static void SRL_z380_ (sourcefile_t *csfile);
static void LDW(sourcefile_t *csfile);
static void LDW_16bitregIndrct(sourcefile_t *csfile, enum ddirmodes ddirmode);
static void LDW_dst_bcdehl_indrct(sourcefile_t *csfile, enum z80ia dstreg, enum ddirmodes ddirmode);
static void DDirImmediateOffset(sourcefile_t *csfile, enum ddirmodes ddirmode, int lstpatchpos);
static void DDirImmediateAddress(sourcefile_t *csfile, expression_t *addrexpr, enum ddirmodes ddirmode, int lstpatchpos);
static enum ddiridents CheckDdirIdent (const sourcefile_t *csfile);
static enum z80rr FetchOptHlReg16Mnem(sourcefile_t *csfile);
static enum ddirmodes GetDecoderDirective(sourcefile_t *csfile);
static enum ddirmodes FetchDdirSuffix(sourcefile_t *csfile);
static enum ctlrr CheckCtlRegMnem (const sourcefile_t *csfile);
static bool FetchOptReg16Mnem(sourcefile_t *csfile, const enum z80rr rr);

/* ------------------------------------------------------------------------------
   Pre-sorted array of Z380 instruction mnemonics (includes Z80+Z180 mnemonics)
   ------------------------------------------------------------------------------ */
static idf_t z380idents[] = {
    {"ADC", {.mpf = ADC_z380_}},     /* Z80, Z380 */
    {"ADCW", {.mpf = ADCW}},         /* Z380 */
    {"ADD", {.mpf = ADD_z380_}},     /* Z80, Z380 */
    {"ADDW", {.mpf = ADDW}},         /* Z380 */
    {"AND", {.mpf = AND_z380_}},     /* Z80, Z380 */
    {"ANDW", {.mpf = ANDW}},         /* Z380 */
    {"BIT", {.mpf = BIT_z380_}},     /* Z80, Z380 */
    {"BTEST", {.cmpf = BTEST}},      /* Z380 */
    {"CALL", {.mpf = CALL_z380_}},   /* Z80, Z380 */
    {"CALR", {.mpf = CALR}},         /* Z380 */
    {"CCF", {.cmpf = CCF}},
    {"CP", {.mpf = CP_z380_}},       /* Z80, Z380 */
    {"CPD", {.cmpf = CPD}},
    {"CPDR", {.cmpf = CPDR}},
    {"CPI", {.cmpf = CPI}},
    {"CPIR", {.cmpf = CPIR}},
    {"CPL", {.cmpf = CPL}},
    {"CPLW", {.mpf = CPLW}},         /* Z380 */
    {"CPW", {.mpf = CPW}},           /* Z380 */
    {"DAA", {.cmpf = DAA}},
    {"DDIR", {.cmpf = DDIR}},         /* Z380 */
    {"DEC", {.mpf = DEC_z380_}},     /* Z80,Z380 */
    {"DECW", {.mpf = DECW}},         /* Z80,Z380 */
    {"DI", {.mpf = DI_z380_}},       /* Z80, Z380 */
    {"DIVUW", {.mpf = DIVUW}},       /* Z380 */
    {"DJNZ", {.mpf = DJNZ_z380_}},   /* Z80, Z380 */
    {"EI", {.mpf = EI_z380_}},       /* Z80, Z380 */
    {"EX", {.mpf = EX_z380_}},       /* Z80, Z380 */
    {"EXALL", {.cmpf = EXALL}},      /* Z380 */
    {"EXTS", {.mpf = EXTS}},         /* Z380 */
    {"EXTSW", {.mpf = EXTSW}},       /* Z380 */
    {"EXX", {.cmpf = EXX}},
    {"EXXX", {.cmpf = EXXX}},        /* Z380 */
    {"EXXY", {.cmpf = EXXY}},        /* Z380 */
    {"HALT", {.cmpf = HALT}},
    {"IM", {.mpf = IM_z380_}},       /* Z80, Z380 */
    {"IN", {.mpf = IN}},
    {"IN0", {.mpf = IN0}},           /* Z180 */
    {"INA", {.mpf = INA}},           /* Z380 */
    {"INAW", {.mpf = INAW}},         /* Z380 */
    {"INC", {.mpf = INC_z380_}},     /* Z80,Z380 */
    {"INCW", {.mpf = INCW}},         /* Z80,Z380 */
    {"IND", {.cmpf = IND}},
    {"INDR", {.cmpf = INDR}},        /* Z80 */
    {"INDRW", {.cmpf = INDRW}},      /* Z380 */
    {"INDW", {.cmpf = INDW}},        /* Z380 */
    {"INI", {.cmpf = INI}},          /* Z80 */
    {"INIR", {.cmpf = INIR}},        /* Z80 */
    {"INIRW", {.cmpf = INIRW}},      /* Z380 */
    {"INIW", {.cmpf = INIW}},        /* Z380 */
    {"INW", {.mpf = INW}},           /* Z380 */
    {"JP", {.mpf = JP_z380_}},       /* Z80, Z380 */
    {"JR", {.mpf = JR_z380_}},       /* Z80, Z380 */
    {"LD", {.mpf = LD_z380_}},       /* Z80, Z380 */
    {"LDCTL", {.mpf = LDCTL}},       /* Z380 */
    {"LDD", {.cmpf = LDD}},
    {"LDDR", {.cmpf = LDDR}},
    {"LDDRW", {.cmpf = LDDRW}},      /* Z380 */
    {"LDDW", {.cmpf = LDDW}},        /* Z380 */
    {"LDI", {.cmpf = LDI}},
    {"LDIR", {.cmpf = LDIR}},
    {"LDIRW", {.cmpf = LDIRW}},      /* Z380 */
    {"LDIW", {.cmpf = LDIW}},        /* Z380 */
    {"LDW", {.mpf = LDW}},           /* Z380 */
    {"MLT", {.mpf = MLT}},           /* Z180 */
    {"MTEST", {.cmpf = MTEST}},      /* Z380 */
    {"MULTUW", {.mpf = MULTUW}},     /* Z380 */
    {"MULTW", {.mpf = MULTW}},       /* Z380 */
    {"NEG", {.cmpf = NEG}},
    {"NEGW", {.mpf = NEGW}},         /* Z380 */
    {"NOP", {.cmpf = NOP}},
    {"OR", {.mpf = OR_z380_}},       /* Z80, Z380 */
    {"ORW", {.mpf = ORW}},           /* Z380 */
    {"OTDM", {.cmpf = OTDM}},        /* Z180 */
    {"OTDMR", {.cmpf = OTDMR}},      /* Z180 */
    {"OTDR", {.cmpf = OTDR}},
    {"OTDRW", {.cmpf = OTDRW}},      /* Z380 */
    {"OTIM", {.cmpf = OTIM}},        /* Z180 */
    {"OTIMR", {.cmpf = OTIMR}},      /* Z180 */
    {"OTIR", {.cmpf = OTIR}},
    {"OTIRW", {.cmpf = OTIRW}},      /* Z380 */
    {"OUT", {.mpf = OUT_z380_}},     /* Z80,Z380 */
    {"OUT0", {.mpf = OUT0}},         /* Z180 */
    {"OUTA", {.mpf = OUTA}},         /* Z380 */
    {"OUTAW", {.mpf = OUTAW}},       /* Z380 */
    {"OUTD", {.cmpf = OUTD}},
    {"OUTDW", {.cmpf = OUTDW}},      /* Z380 */
    {"OUTI", {.cmpf = OUTI}},
    {"OUTIW", {.cmpf = OUTIW}},      /* Z380 */
    {"OUTW", {.mpf = OUTW}},         /* Z380 */
    {"POP", {.mpf = POP_z380_}},     /* Z80,Z380 */
    {"PUSH", {.mpf = PUSH_z380_}},   /* Z80,Z380 */
    {"RES", {.mpf = RES_z380_}},     /* Z80,Z380 */
    {"RESC", {.mpf = RESC}},         /* Z380 */
    {"RET", {.mpf = RET}},
    {"RETB", {.cmpf = RETB}},        /* Z380 */
    {"RETI", {.cmpf = RETI}},
    {"RETN", {.cmpf = RETN}},
    {"RL", {.mpf = RL_z380_}},       /* Z80,Z380 */
    {"RLA", {.cmpf = RLA}},
    {"RLC", {.mpf = RLC_z380_}},     /* Z80,Z380 */
    {"RLCA", {.cmpf = RLCA}},
    {"RLCW", {.mpf = RLCW}},         /* Z380 */
    {"RLD", {.cmpf = RLD}},
    {"RLW", {.mpf = RLW_z380_}},     /* Z380 */
    {"RR", {.mpf = RR_z380_}},       /* Z80,Z380 */
    {"RRA", {.cmpf = RRA}},
    {"RRC", {.mpf = RRC_z380_}},     /* Z80,Z380 */
    {"RRCA", {.cmpf = RRCA}},
    {"RRCW", {.mpf = RRCW}},         /* Z380 */
    {"RRD", {.cmpf = RRD}},
    {"RRW", {.mpf = RRW_z380_}},     /* Z380 */
    {"RST", {.mpf = RST}},
    {"SBC", {.mpf = SBC_z380_}},     /* Z80,Z380 */
    {"SBCW", {.mpf = SBCW}},         /* Z380 */
    {"SCF", {.cmpf = SCF}},
    {"SET", {.mpf = SET_z380_}},     /* Z80,Z380 */
    {"SETC", {.mpf = SETC}},         /* Z380 */
    {"SLA", {.mpf = SLA_z380_}},     /* Z80,Z380 */
    {"SLAW", {.mpf = SLAW_z380_}},   /* Z380 */
    {"SLL", {.cmpf = SLL_z380_}},    /* Z380: EX x,x' instruction group (SLL "undocumented instruction" use same opcodes for Z80) */
    {"SLP", {.cmpf = SLP}},          /* Z180 */
    {"SRA", {.mpf = SRA_z380_}},     /* Z80,Z380 */
    {"SRAW", {.mpf = SRAW_z380_}},   /* Z380 */
    {"SRL", {.mpf = SRL_z380_}},     /* Z80,Z380 */
    {"SRLW", {.mpf = SRLW_z380_}},   /* Z380 */
    {"SUB", {.mpf = SUB_z380_}},     /* Z80, Z380 */
    {"SUBW", {.mpf = SUBW_z380_}},   /* Z380 */
    {"SWAP", {.mpf = SWAP}},         /* Z380 */
    {"TST", {.mpf = TST}},
    {"TSTIO", {.mpf = TSTIO}},       /* Z180 */
    {"XOR", {.mpf = XOR_z380_}},     /* Z80, Z380 */
    {"XORW", {.mpf = XORW}}          /* Z380 */
};


/*!
 * \brief SLL instruction is not supported in Z380, so report an error when used
 * \details The SLL instruction opcode group is replaced by EX r8,r8'
 * \param csfile
 */
static void
SLL_z380_(const sourcefile_t *csfile)
{
    ReportSrcAsmMessage (csfile, "SLL instruction is not supported by Z380 CPU");
}


/*!
 * \brief Identify Z380 DDIR mode mnemonic 'IB','IW','W' or 'LW' identifiers
 * \param csfile
 * \return
 */
static enum ddiridents
CheckDdirIdent (const sourcefile_t *csfile)
{
    if (csfile->sym != name)
        return ddir_unknown;

    switch(csfile->identlen) {
        case 1:
            if (csfile->ident[0] == 'W') {
                return ddir_w;
            }
            break;

        case 2:
            if (csfile->ident[0] == 'I' && csfile->ident[1] == 'B') {
                return ddir_ib;
            }
            if (csfile->ident[0] == 'I' && csfile->ident[1] == 'W') {
                return ddir_iw;
            }
            if (csfile->ident[0] == 'L' && csfile->ident[1] == 'W') {
                return ddir_lw;
            }
            break;

        default:
            break;
    }

    return ddir_unknown;
}


/*!
 * \brief get the Decoder Directive enumerated type
 * \details
 * This routine is used for extended instruction syntax, eg. ld.ib,lw hl,($c01000)
 * or used for independendt DDIR mnemonic. 16bit format:
 *
    wim:
    000 W       Word mode
    001 IB,W    Immediate byte, Word mode
    010 IW,W    Immediate word, Word mode
    011 IB      Immediate byte
    100 LW      Long Word mode
    101 IB,LW   Immediate byte, Long Word mode
    110 IW,LW   Immediate word, Long Word mode
    111 IW      Immediate word

    The two byte structure:
    11w11101 110000im

 * \param csfile
 * \return the enumerated ddirmodes type
 * (ddirmode_unknown if syntax error or unknown DDIR specifier encountered)
 */
static enum ddirmodes
GetDecoderDirective(sourcefile_t *csfile)
{
    const unsigned char *ddircommaptr;

    if (GetSym(csfile) != name) {
        ReportError (csfile, Err_Syntax);
        return ddirmode_unknown;
    }

    switch(CheckDdirIdent(csfile)) {
        case ddir_w:
            return ddirmode_w; /* 11w11101 110000im, wim = 000 */

        case ddir_lw:
            return ddirmode_lw; /* 11w11101 110000im, wim = 100 */

        case ddir_ib: /* 'ib' or 'ib,w' or 'ib,lw' */
            ddircommaptr = MemfTellPtr(csfile->mf);
            if (GetSym(csfile) != comma) {
                MemfSeekPtr(csfile->mf, ddircommaptr);
                return ddirmode_ib; /* 11w11101 110000im, wim = 011 */
            }

            if (GetSym(csfile) == name) {
                switch(CheckDdirIdent(csfile)) {
                    case ddir_w:
                        return ddirmode_ibw; /* 11w11101 110000im, wim = 001 */

                    case ddir_lw:
                        return ddirmode_iblw; /* 11w11101 110000im, wim = 101 */

                    default:
                        ReportError (csfile, Err_IllegalIdent);
                        return ddirmode_unknown;
                }
            }
            break;

        case ddir_iw: /* 'iw' or 'iw,w' or 'iw,lw' */
            ddircommaptr = MemfTellPtr(csfile->mf);

            if (GetSym(csfile) != comma) {
                MemfSeekPtr(csfile->mf, ddircommaptr);
                return ddirmode_iw; /* 11w11101 110000im, wim = 111 */
            }

            if (GetSym(csfile) == name) {
                switch(CheckDdirIdent(csfile)) {
                    case ddir_w:
                        return ddirmode_iww; /* 11w11101 110000im, wim = 010 */

                    case ddir_lw:
                        return ddirmode_iwlw; /* 11w11101 110000im, wim = 110 */

                    default:
                        ReportError (csfile, Err_IllegalIdent);
                        return ddirmode_unknown;
                }
            }
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            return ddirmode_unknown;
    }

    /* anything that reaches here, it's a syntax error */
    ReportError (csfile, Err_Syntax);
    return ddirmode_unknown;
}


/*!
 * \brief Fetch optional DDIR suffix that is appended to instruction mnemonic (eg. JP.IB)
 * \param csfile
 * \return
 * Return the parsed DDIR mode, or ddirmode_notspecfied if no suffix were specified.
 * Return ddirmode_unknown if syntax error were encounted during suffix parsing.
 */
static enum ddirmodes
FetchDdirSuffix(sourcefile_t *csfile)
{
    enum ddirmodes ddirmode = ddirmode_notspecfied;
    const unsigned char *fptr = MemfTellPtr(csfile->mf);   /* remember position after instruction mnemonic */

    if (GetSym(csfile) == fullstop) {
        /* get DDIR mode, or 'unknown' */
        ddirmode = GetDecoderDirective(csfile);
        /* get ready for instruction mnemonic parsing */
        GetSym(csfile);
    } else {
        /* no DDIR suffix; restore file pointer instruction parsing and return 'not specified' DDIR Mode */
        MemfSeekPtr(csfile->mf, fptr);
        GetSym(csfile);
    }

    return ddirmode;
}


/*!
 * \brief Generate DDIR instruction, based on parsed enumeration of GetDecoderDirective()
 * \param csfile the current source file
 * \param dm
 */
static void
DDIRinstr(const sourcefile_t *csfile, enum ddirmodes dm)
{
    switch(dm) {
        case ddirmode_w:
            StreamCodeWord(csfile, 0xC0DD); /* W        11w11101 110000im, wim = 000 */
            break;
        case ddirmode_ibw:
            StreamCodeWord(csfile, 0xC1DD); /* IB,W     11w11101 110000im, wim = 001 */
            break;
        case ddirmode_iww:
            StreamCodeWord(csfile, 0xC2DD); /* IW,W     11w11101 110000im, wim = 010 */
            break;
        case ddirmode_ib:
            StreamCodeWord(csfile, 0xC3DD); /* IB       11w11101 110000im, wim = 011 */
            break;
        case ddirmode_lw:
            StreamCodeWord(csfile, 0xC0FD); /* LW       11w11101 110000im, wim = 100 */
            break;
        case ddirmode_iblw:
            StreamCodeWord(csfile, 0xC1FD); /* IB,LW    11w11101 110000im, wim = 101 */
            break;
        case ddirmode_iwlw:
            StreamCodeWord(csfile, 0xC2FD); /* IW,LW    11w11101 110000im, wim = 110 */
            break;
        case ddirmode_iw:
            StreamCodeWord(csfile, 0xC3FD); /* IW       11w11101 110000im, wim = 111 */
            break;

        default:
            break;
    }
}


/**
 * @brief Generate signed 8, 16 or 24bit offset immediate byte sequences and expressions
 * @details The expression is parsed + evaluated (now or later) and placed at current assember PC
 * @param csfile
 * @param ddirmode
 * @param lstpatchpos
 */
static void
DDirImmediateOffset(sourcefile_t *csfile, enum ddirmodes ddirmode, int lstpatchpos)
{
    switch(ddirmode) {
        case ddirmode_ib:       /* xxx.ib creates a 6-byte instruction using 16-bit displacement */
        case ddirmode_ibw:
        case ddirmode_iblw:
            if (csfile->sym == rparen)
                StreamCodeWord (csfile, 0); /* idiotic 16-bit 0 displacement specified */
            else
                ProcessExpr (csfile, NULL, RANGE_LSB_16SIGN, 2+lstpatchpos);
            break;

        case ddirmode_iw:       /* xxx.iw creates a 7-byte instruction using 24-bit displacement */
        case ddirmode_iww:
        case ddirmode_iwlw:
            if (csfile->sym == rparen)
                StreamCodeInt24 (csfile, 0); /* idiotic 24-bit 0 displacement specified */
            else
                ProcessExpr (csfile, NULL, RANGE_LSB_24SIGN, 2+lstpatchpos);
            break;

        default:
            /* ddirmode_unknown and irrelevant modes - default 8bit signed displacement */
            if (csfile->sym == rparen)
                StreamCodeByte (csfile, 0); /* no displacement specified, use 0 */
            else
                ProcessExpr (csfile, NULL, RANGE_8SIGN, lstpatchpos);
    }
}


/**
 * @brief Generate 16bit, 24bit or 32bit immediate address via expression
 * @param csfile
 * @param addrexpr pointer to pre-parsed expression (or NULL = to be parsed)
 * @param ddirmode the Ddir instruction suffix mode
 * @param lstpatchpos line position to patch in listing file
 */
static void
DDirImmediateAddress(sourcefile_t *csfile, expression_t *addrexpr, enum ddirmodes ddirmode, int lstpatchpos)
{
    if (addrexpr != NULL)
        /* update PC of pre-parsed expression to point at actual PC position (where address will be placed by the expression evaluation (now or later) */
        addrexpr->codepos = (size_t) GetStreamCodePC(csfile);

    switch(ddirmode) {
        case ddirmode_ib:
        case ddirmode_ibw:
        case ddirmode_iblw:
            ProcessExpr (csfile, addrexpr, RANGE_LSB_24SIGN, lstpatchpos);
            break;

        case ddirmode_iww:
        case ddirmode_iw:
        case ddirmode_iwlw:
            ProcessExpr (csfile, addrexpr, RANGE_LSB_32CONST, lstpatchpos);
            break;

        case ddirmode_w:
        case ddirmode_lw:
            /* no immediate suffix specified, use default 16 bits immediate */
            ProcessExpr (csfile, addrexpr, RANGE_LSB_16CONST, lstpatchpos);
            return;

        default:
            /* immediate default constant is 16bit (no DDIR suffix) */
            ProcessExpr (csfile, addrexpr, RANGE_LSB_16CONST, lstpatchpos);
    }
}


/*!
 * \brief Fetch optional specified Z80 16bit register
 * \param csfile
 * \param rr
 * \return true if validated, or false if incorrectly fetched
 */
static bool
FetchOptReg16Mnem(sourcefile_t *csfile, const enum z80rr rr)
{
    switch(GetSym(csfile)) {
        case newline:
            return true; /* optional register was not specified */

        case name:
            if (CheckReg16Mnem(csfile) == rr) {
                return true; /* correct register specified */
            } else {
                ReportError (csfile, Err_IllegalIdent);
                return false;
            }

        default:
            ReportError (csfile, Err_Syntax);
            return false; /* anything else is a syntax error */
    }
}


/* ----------------------------------------------------------------
   Fetch optional "HL," and return z80r_hl, csfile->sym == comma
   otherwise return any legal z80r_rr without trailing comma.

   returns z80rr_unknown if no first z80r_rr were found and
   file pointer restored
   ---------------------------------------------------------------- */
static enum z80rr
FetchOptHlReg16Mnem(sourcefile_t *csfile)
{
    enum z80rr reg16;
    const unsigned char *fptr = MemfTellPtr(csfile->mf);

    if (GetSym(csfile) == name) {
        if ((reg16 = CheckReg16Mnem(csfile)) == z80rr_unknown) {
            MemfSeekPtr(csfile->mf, fptr);  /* restore file pointer */
            return z80rr_unknown;
        }
    } else {
        MemfSeekPtr(csfile->mf, fptr);  /* restore file pointer */
        return z80rr_unknown;      /* no valid reg16 were found */
    }

    if (reg16 == z80r_hl) {
        /* fetch "," if it exists */
        fptr = MemfTellPtr(csfile->mf);
        if (GetSym(csfile) != comma) {
            MemfSeekPtr(csfile->mf, fptr);  /* restore file pointer */
        }
    }

    return reg16;
}


/*!
 * \brief Generate RLCW | RRCW | RLW | RRW | SLAW | SRAW | SRLW [.IB|.IW] (HL) | (IX|IY+d) instructions
 * \param opcodemask
 * \param ddirmode
 */
static void
ShiftRotHlIxIyW(sourcefile_t *csfile, unsigned int opcodemask, enum ddirmodes ddirmode)
{
    enum z80rr reg16;

    GetSym(csfile);
    switch(reg16 = CheckReg16Mnem(csfile)) {
        case z80r_hl:
            StreamCodeByte (csfile, 0xed);
            StreamCodeByte (csfile, 0xcb);
            StreamCodeByte (csfile, (unsigned char) (opcodemask | 2));
            GetSym(csfile); /* fetch ')' silently */
            break;

        case z80r_ix:
        case z80r_iy:
            GetSym(csfile); /* prepare for displacement expression */

            if (ddirmode != ddirmode_notspecfied)
                DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr() */
            StreamCodeByte (csfile, (reg16 == z80r_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, 0xcb);

            DDirImmediateOffset(csfile, ddirmode, 2);
            StreamCodeByte (csfile, (unsigned char) (opcodemask | 2));
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief Generate xxxW[.IB|.IW] [HL,] (IX|IY+d) instructions
 * \param opcodemask
 * \param ddirmode
 */
static void
ArLogInstrIxIyW(sourcefile_t *csfile, unsigned int opcodemask, enum ddirmodes ddirmode)
{
    enum z80rr reg16;

    GetSym(csfile);
    switch(reg16 = FetchOptHlReg16Mnem(csfile)) {
        case z80r_ix:
        case z80r_iy:
            GetSym(csfile); /* prepare for displacement expression */

            if (ddirmode != ddirmode_notspecfied)
                DDIRinstr(csfile, ddirmode);
            StreamCodeByte (csfile, (reg16 == z80r_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, (unsigned char) (0xc6 | opcodemask));
            DDirImmediateOffset(csfile, ddirmode, 2);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


static void
DivMultInstrIxIyW(sourcefile_t *csfile, unsigned int opcodemask, enum ddirmodes ddirmode)
{
    enum z80rr reg16;

    GetSym(csfile);
    switch(reg16 = FetchOptHlReg16Mnem(csfile)) {
        case z80r_ix:
        case z80r_iy:
            GetSym(csfile);             /* prepare for displacement expression */

            if (ddirmode != ddirmode_notspecfied)
                DDIRinstr(csfile, ddirmode);
            StreamCodeByte (csfile, (reg16 == z80r_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, 0xcb);
            DDirImmediateOffset(csfile, ddirmode, 2);
            StreamCodeByte (csfile, (unsigned char) (0x82 | opcodemask));
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/**
   Generic 16bit Arithmetic / Logical instruction group (mask @00111000):

    ADDW = $00 (@00000000) Group 0
    ADCW = $08 (@00001000) Group 1
    SUBW = $10 (@00010000) Group 2
    SBCW = $18 (@00011000) Group 3
    ANDW = $20 (@00100000) Group 4
    XORW = $28 (@00101000) Group 5
    ORW  = $30 (@00110000) Group 6
    CPW  = $38 (@00111000) Group 7

    Instruction Opcode Bit Structure:

    xxxW [HL],src

    xxxW [HL,]rr     11101101 10xxx1rr                   rr = 00/BC, 01/DE, 03/HL
    xxxW [HL,]xy     11y11101 10xxx111                    y = 0/IX, 1/IY
    xxxW [HL,]nn     11101101 10xxx110 -n(low)- -n(high)
    xxxW [HL,](XY+d) 11y11101 11xxx110 ---d--             y = 0/IX, 1/IY

 * @brief ArLogInstrW
 * @param csfile currently parsed (memory) source file
 * @param opcodemask
 */
static void
ArLogInstrW(sourcefile_t *csfile, unsigned int opcodemask)
{
    const unsigned char *fptr = MemfTellPtr(csfile->mf);
    enum ddirmodes ddirmode = ddirmode_unknown;
    enum z80rr reg16;

    if (GetSym(csfile) == fullstop) {
        ddirmode = GetDecoderDirective(csfile);
        if (ddirmode == ddirmode_unknown) {
            return; /* directive incorrectly specified, abort mission for this instruction */
        }
    } else {
        MemfSeekPtr(csfile->mf, fptr);  /* no trailing DDIR, restore file pointer for normal instruction parsing */
    }

    reg16 = FetchOptHlReg16Mnem(csfile);
    if (reg16 == z80r_hl && csfile->sym == comma) {
        reg16 = FetchOptHlReg16Mnem(csfile);
    }

    if (reg16 != z80rr_unknown) {
        switch(reg16) {
            case z80r_bc:
            case z80r_de:
            case z80r_hl: /* xxxW [HL,] BC|DE|HL */
                /* special bit pattern for HL = @11 (3) */
                if (reg16 == z80r_hl) reg16 = 3;

                StreamCodeByte (csfile, 0xed);
                StreamCodeByte (csfile, (unsigned char) (0x84 | opcodemask | reg16));
                return;

            case z80r_sp:
                if (opcodemask == 0x10) {
                    /* SUBW HL,SP meta instruction */
                    SUBWr16(csfile, reg16);
                    return;
                } else {
                    ReportError (csfile, Err_IllegalIdent);
                }
                break;

            case z80r_ix:
            case z80r_iy: /* xxxW [HL,] IX|IY */
                StreamCodeByte (csfile, (reg16 == z80r_ix) ? 0xdd : 0xfd);
                StreamCodeByte (csfile, (unsigned char) (0x87 | opcodemask));
                return;

            default:
                /* xxxW [HL,] AF|SP illegal */
                ReportError (csfile, Err_IllegalIdent);
                return;
        }
    }

    if (csfile->sym == lparen) {
        /* xxxW[.ib|.iw] [HL,] (IX|IY+d) */
        ArLogInstrIxIyW(csfile, opcodemask, ddirmode);
        return;
    }

    /* xxxW [HL,] nn */
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, (unsigned char) (0x86 | opcodemask));
    ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, 2);
}


/**
    RLCW = $00 (@00000000) Group 0
    RRCW = $08 (@00001000) Group 1
    RLW  = $10 (@00010000) Group 2
    RRW  = $18 (@00011000) Group 3
    SLAW = $20 (@00100000) Group 4
    SRAW = $28 (@00101000) Group 5
    SRLW = $38 (@00111000) Group 7

    Instruction Opcode Bit Structure:

    xxxW rr     11101101 11001011 00xxx0rr           rr = 00/BC, 01/DE, 03/HL
    xxxW xy     11101101 11001011 00xxx10y            y = 0/IX, 1/IY
    xxxW (HL)   11101101 11001011 00xxx010
    xxxW (XY+d) 11y11101 11001011 ——d— 00xxx010       y = 0/IX, 1/IY

 * @brief Generic 16bit Shift / Rotate instruction group (mask @00111000):
 * \details
 *
 * @param csfile currently parsed (memory) source file
 * @param opcodemask
 */
static void
ShiftRotInstrW(sourcefile_t *csfile, unsigned int opcodemask)
{
    enum z80rr reg16;
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing of DDIR suffix gave syntax error */

    if (csfile->sym == lparen) {
        /* xxxW[.ib|.iw] (HL) | (IX|IY+d) */
        ShiftRotHlIxIyW(csfile, opcodemask, ddirmode);
        return;
    }

    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xcb);

    reg16 = CheckReg16Mnem(csfile);
    switch(reg16) {
        case z80r_bc:
        case z80r_de:
            StreamCodeByte (csfile, (unsigned char) (opcodemask | reg16));
            return;

        case z80r_hl:
            /* special bit pattern for HL = @11 (3) */
            StreamCodeByte (csfile, (unsigned char) (opcodemask | 3));
            return;

        case z80r_ix:
            StreamCodeByte (csfile, (unsigned char) (opcodemask | 4));
            return;
        case z80r_iy:
            StreamCodeByte (csfile, (unsigned char) (opcodemask | 5));
            return;

        default:
            /* xxxW AF|SP illegal */
            ReportError (csfile, Err_IllegalIdent);
    }
}


static void
ArithLog8bitIndrctZ380(sourcefile_t *csfile, int opcode, enum ddirmodes ddirmode)
{
    enum z80ia reg16 = CheckIndirectAddrMnem (csfile);

    switch (reg16) {
        case z80ia_hl:
            /* xxx  A,(HL) */
            StreamCodeByte (csfile, (unsigned char) (128 + opcode * 8 + 6));
            break;

        case z80ia_ix:
        case z80ia_iy:
            if (ddirmode != ddirmode_notspecfied)
                DDIRinstr(csfile, ddirmode);
            StreamCodeByte (csfile, (reg16 == z80ia_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, (unsigned char) (128 + opcode * 8 + 6));
            DDirImmediateOffset(csfile, ddirmode, 2);
            break;

        default:
            ReportError (csfile, Err_Syntax);
            break;
    }
}


/*!
 * \brief Parsing & code generation of 8bit "<instr>[.ib | .iw] [A,]xxx" for ADD, ADC, SBC, SUB, AND, OR, XOR & CP instructions
 * \param csfile
 * \param opcode
 */
static void
ArithLog8bitZ380 (sourcefile_t *csfile, int opcode)
{
    enum ddirmodes ddirmode = ddirmode_notspecfied;
    const unsigned char *ptr = MemfTellPtr(csfile->mf);   /* remember position after instruction mnemonic */

    if (GetSym(csfile) == fullstop) {
        /* get DDIR mode, or 'unknown' */
        ddirmode = GetDecoderDirective(csfile);
        if (ddirmode != ddirmode_notspecfied)
            /* DDIR preserve parsing position just after DDIR suffix */
            ptr = MemfTellPtr(csfile->mf);
    } else {
        /* no DDIR suffix; restore file pointer instruction parsing */
        MemfSeekPtr(csfile->mf, ptr);
    }

    if (ddirmode == ddirmode_unknown)
        return; /* parsing of DDIR suffix gave syntax error */

    if (GetSym(csfile) == name && CheckReg8Mnem (csfile) == z80r_a) {
        GetSym(csfile);
        if (csfile->sym == comma) {
            /* parsed optional "A," */
            ptr = MemfTellPtr(csfile->mf);
        }
    }

    /* optional "A," may have been parsed, otherwise just reset parsing to begin after instr. mnemonic */
    MemfSeekPtr(csfile->mf, ptr);
    csfile->sym = nil;
    csfile->eol = false;

    if (GetSym(csfile) == lparen) {
        ArithLog8bitIndrctZ380(csfile, opcode, ddirmode);
    } else {
        /* no indirect addressing, parse 8bit source register */
        ArithLogSrc8RegZ80(csfile, opcode);
    }
}


static void
AND_z380_ (sourcefile_t *csfile)
{
    ArithLog8bitZ380 (csfile, 4);
}


static void
XOR_z380_ (sourcefile_t *csfile)
{
    ArithLog8bitZ380 (csfile, 5);
}


static void
OR_z380_ (sourcefile_t *csfile)
{
    ArithLog8bitZ380 (csfile, 6);
}


static void
CP_z380_ (sourcefile_t *csfile)
{
    ArithLog8bitZ380 (csfile, 7);
}


/*!
 * \brief Common Multiply and Division Instruction Groups
 * \details

    MULTW  = $10 (@00010000) Group 2
    MULTUW = $18 (@00011000) Group 3
    DIVUW  = $38 (@00111000) Group 7

    Instruction Opcode Bit Structure:

    xxxW [HL],src

    xxxW [HL,]rr     11101101 11001011 10xxx0rr          rr = 00/BC, 01/DE, 03/HL
    xxxW [HL,]xy     11101101 11001011 10xxx10y           y = 0/IX, 1/IY
    xxxW [HL,]nn     11101101 11001011 10xxx111 -n(low)- -n(high)
    xxxW [HL,](XY+d) 11y11101 11001011 ——d— 10xxx010      y = 0/IX, 1/IY

 */
static void
DivMultW(sourcefile_t *csfile, unsigned int opcodemask)
{
    const unsigned char *fptr = MemfTellPtr(csfile->mf);
    enum ddirmodes ddirmode = ddirmode_unknown;
    enum z80rr reg16;

    if (GetSym(csfile) == fullstop) {
        ddirmode = GetDecoderDirective(csfile);
        if (ddirmode == ddirmode_unknown) {
            return; /* directive incorrectly specified, abort mission for this instruction */
        }
    } else {
        MemfSeekPtr(csfile->mf, fptr);  /* no trailing DDIR, restore file pointer for normal instruction parsing */
    }

    reg16 = FetchOptHlReg16Mnem(csfile);
    if (reg16 == z80r_hl && csfile->sym == comma) {
        reg16 = FetchOptHlReg16Mnem(csfile);
    }

    if (reg16 != z80rr_unknown) {
        StreamCodeByte (csfile, 0xed);
        StreamCodeByte (csfile, 0xcb);

        switch(reg16) {
            case z80r_bc: /* xxxxW [HL,] BC|DE|HL */
            case z80r_de:
            case z80r_hl:
                /* special bit pattern for HL = @11 (3) */
                if (reg16 == z80r_hl) reg16 = z80r_hl_z380;

                StreamCodeByte (csfile, (unsigned char) (0x80 | opcodemask | reg16));
                return;

            case z80r_ix: /* xxxxW [HL,] IX|IY */
            case z80r_iy:
                if (reg16 == z80r_ix)
                    StreamCodeByte (csfile, (unsigned char) (0x84 | opcodemask | 0));
                if (reg16 == z80r_iy)
                    StreamCodeByte (csfile, (unsigned char) (0x84 | opcodemask | 1));
                return;

            default:
                /* xxxxW [HL,] AF|SP illegal */
                ReportError (csfile, Err_IllegalIdent);
                return;
        }
    }

    if (csfile->sym == lparen) {
        /* xxxxW[.ib|.iw] [HL,] (IX|IY+d) */
        DivMultInstrIxIyW(csfile, opcodemask, ddirmode);
    } else {
        /* xxxxW [HL,] nn */
        StreamCodeByte (csfile, 0xed);
        StreamCodeByte (csfile, 0xcb);
        StreamCodeByte (csfile, (unsigned char) (0x87 | opcodemask));

        ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, 2);
    }
}


/* ---------------------------------------------------------------------------
    Exchange all registers with alternate bank

    EXALL

    11101101 11011001
--------------------------------------------------------------------------- */
static void
EXALL(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xd9);
}


/* ---------------------------------------------------------------------------
    Exchange IX register with alternate bank

    EXXX

    11011101 11011001
--------------------------------------------------------------------------- */
static void
EXXX(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xdd);
    StreamCodeByte (csfile, 0xd9);
}


/* ---------------------------------------------------------------------------
    Exchange IY register with alternate bank

    EXXY

    11111101 11011001
--------------------------------------------------------------------------- */
static void
EXXY(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xfd);
    StreamCodeByte (csfile, 0xd9);
}


/*!
 * \brief Divide Unsigned (word) instruction
 * \details

    DIVUW [HL,]src

    DIVUW [HL,]rr     11101101 11001011 101110rr          rr = 00/BC, 01/DE, 03/HL
    DIVUW [HL,]xy     11101101 11001011 1011110y           y = 0/IX, 1/IY
    DIVUW [HL,]nn     11101101 11001011 10111111 -n(low)- -n(high)
    DIVUW [HL,](XY+d) 11y11101 11001011 ——d— 10111010      y = 0/IX, 1/IY

 */
static void
DIVUW(sourcefile_t *csfile)
{
    DivMultW(csfile, 0x38);
}


/*!
 * \brief Multiply Unsigned (word) instruction
 * \details

    MULTUW [HL,]src

    MULTUW [HL,]rr     11101101 11001011 100110rr          rr = 00/BC, 01/DE, 03/HL
    MULTUW [HL,]xy     11101101 11001011 1001110y           y = 0/IX, 1/IY
    MULTUW [HL,]nn     11101101 11001011 10011111 -n(low)- -n(high)
    MULTUW [HL,](XY+d) 11y11101 11001011 ——d— 10011010      y = 0/IX, 1/IY
 */
static void
MULTUW(sourcefile_t *csfile)
{
    DivMultW(csfile, 0x18);
}


/*!
 * \brief Multiply (word) instruction
 * \details

    MULTW [HL,]src

    MULTW [HL,]rr     11101101 11001011 100100rr          rr = 00/BC, 01/DE, 03/HL
    MULTW [HL,]xy     11101101 11001011 1001010y           y = 0/IX, 1/IY
    MULTW [HL,]nn     11101101 11001011 10010111 -n(low)- -n(high)
    MULTW [HL,](XY+d) 11y11101 11001011 ——d— 10010010      y = 0/IX, 1/IY
 */
static void
MULTW(sourcefile_t *csfile)
{
    DivMultW(csfile, 0x10);
}


/* ---------------------------------------------------------------------------
    Add (word), ADDW = $00 (@00000000) Group 0

    ADDW [HL],src

    ADDW [HL,]rr     11101101 100001rr                   rr = 00/BC, 01/DE, 03/HL
    ADDW [HL,]xy     11y11101 10000111                    y = 0/IX, 1/IY
    ADDW [HL,]nn     11101101 10000110 -n(low)- -n(high)
    ADDW [HL,](XY+d) 11y11101 11000110 ---d--             y = 0/IX, 1/IY
--------------------------------------------------------------------------- */
static void
ADDW(sourcefile_t *csfile)
{
     ArLogInstrW(csfile, 0x00);
}


/* ---------------------------------------------------------------------------
    Add with Carry (word), ADCW = $08 (@00001000) Group 1

    ADCW [HL],src

    ADCW [HL,]rr     11101101 100011rr                   rr = 00/BC, 01/DE, 03/HL
    ADCW [HL,]xy     11y11101 10001111                    y = 0/IX, 1/IY
    ADCW [HL,]nn     11101101 10001110 -n(low)- -n(high)
    ADCW [HL,](XY+d) 11y11101 11001110 ---d--             y = 0/IX, 1/IY
--------------------------------------------------------------------------- */
static void
ADCW(sourcefile_t *csfile)
{
     ArLogInstrW(csfile, 0x08);
}


/* ---------------------------------------------------------------------------
    Subtract (word), SUBW = $10 (@00010000) Group 2
    SUBW [HL],src

    SUBW [HL,]rr     11101101 100101rr                   rr = 00/BC, 01/DE, 03/HL
    SUBW [HL,]xy     11y11101 10010111                    y = 0/IX, 1/IY
    SUBW [HL,]nn     11101101 10010110 -n(low)- -n(high)
    SUBW [HL,](XY+d) 11y11101 11010110 ---d--             y = 0/IX, 1/IY
--------------------------------------------------------------------------- */
static void
SUBW_z380_(sourcefile_t *csfile)
{
     ArLogInstrW(csfile, 0x10);
}


/* ---------------------------------------------------------------------------
    Subtract with Carry (word), SBCW = $18 (@00011000) Group 3
    SBCW [HL],src

    SBCW [HL,]rr     11101101 100111rr                   rr = 00/BC, 01/DE, 03/HL
    SBCW [HL,]xy     11y11101 10011111                    y = 0/IX, 1/IY
    SBCW [HL,]nn     11101101 10011110 -n(low)- -n(high)
    SBCW [HL,](XY+d) 11y11101 11011110 ---d--             y = 0/IX, 1/IY
--------------------------------------------------------------------------- */
static void
SBCW(sourcefile_t *csfile)
{
     ArLogInstrW(csfile, 0x18);
}


/* ---------------------------------------------------------------------------
    And (word), ANDW = $20 (@00100000) Group 4
    ANDW [HL],src

    ANDW [HL,]rr     11101101 101001rr                   rr = 00/BC, 01/DE, 03/HL
    ANDW [HL,]xy     11y11101 10100111                    y = 0/IX, 1/IY
    ANDW [HL,]nn     11101101 10100110 -n(low)- -n(high)
    ANDW [HL,](XY+d) 11y11101 11100110 ---d--             y = 0/IX, 1/IY
--------------------------------------------------------------------------- */
static void
ANDW(sourcefile_t *csfile)
{
     ArLogInstrW(csfile, 0x20);
}


/* ---------------------------------------------------------------------------
    Xor(word), XORW = $28 (@00101000) Group 5
    XORW [HL],src

    XORW [HL,]rr     11101101 101011rr                   rr = 00/BC, 01/DE, 03/HL
    XORW [HL,]xy     11y11101 10101111                    y = 0/IX, 1/IY
    XORW [HL,]nn     11101101 10101110 -n(low)- -n(high)
    XORW [HL,](XY+d) 11y11101 11101110 ---d--             y = 0/IX, 1/IY
--------------------------------------------------------------------------- */
static void
XORW(sourcefile_t *csfile)
{
     ArLogInstrW(csfile, 0x28);
}


/* ---------------------------------------------------------------------------
    Or (word), ORW = $30 (@00110000) Group 6
    ORW [HL],src

    ORW [HL,]rr      11101101 101101rr                   rr = 00/BC, 01/DE, 03/HL
    ORW [HL,]xy      11y11101 10110111                    y = 0/IX, 1/IY
    ORW [HL,]nn      11101101 10110110 -n(low)- -n(high)
    ORW [HL,](XY+d)  11y11101 11110110 ---d--             y = 0/IX, 1/IY
--------------------------------------------------------------------------- */
static void
ORW(sourcefile_t *csfile)
{
     ArLogInstrW(csfile, 0x30);
}


/* ---------------------------------------------------------------------------
    Compare (word), CPW = $38 (@00111000) Group 7
    CPW [HL],src

    CPW [HL,]rr      11101101 101111rr                   rr = 00/BC, 01/DE, 03/HL
    CPW [HL,]xy      11y11101 10111111                    y = 0/IX, 1/IY
    CPW [HL,]nn      11101101 10111110 -n(low)- -n(high)
    CPW [HL,](XY+d)  11y11101 11111110 ---d--             y = 0/IX, 1/IY
--------------------------------------------------------------------------- */
static void
CPW(sourcefile_t *csfile)
{
     ArLogInstrW(csfile, 0x38);
}


/*!
 * \brief RLCW instruction group
 * \details
 * RLCW R      11101101 11001011 000000rr
 * RLCW RX     11101101 11001011 0000010y
 * RLCW (HL)   11101101 11001011 00000010
 * RLCW (XY+d) 11y11101 11001011 ——d— 00000010
 *
 * \param csfile
 */
static void
RLCW(sourcefile_t *csfile)
{
    ShiftRotInstrW(csfile, 0x00);
}


/*!
 * \brief RRCW instruction group
 * \details
 * RRCW R      11101101 11001011 000010rr
 * RRCW RX     11101101 11001011 0000110y
 * RRCW (HL)   11101101 11001011 00001010
 * RRCW (XY+d) 11y11101 11001011 ——d— 00001010
 *
 * \param csfile
 */
static void
RRCW(sourcefile_t *csfile)
{
    ShiftRotInstrW(csfile, 0x08);
}


/*!
 * \brief RLW instruction group
 * \details
 * RLW R       11101101 11001011 000100rr
 * RLW RX      11101101 11001011 0001010y
 * RLW (HL)    11101101 11001011 00010010
 * RLW (XY+d)  11y11101 11001011 ——d— 00010010
 *
 * \param csfile
 */
static void
RLW_z380_(sourcefile_t *csfile)
{
    ShiftRotInstrW(csfile, 0x10);
}


/*!
 * \brief RRW instruction group
 * \details
 * RRW R       11101101 11001011 000110rr
 * RRW RX      11101101 11001011 0001110y
 * RRW (HL)    11101101 11001011 00011010
 * RRW (XY+d)  11y11101 11001011 ——d— 00011010
 *
 * \param csfile
 */
static void
RRW_z380_(sourcefile_t *csfile)
{
    ShiftRotInstrW(csfile, 0x18);
}


/*!
 * \brief SLAW instruction group
 * \details
 * SLAW R      11101101 11001011 001000rr
 * SLAW RX     11101101 11001011 0010010y
 * SLAW (HL)   11101101 11001011 00100010
 * SLAW (XY+d) 11y11101 11001011 ——d— 00100010
 *
 * \param csfile
 */
static void
SLAW_z380_(sourcefile_t *csfile)
{
    ShiftRotInstrW(csfile, 0x20);
}


/*!
 * \brief SRAW instruction group
 * \details
 * SRAW R      11101101 11001011 001010rr
 * SRAW RX     11101101 11001011 0010110y
 * SRAW (HL)   11101101 11001011 00101010
 * SRAW (XY+d) 11y11101 11001011 ——d— 00101010
 *
 * \param csfile
 */
static void
SRAW_z380_(sourcefile_t *csfile)
{
    ShiftRotInstrW(csfile, 0x28);
}


/*!
 * \brief SRLW instruction group
 * \details
 * SRLW R      11101101 11001011 001110rr
 * SRLW RX     11101101 11001011 0011110y
 * SRLW (HL)   11101101 11001011 00111010
 * SRLW (XY+d) 11y11101 11001011 ——d— 00111010
 *
 * \param csfile
 */
static void
SRLW_z380_(sourcefile_t *csfile)
{
    ShiftRotInstrW(csfile, 0x38);
}


static void
ADCSBC_z380_ (sourcefile_t *csfile, int opc8bitreg, int opc16bitreg)
{
    const unsigned char *ptr = MemfTellPtr(csfile->mf);

    GetSym(csfile);
    switch (CheckReg16Mnem (csfile)) {
        case z80rr_unknown:
            /* 16 bit register wasn't found - try to evaluate the 8 bit version */
            MemfSeekPtr(csfile->mf, ptr);
            ArithLog8bitZ380(csfile, opc8bitreg);
            break;

        case z80r_hl:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }
            AdcSbcHL(csfile, opc16bitreg);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            break;
    }
}


static void
ADC_z380_ (sourcefile_t *csfile)
{
    ADCSBC_z380_ (csfile, 1, 74);
}


static void
SBC_z380_ (sourcefile_t *csfile)
{
    ADCSBC_z380_ (csfile, 3, 66);
}


/*!
 * \brief ADD | SUB [.ib|.iw] SP,nn
 * \param csfile
 * \param opcode
 * \param ddirmode
 */
static void
AddSubSPnn(sourcefile_t *csfile, int opcode, enum ddirmodes ddirmode)
{
    /* fetch "," (mandatory) */
    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    GetSym(csfile);

    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr(), if specified */

    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, (unsigned char) opcode);

    DDirImmediateAddress(csfile, NULL, ddirmode, 2);
}


/*!
 * \brief ADD | SUB [.ib | .iw] HL,(nn)
 * \param csfile
 * \param opcode
 * \param ddirmode
 */
static void
AddSubHLnn(sourcefile_t *csfile, int opcode, enum ddirmodes ddirmode)
{
    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr() , if specified */

    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, (unsigned char) opcode);

    DDirImmediateAddress(csfile, NULL, ddirmode, 2);
}



/*!
 * \brief Combined Z80 / Z380 SUB instructions
 * \details
 * ADD [A,]r8
 * ADD [A,](HL)
 * ADD[.ib|.iw] [A,](IX|IY + o)
 * ADD HL,r16
 * ADD[.ib|.iw] HL,(nn)
 * ADD[.ib|.iw] SP,nn
 */
static void
ADD_z380_(sourcefile_t *csfile)
{
    const unsigned char *fptr = MemfTellPtr(csfile->mf);
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);
    enum z80rr r16;

    if (ddirmode == ddirmode_unknown)
        return; /* parsing of DDIR suffix gave syntax error */

    if (csfile->sym != name) {
        MemfSeekPtr(csfile->mf, fptr);       /* probably start of expression */
        ArithLog8bitZ380(csfile, 0);         /* restore file pointer and continue parsing for Z80 ADD 8bit instructions.. */
        return;
    }

    /* a name identifier was fetched, check if its an 8bit register? */
    if (CheckReg8Mnem(csfile) != z80r_unknown) {
        MemfSeekPtr(csfile->mf, fptr);       /* an 8bit register is recognised */
        ArithLog8bitZ380(csfile, 0);         /* restore file pointer and continue parsing for Z80 ADD 8bit register instructions.. */
        return;
    }

    switch(r16 = CheckReg16Mnem(csfile)) {
        case z80r_hl:
            /* fetch "," (mandatory) */
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            fptr = MemfTellPtr(csfile->mf);
            if (GetSym(csfile) != lparen) {
                /* should be ADD HL,rg16 instructions */
                MemfSeekPtr(csfile->mf, fptr);
                AddHL(csfile, 9);        /* restore file pointer and continue parsing for Z80 ADD HL 16bit instructions.. */
                return;
            }

            /* create ADD HL,(nn) instructions with optional .ib and .iw suffices */
            AddSubHLnn(csfile, 0xc6, ddirmode);
            break;

        case z80r_ix:
        case z80r_iy:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }
            AddIxIyReg16(csfile, r16, 9);
            break;

        case z80r_sp:
            AddSubSPnn(csfile, 0x82, ddirmode);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            return;
    }

}


/*!
 * \brief Combined Z80 / Z380 SUB instructions
 * \details
 * SUB A,r8
 * SUB A,(HL)
 * SUB[.ib | .iw] A,(IX|IY + o)
 * SUB[.ib|.iw] HL,(nn)
 * SUB[.ib|.iw] SP,nn
 */
static void
SUB_z380_(sourcefile_t *csfile)
{
    const unsigned char *fptr = MemfTellPtr(csfile->mf);
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing of DDIR suffix gave syntax error */

    if (csfile->sym != name) {
        MemfSeekPtr(csfile->mf, fptr);       /* probably start of expression */
        ArithLog8bitZ380(csfile, 2);         /* restore file pointer and continue parsing for Z80 ADD 8bit instructions.. */
        return;
    }

    /* a name identifier was fetched, check if its an 8bit register? */
    if (CheckReg8Mnem(csfile) != z80r_unknown) {
        MemfSeekPtr(csfile->mf, fptr);       /* an 8bit register is recognised */
        ArithLog8bitZ380(csfile, 2);         /* restore file pointer and continue parsing for Z80 ADD 8bit register instructions.. */
        return;
    }

    switch(CheckReg16Mnem(csfile)) {
        case z80r_hl:
            /* fetch "," (mandatory) */
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            if (GetSym(csfile) != lparen) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            /* generate SUB HL,(nn) instructions with optional .ib and .iw suffices */
            AddSubHLnn(csfile, 0xd6, ddirmode);
            break;

        case z80r_sp:
            AddSubSPnn(csfile, 0x92, ddirmode);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            return;
    }
}


/*!
 * \brief IM 0 | 1 | 2 | 3
 * \param csfile
 * \param imType
 */
static void
IM_z380_ (sourcefile_t *csfile)
{
    int imType;

    GetSym(csfile);
    if ((imType = (int) ParseMnemConstant(csfile)) == -1)
        return;

    StreamCodeByte (csfile, 0xED);
    if (imType != 3)
        ImZ80(csfile, imType);
    else
        StreamCodeByte (csfile, 0x4E); /* Z380 only */
}


/* ---------------------------------------------------------------------------
    Disable interrupts

    DI [n]

    11110011
    11011101 11110011 —n——
--------------------------------------------------------------------------- */
static void
DI_z380_(sourcefile_t *csfile)
{
    if (GetSym(csfile) == newline)
        DI(csfile);
    else {
        StreamCodeByte (csfile, 0xdd);
        StreamCodeByte (csfile, 0xf3);
        ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 2);
    }
}


/* ---------------------------------------------------------------------------
    Enable interrupts

    EI [n]

    11111011
    11011101 11111011 —n——
--------------------------------------------------------------------------- */
static void
EI_z380_(sourcefile_t *csfile)
{
    if (GetSym(csfile) == newline)
        EI(csfile);
    else {
        StreamCodeByte (csfile, 0xdd);
        StreamCodeByte (csfile, 0xfb);
        ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 2);
    }
}


/* ---------------------------------------------------------------------------
    Parse, validate & code generate for the following instructions

    EX A, (HL) | A | A' | B | C | D | E | H | L
    EX B,B'
    EX C,C'
    EX D,D'
    EX E,E'
    EX H,H'
    EX L,L'
    EX AF,AF'
    EX BC, BC' | DE | HL | IX | IY
    EX DE, DE' | HL | IX | IY
    EX HL, HL' | IX | IY
    EX IX, IX' | IY
    EX IY, IY'
    EX (SP), HL | IX | IY
   --------------------------------------------------------------------------- */
static void
EX_z380_(sourcefile_t *csfile)
{
    enum z80r src8;
    enum z80r dst8;
    enum z80rr src16;
    enum z80rr dst16;
    enum symbols srcsym;

    switch(GetSym(csfile)) {
        case lparen:
            /* finalize parsing and code generation of EX (SP),xx (original Z80) */
            EX_SP(csfile);
            break;

        case name:
            dst8 = CheckReg8Mnem(csfile);    /* pre-validate a 8bit destination register */
            dst16 = CheckReg16Mnem (csfile); /* pre-validate a 16bit destination register */

            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            srcsym = GetSym(csfile);
            if (srcsym != name && srcsym != lparen) { /* register accepted or (HL) for EX A,(HL) */
                ReportError (csfile, Err_Syntax);
                return;
            }

            src8 = CheckReg8Mnem (csfile); /* EX r,r combinations */
            src16 = CheckReg16Mnem (csfile); /* EX rr,rr combinations */
            GetSym(csfile); /* fetch token, right after source register */

            switch (dst16) {
                case z80r_af: /* only variation is EX AF,AF' */
                    if (src16 == z80r_af && csfile->sym == squote)
                        EX_AFaf(csfile);
                    else
                        ReportError (csfile, Err_IllegalIdent);
                    break;

                case z80r_bc:
                    switch(src16) {
                        case z80r_bc: /* EX BC,BC' */
                            if (csfile->sym == squote) {
                                StreamCodeByte (csfile, 0xed);
                                StreamCodeByte (csfile, 0xcb);
                                StreamCodeByte (csfile, 0x30);
                            } else {
                                ReportError (csfile, Err_IllegalIdent);
                            }
                            break;

                        case z80r_de: /* EX BC,DE */
                            StreamCodeByte (csfile, 0xed);
                            StreamCodeByte (csfile, 0x05);
                            break;

                        case z80r_hl: /* EX BC,HL */
                            StreamCodeByte (csfile, 0xed);
                            StreamCodeByte (csfile, 0x0d);
                            break;

                        case z80r_ix: /* EX BC,IX */
                            StreamCodeByte (csfile, 0xed);
                            StreamCodeByte (csfile, 0x03);
                            break;

                        case z80r_iy: /* EX BC,IY */
                            StreamCodeByte (csfile, 0xed);
                            StreamCodeByte (csfile, 0x0b);
                            break;

                        default:
                            ReportError (csfile, Err_IllegalIdent);
                    }
                    break;

                case z80r_de:
                    switch(src16) {
                        case z80r_de: /* EX DE,DE' */
                            if (csfile->sym == squote) {
                                StreamCodeByte (csfile, 0xed);
                                StreamCodeByte (csfile, 0xcb);
                                StreamCodeByte (csfile, 0x31);
                            } else {
                                ReportError (csfile, Err_IllegalIdent);
                            }
                            break;

                        case z80r_hl: /* EX DE,HL */
                            StreamCodeByte (csfile, 0xeb);
                            break;

                        case z80r_ix: /* EX DE,IX */
                            StreamCodeByte (csfile, 0xed);
                            StreamCodeByte (csfile, 0x13);
                            break;

                        case z80r_iy: /* EX DE,IY */
                            StreamCodeByte (csfile, 0xed);
                            StreamCodeByte (csfile, 0x1b);
                            break;

                        default:
                            ReportError (csfile, Err_IllegalIdent);
                    }
                    break;

                case z80r_hl:
                    switch(src16) {
                        case z80r_hl: /* EX HL,HL' */
                            if (csfile->sym == squote) {
                                StreamCodeByte (csfile, 0xed);
                                StreamCodeByte (csfile, 0xcb);
                                StreamCodeByte (csfile, 0x33);
                            } else {
                                ReportError (csfile, Err_IllegalIdent);
                            }
                            break;

                        case z80r_ix: /* EX HL,IX */
                            StreamCodeByte (csfile, 0xed);
                            StreamCodeByte (csfile, 0x33);
                            break;

                        case z80r_iy: /* EX HL,IY */
                            StreamCodeByte (csfile, 0xed);
                            StreamCodeByte (csfile, 0x3b);
                            break;

                        default:
                            ReportError (csfile, Err_IllegalIdent);
                    }
                    break;

                case z80r_sp: /* EX SP,XX is not implemented on Z380 */
                    ReportError (csfile, Err_IllegalIdent);
                    break;

                case z80r_ix:
                    switch(src16) {
                        case z80r_ix: /* EX IX,IX' */
                            if (csfile->sym == squote) {
                                StreamCodeByte (csfile, 0xed);
                                StreamCodeByte (csfile, 0xcb);
                                StreamCodeByte (csfile, 0x34);
                            } else {
                                ReportError (csfile, Err_IllegalIdent);
                            }
                            break;
                        case z80r_iy: /* EX IX,IY */
                            StreamCodeByte (csfile, 0xed);
                            StreamCodeByte (csfile, 0x2b);
                            break;
                        default:
                            ReportError (csfile, Err_IllegalIdent);
                    }
                    break;

                case z80r_iy:
                    if (src16 == z80r_iy) { /* EX IY,IY' */
                            if (csfile->sym == squote) {
                                StreamCodeByte (csfile, 0xed);
                                StreamCodeByte (csfile, 0xcb);
                                StreamCodeByte (csfile, 0x35);
                            } else {
                                ReportError (csfile, Err_IllegalIdent);
                            }
                    } else {
                        ReportError (csfile, Err_IllegalIdent);
                    }
                    break;

                case z80rr_unknown:
                    switch(dst8) {
                        case z80r_a:  /* EX A,r8 */
                            switch (srcsym) {
                                case name:
                                    switch(src8) {   /* most probably an 8bit register mnemonic */
                                        case z80r_a:
                                            if (csfile->sym == squote) {
                                                /* EX A,A' */
                                                StreamCodeByte (csfile, 0xcb);
                                                StreamCodeByte (csfile, 0x37);
                                            } else {
                                                /* EX A,A - it's an allowed NOP but generate it anyway! */
                                                StreamCodeByte (csfile, 0xed);
                                                StreamCodeByte (csfile, 0x3f);
                                            }
                                            break;

                                        case z80r_b:
                                        case z80r_c:
                                        case z80r_d:
                                        case z80r_e:
                                        case z80r_h:
                                        case z80r_l:
                                            /* 11101101 00-r-111 */
                                            StreamCodeByte (csfile, 0xed);
                                            StreamCodeByte (csfile, (unsigned char) src8 * 8 + 7);
                                            break;

                                        default:
                                            /* EX A,F and EX A,IXL etc. are not defined... */
                                            ReportError (csfile, Err_IllegalIdent);
                                    }
                                    break;

                                case lparen: /* EX A,(HL) is the only varation allowed */
                                    if (CheckReg16Mnem (csfile) == z80r_hl && GetSym(csfile) == rparen) {
                                        StreamCodeByte (csfile, 0xed);
                                        StreamCodeByte (csfile, 0x37);
                                    } else {
                                        ReportError (csfile, Err_Syntax);
                                    }
                                    break;

                                default: /* an 8bit source register name was not identifed */
                                    ReportError (csfile, Err_Syntax);
                            }
                            break;

                        case z80r_b:
                        case z80r_c:
                        case z80r_d:
                        case z80r_e:
                        case z80r_h:
                        case z80r_l:
                            /* 11101101 00110-r-   EX r,r' */
                            if (csfile->sym == squote) {
                                StreamCodeByte (csfile, 0xcb);
                                StreamCodeByte (csfile, (unsigned char) (0x30 + dst8));
                            } else {
                                ReportError (csfile, Err_IllegalIdent);
                            }
                            break;

                        default:
                            ReportError (csfile, Err_IllegalIdent);
                    }
                    break;
            }
            break;

        default:
            ReportError (csfile, Err_Syntax);
    }
}


/* ---------------------------------------------------------------------------
    DJNZ relative (8, 16 or 24bit relative addressing)

    DJNZ addr   00010000 —disp—
    DJNZ addr   11011101 00010000 -d(low)- -d(high)             (DDIR IB)
    DJNZ addr   11111101 00010000 -d(low)- -d(mid)  -d(high)    (DDIR IW)
--------------------------------------------------------------------------- */
static void
DJNZ_z380_(sourcefile_t *csfile)
{
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing of DDIR suffix gave syntax error */

    switch(ddirmode) {
        case ddirmode_ib: /* DJNZ.IB [cc],reladdress16 */
            StreamCodeByte (csfile, 0xdd);
            DJNZopcode(csfile);
            ProcessPcRelativeExpr (csfile, RANGE_LSB_PCREL16, 2);
            break;

        case ddirmode_iw: /* DJNZ.IW [cc],reladdress24 */
            StreamCodeByte (csfile, 0xfd);
            DJNZopcode(csfile);
            ProcessPcRelativeExpr (csfile, RANGE_LSB_PCREL24, 2);
            break;

        case ddirmode_notspecfied:
            /* no immediate address directive specified, default is DJNZ [,]reladdress8 */
            DJNZopcode(csfile);
            ProcessPcRelativeExpr (csfile, RANGE_PCREL8, 1);
            break;

        default:
            ReportError (csfile, Err_Syntax); /* only DJNZ.IB or DJNZ.IW suppported */
    }
}


/* ---------------------------------------------------------------------------
    JR relative (8, 16 or 24bit relative addressing)

    JR addr     00011000 —disp—
    JR CC,addr  001cc000 —disp—
    JR addr     11011101 00011000 -d(low)- -d(high)             (DDIR IB)
    JR CC,addr  11011101 001cc000 -d(low)- -d(high)             (DDIR IB)
    JR addr     11111101 00011000 -d(low)- -d(mid)  -d(high)    (DDIR IW)
    JR CC,addr  11111101 001cc000 -d(low)- -d(mid)- -d(high)    (DDIR IW)
--------------------------------------------------------------------------- */
static void
JR_z380_(sourcefile_t *csfile)
{
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing of DDIR suffix gave syntax error */

    switch(ddirmode) {
        case ddirmode_ib: /* JR.IB [cc],reladdress16 */
            StreamCodeByte (csfile, 0xdd);
            JRccOpcode(csfile);
            ProcessPcRelativeExpr (csfile, RANGE_LSB_PCREL16, 2);
            break;

        case ddirmode_iw: /* JR.IW [cc],reladdress24 */
            StreamCodeByte (csfile, 0xfd);
            JRccOpcode(csfile);
            ProcessPcRelativeExpr (csfile, RANGE_LSB_PCREL24, 2);
            break;

        case ddirmode_notspecfied:
            JRccOpcode(csfile);
            ProcessPcRelativeExpr (csfile, RANGE_PCREL8, 1);
            break;

        default:
            ReportError (csfile, Err_Syntax); /* only JR.IB or JR.IW suppported */
    }
}


/* ---------------------------------------------------------------------------
    Bank Test

    BTEST

    11101101 11001111
--------------------------------------------------------------------------- */
static void
BTEST(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xcf);
}


/* ---------------------------------------------------------------------------
    CALL relative (8, 16 or 24bit relative addressing)

    CALR CC,addr    11101101 11-cc100 —disp—
    CALR addr       11101101 11001101 —disp—
    CALR.ib CC,addr 11011101 11-cc100 -d(low)- -d(high)
    CALR.ib addr    11011101 11001101 -d(low)- -d(high)
    CALR.iw CC,addr 11111101 11-cc100 -d(low)- -d(mid)- -d(high)
    CALR.iw addr    11111101 11001101 -d(low)- -d(mid) -d(high)
--------------------------------------------------------------------------- */
static void
CALR(sourcefile_t *csfile)
{
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing of DDIR suffix gave syntax error */

    switch(ddirmode) {
        case ddirmode_ib: /* CALR.IB [cc],reladdress16 */
            StreamCodeByte (csfile, 0xdd);
            CallJpZ80 (csfile, 0xCD, 0xC4);
            ProcessPcRelativeExpr (csfile, RANGE_LSB_PCREL16, 2);
            break;

        case ddirmode_iw: /* CALR.IW [cc],reladdress24 */
            StreamCodeByte (csfile, 0xfd);
            CallJpZ80 (csfile, 0xCD, 0xC4);
            ProcessPcRelativeExpr (csfile, RANGE_LSB_PCREL24, 2);
            break;

        case ddirmode_notspecfied:
            /* no immediate address directive specified, default is CALR [cc],reladdress8 */
            StreamCodeByte (csfile, 0xed);
            CallJpZ80 (csfile, 0xCD, 0xC4);
            ProcessPcRelativeExpr (csfile, RANGE_PCREL8, 2);
            break;

        default:
            ReportError (csfile, Err_Syntax); /* only CALR.IB or CALR.IW suppported */
    }
}


/**
 * @brief INC/DEC (HL) | (YX|IY+d)
 * @param csfile
 * @param opcode base opcode for instruction group (INC or DEC)
 * @param ddirmode
 */
static void
IncDecIndrct8bitZ380 (sourcefile_t *csfile, int opcode, enum ddirmodes ddirmode)
{
    enum z80ia rr = CheckIndirectAddrMnem (csfile);

    switch (rr) {
        case z80ia_hl:
            /* INC/DEC (HL) */
            StreamCodeByte (csfile, (unsigned char) (48 + opcode));
            break;

        case z80ia_ix:
        case z80ia_iy:
            /* INC/DEC (IX|IY+d) */
            if (ddirmode != ddirmode_notspecfied)
                DDIRinstr(csfile, ddirmode);
            StreamCodeByte (csfile, (rr == z80ia_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, (unsigned char) (48 + opcode));
            DDirImmediateOffset(csfile, ddirmode, 2);
            break;

        default:
            ReportError (csfile, Err_Syntax);
            break;
    }
}


static void
IncDec_z380_(sourcefile_t *csfile, int opc8, int opc16)
{
    enum z80rr reg16;
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing of DDIR suffix gave syntax error */

    if ((reg16 = CheckReg16Mnem (csfile)) == z80rr_unknown) {
        /* 16 bit register wasn't found - try to evaluate the 8bit version */
        if (csfile->sym == lparen) {
            IncDecIndrct8bitZ380 (csfile, opc8, ddirmode);
        } else {
            /* no indirect addressing, try to get an 8bit register */
            IncDec8bitRegZ80(csfile, opc8);
        }
    } else
        IncDec16bit(csfile, reg16, opc16);
}


static void
DEC_z380_ (sourcefile_t *csfile)
{
    IncDec_z380_(csfile, 5, 16);
}


static void
INC_z380_ (sourcefile_t *csfile)
{
    IncDec_z380_(csfile, 4, 3);
}


/* ---------------------------------------------------------------------------
    DECW rr

    alias for original Z80 DEC rr (BC, DE, HL, SP, IX, IY)
--------------------------------------------------------------------------- */
static void
DECW (sourcefile_t *csfile)
{
    if (GetSym(csfile) == name)
        IncDec16bit(csfile, CheckReg16Mnem(csfile), 11);
    else
        ReportError (csfile, Err_Syntax);
}


/* ---------------------------------------------------------------------------
    EXTS [A]

    Extend Sign into HL

    11101101 01100101
--------------------------------------------------------------------------- */
static void
EXTS(sourcefile_t *csfile)
{
    switch(GetSym(csfile)) {
        case newline:
            break;

        case name:
            if (CheckReg8Mnem(csfile) != z80r_a)
                ReportError (csfile, Err_IllegalIdent);
            break;
        default:
            ReportError (csfile, Err_Syntax);
            return;
    }

    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0x65);
}


/* ---------------------------------------------------------------------------
    EXTSW [HL]

    HL is sign-extended into 32bit

    11101101 01110101
--------------------------------------------------------------------------- */
static void
EXTSW(sourcefile_t *csfile)
{
    if (FetchOptReg16Mnem(csfile, z80r_hl) == true) {
        StreamCodeByte (csfile, 0xed);
        StreamCodeByte (csfile, 0x75);
    }
}


/* ---------------------------------------------------------------------------
    INA[.ddir] A,(nn)

    Input direct from port address (byte)

    11101101 11011011 -n(low)- -n(high)
--------------------------------------------------------------------------- */
static void
INA (sourcefile_t *csfile)
{
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (CheckReg8Mnem (csfile) != z80r_a) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }
    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }
    if (GetSym(csfile) != lparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr(), if specified */

    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xdb);

    GetSym(csfile);
    DDirImmediateAddress(csfile, NULL, ddirmode, 2);

    if (csfile->sym != rparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }
}


/* ---------------------------------------------------------------------------
    INAW[.ddir] HL,(nn)

    Input direct from port address (word)

    11111101 11011011 -n(low)- -n(high)
--------------------------------------------------------------------------- */
static void
INAW (sourcefile_t *csfile)
{
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (CheckReg16Mnem (csfile) != z80r_hl) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }
    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }
    if (GetSym(csfile) != lparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr(), if specified */

    StreamCodeByte (csfile, 0xfd);
    StreamCodeByte (csfile, 0xdb);

    GetSym(csfile);

    DDirImmediateAddress(csfile, NULL, ddirmode, 2);

    if (csfile->sym != rparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }
}


/* ---------------------------------------------------------------------------
    INCW rr

    alias for original Z80 INC rr (BC, DE, HL, SP, IX, IY)
--------------------------------------------------------------------------- */
static void
INCW (sourcefile_t *csfile)
{
    if (GetSym(csfile) == name)
        IncDec16bit(csfile, CheckReg16Mnem(csfile), 3);
    else
        ReportError (csfile, Err_Syntax);
}


/* ---------------------------------------------------------------------------
    Input and Decrement (Word)

    INDW

    11101101 11101010
--------------------------------------------------------------------------- */
static void
INDW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xea);
}


/* ---------------------------------------------------------------------------
    Input, decrement and repeat (Word)

    INDWR

    11101101 11111010
--------------------------------------------------------------------------- */
static void
INDRW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xfa);
}


/* ---------------------------------------------------------------------------
    Input and Inrement (Word)

    INIW

    11101101 11100010
--------------------------------------------------------------------------- */
static void
INIW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xe2);
}


/* ---------------------------------------------------------------------------
    Input, Inrement and repeat (Word)

    INIRW

    11101101 11100010
--------------------------------------------------------------------------- */
static void
INIRW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xf2);
}


/* ---------------------------------------------------------------------------
    Input Word

    INW rr,(c)

    11011101 01rrr000, rrr: 000 for BC, 010 for DE, 111 for HL
--------------------------------------------------------------------------- */
static void
INW (sourcefile_t *csfile)
{
    enum z80rr dstreg;

    if (GetSym(csfile) != name) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    dstreg = CheckReg16Mnem (csfile);

    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (GetSym(csfile) != lparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    GetSym(csfile);
    if (CheckReg8Mnem (csfile) != z80r_c) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }

    GetSym(csfile); /* fetch ')' */
    StreamCodeByte (csfile, 0xdd);

    switch (dstreg) {
        case z80r_bc:
            /* IN bc,(c) */
            StreamCodeByte (csfile, 0x40);
            break;

        case z80r_de:
            /* IN de,(c) */
            StreamCodeByte (csfile, 0x50);
            break;

        case z80r_hl:
            /* IN hl,(c) */
            StreamCodeByte (csfile, 0x78);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            break;
    }
}


/* ---------------------------------------------------------------------------
    Complement HL Register

    CPLW [HL]

    11011101 00101111
--------------------------------------------------------------------------- */
static void
CPLW(sourcefile_t *csfile)
{
    if (FetchOptReg16Mnem(csfile, z80r_hl) == true) {
        StreamCodeByte (csfile, 0xdd);
        StreamCodeByte (csfile, 0x2f);
    }
}


/* ---------------------------------------------------------------------------
    Negate HL Register

    NEGW [HL]

    11101101 01010100
--------------------------------------------------------------------------- */
static void
NEGW(sourcefile_t *csfile)
{
    if (FetchOptReg16Mnem(csfile, z80r_hl) == true) {
        StreamCodeByte (csfile, 0xed);
        StreamCodeByte (csfile, 0x54);
    }
}


/* ---------------------------------------------------------------------------
    Load and decrement (Word)

    LDDW

    11101101 11101000
--------------------------------------------------------------------------- */
static void
LDDW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xe8);
}


/* ---------------------------------------------------------------------------
    Load, decrement and repeat (Word)

    LDDRW

    11101101 11111000
--------------------------------------------------------------------------- */
static void
LDDRW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xf8);
}


/* ---------------------------------------------------------------------------
    Load and increment (Word)

    LDIW

    11101101 11100000
--------------------------------------------------------------------------- */
static void
LDIW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xe0);
}


/* ---------------------------------------------------------------------------
    Load, increment and repeat (Word)

    LDIRW

    11101101 11110000
--------------------------------------------------------------------------- */
static void
LDIRW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xf0);
}


/* ---------------------------------------------------------------------------
    Mode test

    MTEST

    11011101 11001111
--------------------------------------------------------------------------- */
static void
MTEST(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xdd);
    StreamCodeByte (csfile, 0xcf);
}


/* ---------------------------------------------------------------------------
    Output, decrement and repeat (word)

    OTDRW

    11101101 11111011
--------------------------------------------------------------------------- */
static void
OTDRW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xfb);
}


/* ---------------------------------------------------------------------------
    Output, increment and repeat (word)

    OTIRW

    11101101 11110011
--------------------------------------------------------------------------- */
static void
OTIRW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xf3);
}


/* ---------------------------------------------------------------------------
    Output and decrement (word)

    OUTDW

    11101101 11101011
--------------------------------------------------------------------------- */
static void
OUTDW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xeb);
}


/* ---------------------------------------------------------------------------
    Output and increment (word)

    OUTIW

    11101101 11100011
--------------------------------------------------------------------------- */
static void
OUTIW(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xe3);
}


/**
 * @brief OUTA[.ddir] (nn),a
 * @param csfile
 */
static void
OUTA (sourcefile_t *csfile)
{
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (csfile->sym != lparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr(), if specified */

    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0xd3);

    GetSym(csfile);
    DDirImmediateAddress(csfile, NULL, ddirmode, 2);
    if (asmerror == true)
        return; /* illegal address expression ... */

    if (csfile->sym != rparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    GetSym(csfile);
    if (CheckReg8Mnem (csfile) != z80r_a) {
        ReportError (csfile, Err_IllegalIdent);
    }
}


/**
 * @brief OUTAW[.ddir] (nn),hl
 * @param csfile
 */
static void
OUTAW (sourcefile_t *csfile)
{
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (csfile->sym != lparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr(), if specified */

    StreamCodeByte (csfile, 0xfd);
    StreamCodeByte (csfile, 0xd3);

    GetSym(csfile);
    DDirImmediateAddress(csfile, NULL, ddirmode, 2);
    if (asmerror == true)
        return; /* illegal address expression ... */

    if (csfile->sym != rparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    GetSym(csfile);
    if (CheckReg16Mnem(csfile) != z80r_hl) {
        ReportError (csfile, Err_IllegalIdent);
    }
}


/**
 * @brief OUT (n),A  OUT (C),r  OUT (C),r
 * @param csfile
 */
static void
OUT_z380_ (sourcefile_t *csfile)
{
    enum z80r reg8;

    if (GetSym(csfile) != lparen) {
        ReportError (csfile, Err_Syntax);
    }

    GetSym(csfile);
    if (CheckReg8Mnem (csfile) == z80r_c) {
        /* OUT (C) */
        if (GetSym(csfile) != rparen) {
            ReportError (csfile, Err_Syntax);
            return;
        }
        if (GetSym(csfile) != comma) {
            ReportError (csfile, Err_Syntax);
            return;
        }

        GetSym(csfile);
        reg8 = CheckReg8Mnem (csfile);
        if (reg8 == z80r_unknown) {
            /*  Z380: OUT (C),n */
            StreamCodeByte (csfile, 0xed);
            StreamCodeByte (csfile, 0x71);
            ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 2);
        } else {
            OUT_C_r8(csfile, reg8);
        }
    } else {
        OUT_N_A(csfile);
    }
}


/* ---------------------------------------------------------------------------
    Output (word)

    OUTW (C),RR | nn

    11011101 01rrr 001, rrr = 000 for BC, 010 for DE, 111 for HL
    11111101 01111001 -n(low)- -n(high)
--------------------------------------------------------------------------- */
static void
OUTW(sourcefile_t *csfile)
{
    if (GetSym(csfile) != lparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }
    GetSym(csfile);
    if (CheckReg8Mnem (csfile) != z80r_c) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }
    if (GetSym(csfile) != rparen) {
        ReportError (csfile, Err_Syntax);
        return;
    }
    if (GetSym(csfile) != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    /* OUTW (C), */
    GetSym(csfile);
    switch (CheckReg16Mnem(csfile)) {
        case z80r_bc:
            StreamCodeByte (csfile, 0xdd);
            StreamCodeByte (csfile, 0x41);
            return;

        case z80r_de:
            StreamCodeByte (csfile, 0xdd);
            StreamCodeByte (csfile, 0x51);
            return;

        case z80r_hl:
            StreamCodeByte (csfile, 0xdd);
            StreamCodeByte (csfile, 0x79);
            return;

        case z80rr_unknown:
            /* probably is part of constant expression */
            break;

        default:
            /* AF, SP & Index registers are not defined */
            ReportError (csfile, Err_IllegalIdent);
            return;
    }

    /* fall-back to OUTW (C),nn */
    StreamCodeByte (csfile, 0xfd);
    StreamCodeByte (csfile, 0x79);
    ProcessExpr (csfile, NULL, RANGE_LSB_16CONST, 2);
}


/* ---------------------------------------------------------------------------
    Return from breakpoint

    RETB

    11101101 01010101
--------------------------------------------------------------------------- */
static void
RETB(const sourcefile_t *csfile)
{
    StreamCodeByte (csfile, 0xed);
    StreamCodeByte (csfile, 0x55);
}


/* ---------------------------------------------------------------------------
    Instruction Decoder Directive

    This instruction is prohibited as stand-alone instruction, but used as
    ddir suffix for relevant instructions

    DDIR wim

    wim:
    000 W       Word mode
    001 IB,W    Immediate byte, Word mode
    010 IW,W    Immediate word, Word mode
    011 IB      Immediate byte
    100 LW      Long Word mode
    101 IB,LW   Immediate byte, Long Word mode
    110 IW,LW   Immediate word, Long Word mode
    111 IW      Immediate word

    11w11101 110000im
--------------------------------------------------------------------------- */
static void
DDIR(const sourcefile_t *csfile)
{
    ReportSrcAsmMessage (csfile, "Use ddir as a suffix being part of instruction, eg. ld.iw");
}


/* ---------------------------------------------------------------------------
    Set Control Bit

    SETC LCK    (Lock Mode)
    SETC LW     (Long Word Mode)
    SETC XM     (Extended Mode)

    0b11mm1101 11110111
--------------------------------------------------------------------------- */
static void
SETC(sourcefile_t *csfile)
{
    if (GetSym(csfile) == name) {
        if (strcmp(csfile->ident,"LCK") == 0)
            StreamCodeByte (csfile, 0xed); /* 0b11mm1101, mm = 10 */
        else if (strcmp(csfile->ident,"LW") == 0)
            StreamCodeByte (csfile, 0xdd); /* 0b11mm1101, mm = 01 */
        else if (strcmp(csfile->ident,"XM") == 0)
            StreamCodeByte (csfile, 0xfd); /* 0b11mm1101, mm = 11 */
        else
            ReportError (csfile, Err_IllegalIdent);

        StreamCodeByte (csfile, 0xf7);
    } else {
        ReportError (csfile, Err_Syntax);
    }
}


/* ---------------------------------------------------------------------------
    Reset Control Bit

    RESC LCK    (Lock Mode)
    RESC LW     (Long Word Mode)

    0b11mm1101 11111111
--------------------------------------------------------------------------- */
static void
RESC(sourcefile_t *csfile)
{
    if (GetSym(csfile) == name) {
        if (strcmp(csfile->ident,"LCK") == 0)
            StreamCodeByte (csfile, 0xed); /* 0b11mm1101, mm = 10 */
        else if (strcmp(csfile->ident,"LW") == 0)
            StreamCodeByte (csfile, 0xdd); /* 0b11mm1101, mm = 01 */
        else
            ReportError (csfile, Err_IllegalIdent);

        StreamCodeByte (csfile, 0xff);
    } else {
        ReportError (csfile, Err_Syntax);
    }
}


/*!
 * \brief SWAP BC | DE | HL | IX | IY instruction
 * \details
 * SWAP BC | DE | HL
 * 11101101 00rr1110
 *
 * SWAP IX | IY
 * 11y11101 00111110
 *
 * \param csfile
 */
static void
SWAP (sourcefile_t *csfile)
{
    enum z80rr qq;

    if (GetSym(csfile) != name) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    switch (qq = CheckReg16Mnem (csfile)) {
        case z80r_bc:
        case z80r_de:
        case z80r_hl:
            StreamCodeByte (csfile, 0xed);
            StreamCodeByte (csfile, (qq == z80r_hl) ? 0x3e : (unsigned char) 0x0e + (unsigned char) qq * 16);
            break;

        case z80r_ix:
        case z80r_iy:
            StreamCodeByte (csfile, (qq == z80r_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, 0x3e);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief Common code generator for Z80/Z380 PUSH / POP instructions
 * \param csfile
 * \param opcode
 */
static void
PUSHPOP_z380_ (sourcefile_t *csfile, int opcode)
{
    enum z80rr qq;
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing of DDIR suffix gave syntax error */

    /* multiple register pairs may be pushed / popped using comma separator */
    do {
        if (ddirmode != ddirmode_notspecfied)
            DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr() */

        switch (qq = CheckReg16Mnem (csfile)) {
            case z80r_bc:
            case z80r_de:
            case z80r_hl:
            case z80r_af:
            case z80r_ix:
            case z80r_iy:
                /* the Z80-compatible group */
                PushPop_instr (csfile, opcode, qq);
                break;

            case z80rr_unknown:
                /* "PUSH SR"? */
                if (opcode == 197 && strcmp (csfile->ident, "SR") == 0) {
                    StreamCodeByte (csfile, 0xed);
                    StreamCodeByte (csfile, 0xc5);
                    break;
                }

                /* "POP SR"? */
                if (opcode == 193 && strcmp (csfile->ident, "SR") == 0) {
                    StreamCodeByte (csfile, 0xed);
                    StreamCodeByte (csfile, 0xc1);
                    break;
                }

                if (opcode != 197) {
                    /* POP immediate has no meaning! */
                    ReportError (csfile, Err_Syntax);
                    return;
                }

                /* PUSH immediate constant expression */
                StreamCodeByte (csfile, 0xfd);
                StreamCodeByte (csfile, 0xf5);

                /* immediate constant size (16bit, 24bit or 32bit) depends on DDIR .IB, .IW or none */
                DDirImmediateAddress(csfile, NULL, ddirmode, 2);
                break;

            default:
                /* push/pop sp is illegal */
                ReportError (csfile, Err_IllegalIdent);
                break;
        }

        /* more registers to push/pop? */
        if (GetSym(csfile) == comma)
            GetSym(csfile);

        /* stop parsing if a ':' (instruction separator) or end of line has been reached */
    } while (csfile->sym != newline && csfile->sym != colon);
}


/*!
 * \brief PUSH [ddir] BC | DE | HL | IX | IY | SR | <immediate expr>
 * \param csfile
 */
static void
PUSH_z380_ (sourcefile_t *csfile)
{
    PUSHPOP_z380_(csfile, 197);
}


/*!
 * \brief POP [ddir] BC | DE | HL | IX | IY | SR | <immediate expr>
 * \param csfile
 */
static void
POP_z380_ (sourcefile_t *csfile)
{
    PUSHPOP_z380_(csfile, 193);
}


/*!
 * \brief validate Z380 Control Register mnemonics
 *
 * \param csfile
 * \return return a recognized enumerated type, or ctlr_unknown
 */
static enum ctlrr
CheckCtlRegMnem (const sourcefile_t *csfile)
{
    if (csfile->sym != name)
        return ctlr_unknown;

    if ( csfile->identlen == 1 && csfile->ident[0] == 'A')
        return ctlr_a;

    switch(csfile->identlen) {
        case 2:
            if (strcmp (csfile->ident, "HL") == 0) {
                return ctlr_hl;
            } else if (strcmp (csfile->ident, "SR") == 0) {
                return ctlr_sr;
            }
            break;

        case 3:
            if (strcmp (csfile->ident, "XSR") == 0) {
                return ctlr_xsr;
            } else if (strcmp (csfile->ident, "DSR") == 0) {
                return ctlr_dsr;
            } else if (strcmp (csfile->ident, "YSR") == 0) {
                return ctlr_ysr;
            }
            break;

        default:
            /* any other lengths do not match with any Control Register mnemonics */
            break;
    }

    return ctlr_unknown;
}


/*!
 * \brief LDCTL [.w | .lw] instructions
 * \param csfile
 */
static void
LDCTL(sourcefile_t *csfile)
{
    enum ctlrr dst;
    enum ctlrr src;
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing of DDIR suffix gave syntax error */

    switch( dst = CheckCtlRegMnem(csfile) ) {
        case ctlr_a:
            /* LDCTL A, XSR | DSR | YSR  */
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            GetSym(csfile);
            src = CheckCtlRegMnem(csfile);
            if (src == ctlr_xsr || src == ctlr_dsr || src == ctlr_ysr) {
                StreamCodeByte (csfile, 0xcd | (unsigned char) src * 16);
                StreamCodeByte (csfile, 0xd0);
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case ctlr_sr:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            GetSym(csfile);
            src = CheckCtlRegMnem(csfile);
            if (src == ctlr_a) {
                /* LDCTL SR,A */
                StreamCodeByte (csfile, 0xdd);
                StreamCodeByte (csfile, 0xc8);
                return;
            }
            if (src == ctlr_hl) {
                /* LDCTL [.w | .lw] SR,HL */
                if (ddirmode != ddirmode_notspecfied)
                    DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr() */

                StreamCodeByte (csfile, 0xed);
                StreamCodeByte (csfile, 0xc8);
                return;
            }

            /* LDCTL SR,n */
            StreamCodeByte (csfile, 0xdd);
            StreamCodeByte (csfile, 0xca);
            ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 2);
            break;

        case ctlr_hl:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            GetSym(csfile);
            if (CheckCtlRegMnem(csfile) == ctlr_sr) {
                /* LDCTL [.w | .lw] HL,SR */
                if (ddirmode != ddirmode_notspecfied)
                    DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr() */

                StreamCodeByte (csfile, 0xed);
                StreamCodeByte (csfile, 0xc0);
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case ctlr_xsr:
        case ctlr_dsr:
        case ctlr_ysr:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            GetSym(csfile);
            src = CheckCtlRegMnem(csfile);
            if (src == ctlr_a) {
                /*  LDCTL XSR | DSR | YSR , A */
                StreamCodeByte (csfile, 0xcd | (unsigned char) dst * 16);
                StreamCodeByte (csfile, 0xd8);
                return;
            }

            /*  LDCTL XSR | DSR | YSR , n */
            StreamCodeByte (csfile, 0xcd | (unsigned char) dst * 16);
            StreamCodeByte (csfile, 0xda);
            ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 2);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/*!
 * \brief CALL [.ib | .iw] [cc,] address  OR  JP [.ib | .iw] [cc,] address instructions
 * \param csfile
 * \param opcodenn the base opcode for CALL or JP address (no CC)
 * \param opcodecc the base opcode for CALL cc,address or JP cc,address
 */
static void
CallJpZ380 (sourcefile_t *csfile, int opcodenn, int opcodecc)
{
    enum z80cc cc;
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing DDIR suffix gave syntax error */

    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr() */

    if ((cc = CheckCondMnem (csfile)) != z80f_unknown) {
        /* JP CC,nn, CALL CC,nn, */
        StreamCodeByte (csfile, (unsigned char) opcodecc + (unsigned char) cc * 8);
        if (GetSym(csfile) == comma) {
            GetSym(csfile);
        } else {
            ReportError (csfile, Err_Syntax);
            return;
        }
    } else {
        /* JP nn, CALL nn */
        StreamCodeByte (csfile, (unsigned char) opcodenn);
    }

    /* immediate constant size (16bit, 24bit or 32bit) depends on DDIR .IB, .IW or none */
    DDirImmediateAddress(csfile, NULL, ddirmode, 2);
}


/*!
 * \brief CALL [.ib | .iw] [cc,] address
 * \param csfile
 */
static void
CALL_z380_(sourcefile_t *csfile)
{
    CallJpZ380 (csfile, 0xCD, 0xC4);
}


/*!
 * \brief JP (HL) | (IX) | (IY)  or  JP [.ib | .iw] [cc,] address
 * \param csfile
 */
static void
JP_z380_(sourcefile_t *csfile)
{
    const unsigned char *fptr = MemfTellPtr(csfile->mf); /* remember pos. after instruction mnemonic */

    if (GetSym(csfile) == lparen) {
        /* generate Z80 opcodes for JP (HL) | (IX) | (IY) */
        GetSym(csfile);
        JP_IndReg16(csfile);
    } else {
        /* base opcode for JP [.ib | .iw] <instr> nn; <instr> cc, nn */
        MemfSeekPtr(csfile->mf, fptr);
        CallJpZ380 (csfile, 195, 194);
    }
}


/*!
 * \brief RLC | RRC | RL | RR | SLA | SRA | SRL | BIT | RES | SET (HL) or [.ib | .iw] (IX|IY+ofs)
 * \param csfile
 * \param opcode
 * \param ddirmode
 */
static void
BitSrcRg8BitIndrct(sourcefile_t *csfile, int opcode, enum ddirmodes ddirmode)
{
    enum z80ia iareg = CheckIndirectAddrMnem (csfile);

    switch (iareg) {
        case z80ia_hl:
            /* (HL)  */
            StreamCodeByte (csfile, 0xCB);
            StreamCodeByte (csfile, (unsigned char) opcode);
            break;

        case z80ia_ix:
        case z80ia_iy:
            if (ddirmode != ddirmode_notspecfied)
                DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr(), if specified */
            StreamCodeByte (csfile, (iareg == z80ia_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, 0xcb);
            DDirImmediateOffset(csfile, ddirmode, 2);
            StreamCodeByte (csfile, (unsigned char) opcode);
            break;

        default:
            ReportError (csfile, Err_Syntax);
            break;
    }
}


/* BIT, SET, RES (Main & IX/IY registers) with Z380 extended immediate offsets */
static void
BitSetResZ380 (sourcefile_t *csfile, int opcode)
{
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);
    int bitnumber = (int) ParseMnemConstant(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing DDIR suffix gave syntax error */

    if (bitnumber < 0 || bitnumber > 7) {
        ReportError (csfile, Err_IntegerRange);
        return;
    }

    /* bit 0 - 7 */
    if (csfile->sym != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    if (GetSym(csfile) == lparen) {
        BitSrcRg8BitIndrct(csfile, opcode + (bitnumber * 8 + 6), ddirmode);
    } else {
        /* no indirect addressing, BIT/RES/SET 8bit register */
        BitSrcRg8bit(csfile, opcode, bitnumber);
    }
}


/* RLC, RRC, RL, RR, SLA, SRA, SRL */
static void
ShiftRotInstrZ380 (sourcefile_t *csfile, int opcode)
{
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing DDIR suffix gave syntax error */

    if (csfile->sym == lparen) {
        BitSrcRg8BitIndrct(csfile, opcode * 8 + 6, ddirmode);
    } else {
        /* no indirect addressing, generate code for 8bit register SHift/Rotate instructions (Z80) */
        RotShift8bitReg (csfile, opcode);
    }
}


static void
BIT_z380_ (sourcefile_t *csfile)
{
    BitSetResZ380 (csfile, 64);
}


static void
RES_z380_ (sourcefile_t *csfile)
{
    BitSetResZ380 (csfile, 128);
}


static void
SET_z380_ (sourcefile_t *csfile)
{
    BitSetResZ380 (csfile, 192);
}


static void
RLC_z380_ (sourcefile_t *csfile)
{
    ShiftRotInstrZ380(csfile, 0);
}


static void
RRC_z380_ (sourcefile_t *csfile)
{
    ShiftRotInstrZ380(csfile, 1);
}


static void
RL_z380_ (sourcefile_t *csfile)
{
    ShiftRotInstrZ380(csfile, 2);
}


static void
RR_z380_ (sourcefile_t *csfile)
{
    ShiftRotInstrZ380(csfile, 3);

}


static void
SLA_z380_ (sourcefile_t *csfile)
{
    ShiftRotInstrZ380(csfile, 4);
}


static void
SRA_z380_ (sourcefile_t *csfile)
{
    ShiftRotInstrZ380(csfile, 5);
}


static void
SRL_z380_ (sourcefile_t *csfile)
{
    ShiftRotInstrZ380(csfile, 7);
}


/**
 * @brief LD (BC),A | LD (BC),rr | LD (DE),A | LD (DE),rr | LD (HL),r | LD (HL),n | LD (HL),rr
 * @param csfile
 * @param dstreg the 16bit destination register BC, DE or HL
 * @param ddirmode
 */
static void
LD_dst_bcdehl_indrct(sourcefile_t *csfile, enum z80ia dstreg, enum ddirmodes ddirmode)
{
    enum z80r src8reg;
    enum z80rr src16reg = z80rr_unknown;

    if (csfile->sym != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    /* if any DDIR suffix has been specified, generate opcodes now */
    if ( ddirmode == ddirmode_w || ddirmode == ddirmode_lw ) {
        DDIRinstr(csfile, ddirmode);
    } else {
        if (ddirmode != ddirmode_notspecfied ) {
            /* illegal suffix, there is no immediate byte or immediate word options for these instructions */
            ReportError (csfile, Err_IllegalIdent);
            return;
        }
    }

    GetSym(csfile);
    if ( (src8reg = CheckReg8Mnem (csfile)) == z80r_unknown)
        src16reg = CheckReg16Mnem(csfile);

    if (dstreg == z80ia_hl && src16reg == z80rr_unknown) {
        /* LD (HL),r | LD (HL),n */
        LD_HL8bit_indrct (csfile, src8reg);
        return;
    }

    /* LD  (BC),A  */
    if (dstreg == z80ia_bc && src8reg == z80r_a) {
        StreamCodeByte (csfile, 2);
        return;
    }

    /* LD  (DE),A  */
    if (dstreg == z80ia_de && src8reg == z80r_a) {
        StreamCodeByte (csfile, 18);
        return;
    }

    if (dstreg == z80ia_hl)
        dstreg = z80ia_hl_z380; /* for Z380 mnemonics, HL = 011b */

    switch (src16reg) {
        case z80r_bc:
        case z80r_de:
        case z80r_hl:
            if (src16reg == z80r_hl)
                src16reg = z80r_hl_z380; /* for Z380 mnemonics, HL = 011b */

            StreamCodeByte (csfile, 0xfd);
            StreamCodeByte (csfile, (unsigned char ) (src16reg * 16 | 0x0c | dstreg));
            break;

        case z80r_ix:
            StreamCodeByte (csfile, 0xdd);
            StreamCodeByte (csfile, (unsigned char ) (dstreg * 16 | 1));
            break;

        case z80r_iy:
            StreamCodeByte (csfile, 0xfd);
            StreamCodeByte (csfile, (unsigned char ) (dstreg * 16 | 1));
            break;
        default:
            ReportError (csfile, Err_IllegalIdent);
    }

}


/**
 * @brief LD[.ddir] (nn),rr  ;  LD[.ddir] (nn),a
 * @param csfile
 * @param ddirmode
 */
static void
LD_z380_address_indrct (sourcefile_t *csfile, enum ddirmodes ddirmode)
{
    enum z80rr sourcereg;
    int lstpatchpos;
    expression_t *addrexpr = ParseNumExpr (csfile);

    if (addrexpr == NULL) {
        return;    /* parse to right bracket failed */
    }

    if (csfile->sym != rparen) {
        ReportError (csfile, Err_Syntax);   /* Right bracket missing! */
        FreeNumExpr (addrexpr);             /* remove this expression again */
        return;
    }

    if (GetSym(csfile) == comma) {
        /* if any DDIR suffix has been specified, generate opcodes now */
        if (ddirmode != ddirmode_notspecfied)
            DDIRinstr(csfile, ddirmode);

        GetSym(csfile);
        switch (sourcereg = CheckReg16Mnem (csfile)) {
            case z80r_hl:
                /* LD  (nn),HL  */
                StreamCodeByte (csfile, 34);
                lstpatchpos = 1;
                break;

            case z80r_bc:
            case z80r_de:     /* LD  (nn),dd  => dd: BC,DE,SP  */
            case z80r_sp:
                StreamCodeByte (csfile, 0xED);
                StreamCodeByte (csfile, (unsigned char) (67 + sourcereg * 16));
                lstpatchpos = 2;
                break;

            case z80r_ix:
                /* LD  (nn),IX */
                StreamCodeByte (csfile, 0xDD);
                StreamCodeByte (csfile, 34);
                lstpatchpos = 2;
                break;

            case z80r_iy:
                /* LD (nn),IY */
                StreamCodeByte (csfile, 0xFD);
                StreamCodeByte (csfile, 34);
                lstpatchpos = 2;
                break;

            case z80rr_unknown:
                if (CheckReg8Mnem (csfile) == z80r_a) {
                    /* LD (nn),A */
                    StreamCodeByte (csfile, 50);
                    lstpatchpos = 1;
                } else {
                    ReportError (csfile, Err_IllegalIdent);
                    FreeNumExpr (addrexpr);
                    return;
                }
                break;

            default:
                ReportError (csfile, Err_IllegalIdent);
                FreeNumExpr (addrexpr);
                return;
        }

        DDirImmediateAddress(csfile, addrexpr, ddirmode, lstpatchpos);
    } else {
        ReportError (csfile, Err_Syntax);
        FreeNumExpr (addrexpr);
    }
}


/*
 * LD[.ib|.iw] (IX|IY+d),r
 * LD[.ib|.iw] (IX|IY+d),n
 */
static void
LD_z380_index8bit_indrct (sourcefile_t *csfile, expression_t *idxoffsetexpr, enum z80ia destreg, enum ddirmodes ddirmode)
{
    enum z80r src8bitreg;
    int opcodeidx;
    int lstoffset;

    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr() , if specified */
    StreamCodeByte (csfile, (destreg == z80ia_ix) ? 0xdd : 0xfd);
    opcodeidx = GetStreamCodePC(csfile); /* know PC to instruction opcode, preset to 0x36 */
    StreamCodeByte (csfile, 0x36);       /* preset 2. opcode to LD (IX|IY+d),n */

    if (idxoffsetexpr != NULL)
        /* update PC of pre-parsed expression to point at actual displacement of IX/IY instruction */
        /* (when expression were originally parsed, the PC was pointing 1st opcode of instruction) */
        idxoffsetexpr->codepos = (size_t) GetStreamCodePC(csfile);

    switch(ddirmode) {
        case ddirmode_ib:       /* xxx.ib creates a 6-byte instruction using 16-bit displacement */
        case ddirmode_ibw:
        case ddirmode_iblw:
            lstoffset = 2+2;
            if (idxoffsetexpr == NULL)
                StreamCodeWord (csfile, 0); /* idiotic 16-bit 0 displacement specified */
            else
                /* listing patch offset is DDIR + 2-byte opcode, then displacement */
                ProcessExpr (csfile, idxoffsetexpr, RANGE_LSB_16SIGN, lstoffset);
            break;

        case ddirmode_iw:       /* xxx.iw creates a 7-byte instruction using 24-bit displacement */
        case ddirmode_iww:
        case ddirmode_iwlw:
            lstoffset = 2+2;
            if (idxoffsetexpr == NULL)
                StreamCodeInt24 (csfile, 0); /* idiotic 24-bit 0 displacement specified */
            else
                ProcessExpr (csfile, idxoffsetexpr, RANGE_LSB_24SIGN, lstoffset);
            break;

        default:
            lstoffset = 2;
            /* ddirmode_unknown and irrelevant modes - default 8bit signed displacement */
            if (idxoffsetexpr == NULL)
                StreamCodeByte (csfile, 0); /* no displacement specified, use 0 */
            else
                ProcessExpr (csfile, idxoffsetexpr, RANGE_8SIGN, lstoffset);
    }

    switch (src8bitreg = CheckReg8Mnem (csfile)) {
        case z80r_f:     /* xxx ,F illegal */
        case z80r_i:     /* xxx ,I illegal */
        case z80r_r:     /* xxx ,R illegal */
            ReportError (csfile, Err_IllegalIdent);
            break;

        case z80r_unknown:
            /* Execute, store & patch 8bit expression for <n> */
            ProcessExpr (csfile, NULL, RANGE_8UNSIGN, lstoffset+1);
            break;

        default:
            /* check that IXh/l and IYh/l are not mixed, which is not supported by Z80 */
            if (destreg == z80ia_ix && (src8bitreg == z80r_iyh || src8bitreg == z80r_iyl) ) {
                /* LD (IX+d),iyh/l */
                ReportError (csfile, Err_IllegalIdent);
                return;
            }
            if (destreg == z80ia_iy && (src8bitreg == z80r_ixh || src8bitreg == z80r_ixl) ) {
                /* LD (IY+d),ixh/l */
                ReportError (csfile, Err_IllegalIdent);
                return;
            }

            /* LD  (IX|IY+d),r */
            UpdateStreamCodeByte (csfile, opcodeidx, (int) (112 + src8bitreg));
            break;
    }
}


/**
 * @brief "LD (register), xxx" or "LD (nn),xxx" instructions
 * @param csfile
 */
static void
LD_z380_dst_indrct(sourcefile_t *csfile, enum ddirmodes ddirmode)
{
    enum z80rr srcreg16;
    expression_t *idxoffsetexpr = NULL;
    enum z80ia iareg = CheckIndirectAddrMnem (csfile);

    switch (iareg) {
        case z80ia_bc:
        case z80ia_de:
        case z80ia_hl:
            /* LD  (HL),XX | LD (DE),XX | LD (BC),XX */
            LD_dst_bcdehl_indrct(csfile, iareg, ddirmode);
            break;

        case z80ia_sp: /* LD[.ddir] (SP+d),rr */
        case z80ia_ix: /* LD[.ddir] (IX|IY+d),X */
        case z80ia_iy:

            if (csfile->sym != rparen) {
                /* an index offset expression is probably specified, parse it (PC for expression is incorrect, though) */
                if ((idxoffsetexpr = ParseNumExpr (csfile)) == NULL) {
                    return;    /* failed to parse to right bracket */
                }
            }

            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);   /* comma missing! */
                FreeNumExpr (idxoffsetexpr);        /* remove this expression again */
                return;
            }

            GetSym(csfile);
            srcreg16 = CheckReg16Mnem (csfile);
            switch(srcreg16) {
                case z80r_bc:
                case z80r_de:
                case z80r_hl:
                case z80r_ix:
                case z80r_iy:
                    if (iareg == z80ia_sp)
                        LD_z380_IndirectIXIYSP_16bitSrcReg(csfile, ddirmode, idxoffsetexpr, z80ia_sp, srcreg16, 9);
                    else
                        LD_z380_IndirectIXIYSP_16bitSrcReg(csfile, ddirmode, idxoffsetexpr, iareg, srcreg16, 11);
                    break;

                default:
                    /* parse for 8bit register or 8bit expression source */
                    LD_z380_index8bit_indrct (csfile, idxoffsetexpr, iareg, ddirmode);
            }
            break;

        case z80ia_nn:
            /* LD  (nn),rr  ;  LD  (nn),A */
            LD_z380_address_indrct (csfile, ddirmode);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/**
 * @brief LD[.ddir] (SP|IX|IY+d),rr
 * @param csfile the current source file line
 * @param ddirmode the immediate and/or word/long word suffix modes
 * @param idxoffsetexpr the index offset expression
 * @param dstreg16 destination indirect register SP, IX or IY
 * @param srcreg16 source registers BC, DE, HL, IX or IY
 * @param opcode base opcode for IX/IY or SP instruction group
 */
static void
LD_z380_IndirectIXIYSP_16bitSrcReg(sourcefile_t *csfile, enum ddirmodes ddirmode, expression_t *idxoffsetexpr, enum z80ia dstreg16, enum z80rr srcreg16, int opcode)
{
    /* if any DDIR suffix has been specified, generate 2-byte opcodes now */
    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode);

    if (
        (srcreg16 == z80r_ix && dstreg16 == z80ia_ix) ||
        (srcreg16 == z80r_iy && dstreg16 == z80ia_iy)
       ) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }

    /* define base instruction opcode group */
    switch(dstreg16) {
        case z80ia_ix:
            StreamCodeByte (csfile, 0xdd);
            break;

        case z80ia_sp:
            if (srcreg16 == z80r_iy)
                StreamCodeByte (csfile, 0xfd);
            else
                StreamCodeByte (csfile, 0xdd);
            break;

        case z80ia_iy:
                StreamCodeByte (csfile, 0xfd);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            return;
    }
    StreamCodeByte (csfile, 0xcb);

    if (idxoffsetexpr != NULL)
        /* update PC of pre-parsed expression to point at actual displacement of IX/IY instruction */
        /* (when expression were originally parsed, the PC was pointing at 1st opcode of instruction) */
        idxoffsetexpr->codepos = (size_t) GetStreamCodePC(csfile);

    switch(ddirmode) {
        case ddirmode_ib:       /* xxx.ib creates a 6-byte instruction using 16-bit displacement */
        case ddirmode_ibw:
        case ddirmode_iblw:
            if (idxoffsetexpr == NULL)
                StreamCodeWord (csfile, 0); /* idiotic 16-bit 0 displacement specified */
            else
                ProcessExpr (csfile, idxoffsetexpr, RANGE_LSB_16SIGN, 2+2);
            break;

        case ddirmode_iw:       /* xxx.iw creates a 7-byte instruction using 24-bit displacement */
        case ddirmode_iww:
        case ddirmode_iwlw:
            if (idxoffsetexpr == NULL)
                StreamCodeInt24 (csfile, 0); /* idiotic 24-bit 0 displacement specified */
            else
                ProcessExpr (csfile, idxoffsetexpr, RANGE_LSB_24SIGN, 2+2);
            break;

        default:
            /* ddirmode_unknown and irrelevant modes - default 8bit signed displacement */
            if (idxoffsetexpr == NULL)
                StreamCodeByte (csfile, 0); /* no displacement specified, use 0 */
            else
                ProcessExpr (csfile, idxoffsetexpr, RANGE_8SIGN, 2);
    }

    switch(srcreg16) {
        case z80r_bc:
        case z80r_de:
        case z80r_hl:
            if (srcreg16 == z80r_hl)
                srcreg16 = z80r_hl_z380;
            StreamCodeByte (csfile, (unsigned char) srcreg16*16 | (unsigned char) opcode);
            break;

        case z80r_ix:
        case z80r_iy:
            StreamCodeByte (csfile, (unsigned char) 2*16 | (unsigned char) opcode);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/**
 * @brief LD[.ddir] rr,(SP|IX|IY+d)
 * @param csfile    the current source file line parsing
 * @param ddirmode  the immediate and/or word/long word suffix modes
 * @param dstreg16  destination registers BC, DE, HL, IX or IY
 * @param srcreg16  IX, IY or SP
 * @param opcode    base opcode for IX/IY or SP instruction group
 */
static void
LD_z380_16bitDestReg_IndirectIXIYSP(sourcefile_t *csfile, enum ddirmodes ddirmode, enum z80rr dstreg16, enum z80ia srcreg16, int opcode)
{
    /* if any DDIR suffix has been specified, generate 2-byte opcodes now */
    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode);

    if (
        (srcreg16 == z80ia_ix && dstreg16 == z80r_ix) ||
        (srcreg16 == z80ia_iy && dstreg16 == z80r_iy)
       ) {
        ReportError (csfile, Err_IllegalIdent);
        return;
    }

    /* define base instruction opcode group */
    switch(srcreg16) {
        case z80ia_ix:
            StreamCodeByte (csfile, 0xdd);
            break;

        case z80ia_sp:
            if (dstreg16 == z80r_iy)
                StreamCodeByte (csfile, 0xfd);
            else
                StreamCodeByte (csfile, 0xdd);
            break;

        case z80ia_iy:
                StreamCodeByte (csfile, 0xfd);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            return;
    }
    StreamCodeByte (csfile, 0xcb);
    DDirImmediateOffset(csfile, ddirmode, 2);

    switch(dstreg16) {
        case z80r_bc:
        case z80r_de:
        case z80r_hl:
            if (dstreg16 == z80r_hl)
                dstreg16 = z80r_hl_z380;
            StreamCodeByte (csfile, (unsigned char) dstreg16*16 | (unsigned char) opcode);
            break;

        case z80r_ix:
        case z80r_iy:
            StreamCodeByte (csfile, (unsigned char) 2*16 | (unsigned char) opcode);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/**
 * @brief LD rr, XXX
 * @details
 * LD[.ddir]  rr,(BC) | LD rr,(DE) | LD rr,(HL)
 * LD[.ddir]  rr,(SP+d)
 * LD[.ddir]  rr,(IX/IY+d)
 * @param csfile
 * @param ddirmode
 * @param dstreg16
 * @param srcreg16
 */
static void
LD_z380_16bitDestReg_SrcInd16bitReg(sourcefile_t *csfile, enum ddirmodes ddirmode, enum z80rr dstreg16, enum z80ia srcreg16)
{
    switch (srcreg16) {
        case z80ia_bc:
        case z80ia_de:
        case z80ia_hl:
            /* LD  rr,(BC) | LD rr,(DE) | LD rr,(HL) */
            if ( ddirmode == ddirmode_w || ddirmode == ddirmode_lw ) {
                /* if any DDIR suffix has been specified, generate opcodes now */
                DDIRinstr(csfile, ddirmode);
            } else {
                if (ddirmode != ddirmode_notspecfied ) {
                    /* illegal suffix, there is no immediate byte or immediate word options for these instructions */
                    ReportError (csfile, Err_IllegalIdent);
                    return;
                }
            }

            if (srcreg16 == z80ia_hl)
                srcreg16 = z80ia_hl_z380; /* for Z380 mnemonics, HL = 011b */

            switch (dstreg16) {
                case z80r_bc:
                case z80r_de:
                case z80r_hl:
                    if (dstreg16 == z80r_hl)
                        dstreg16 = z80r_hl_z380; /* for Z380 mnemonics, HL = 011b */

                    StreamCodeByte (csfile, 0xdd);
                    StreamCodeByte (csfile, (unsigned char ) (dstreg16 * 16 | 0x0c | srcreg16));
                    break;

                case z80r_ix:
                    StreamCodeByte (csfile, 0xdd);
                    StreamCodeByte (csfile, (unsigned char ) (srcreg16 * 16 | 3));
                    break;

                case z80r_iy:
                    StreamCodeByte (csfile, 0xfd);
                    StreamCodeByte (csfile, (unsigned char ) (srcreg16 * 16 | 3));
                    break;

                default:
                    /* ld af,(rr)  and  ld sp,(rr) illegal */
                    ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80ia_sp:
            /* LD[.ddir]  rr,(SP+d) */
            LD_z380_16bitDestReg_IndirectIXIYSP(csfile, ddirmode, dstreg16, srcreg16, 1);
            break;

        case z80ia_ix:
        case z80ia_iy:
            /* LD[.ddir]  rr,(IX|IY+d) */
            LD_z380_16bitDestReg_IndirectIXIYSP(csfile, ddirmode, dstreg16, srcreg16, 3);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


static void
LD_z380_16bitDestReg_SrcIndAddess(sourcefile_t *csfile, enum ddirmodes ddirmode, enum z80rr dstreg16)
{
    int lstpatchpos;

    /* if any DDIR suffix has been specified, generate opcodes now */
    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode);

    switch (dstreg16) {
        case z80r_hl:
            /* LD HL, (nn) */
            StreamCodeByte (csfile, 42);
            lstpatchpos = 1;
            break;

        case z80r_ix:
            /* LD IX, (nn) */
            StreamCodeByte (csfile, 0xdd);
            StreamCodeByte (csfile, 42);
            lstpatchpos = 2;
            break;

        case z80r_iy:
            /* LD IY, (nn) */
            StreamCodeByte (csfile, 0xfd);
            StreamCodeByte (csfile, 42);
            lstpatchpos = 2;
            break;

        default:
            /* LD BC | DE | SP, (nn) */
            StreamCodeByte (csfile, 0xed);
            StreamCodeByte (csfile, (unsigned char) (75 + dstreg16 * 16));
            lstpatchpos = 2;
            break;
    }

    DDirImmediateAddress(csfile, NULL, ddirmode, lstpatchpos);
}


static void
LD_z380_16bitDestReg_SrcindDirect(sourcefile_t *csfile, enum ddirmodes ddirmode, enum z80rr dstreg16)
{
    enum z80ia srcreg16 = CheckIndirectAddrMnem (csfile);

    if (srcreg16 == z80ia_unknown || srcreg16 == z80ia_nn) {
        LD_z380_16bitDestReg_SrcIndAddess(csfile, ddirmode, dstreg16);
    } else {
        LD_z380_16bitDestReg_SrcInd16bitReg(csfile, ddirmode, dstreg16, srcreg16);
    }
}


/*
 * LD  r,(HL)
 * LD  r,(IX|IY+d)
 * LD  a,(nn)
 * LD  a,(BC|DE)
 */
static void
LD_z380_8bitdstreg_indrct (sourcefile_t *csfile, enum ddirmodes ddirmode, enum z80r destreg)
{
    enum z80ia sourcereg = CheckIndirectAddrMnem (csfile);

    switch (sourcereg) {
        case z80ia_hl:
            /* LD r,(HL) */
            StreamCodeByte (csfile, (unsigned char) (64 + destreg * 8 + 6));
            break;

        case z80ia_ix:
        case z80ia_iy:
            /* check that IXh/l and IYh/l are not mixed, which is not supported by Z80 */
            if ( destreg == z80r_ixh || destreg == z80r_ixl || destreg == z80r_iyh || destreg == z80r_iyl) {
                /* LD ixh/l,(IY+d) */
                ReportError (csfile, Err_IllegalIdent);
                return;
            }

            /* LD[.ib|.iw]  r,(IX|IY+d) */
            if (ddirmode != ddirmode_notspecfied)
                DDIRinstr(csfile, ddirmode); /* PC += 2 by DDIRinstr() , if specified */
            StreamCodeByte (csfile, (sourcereg == z80ia_ix) ? 0xdd : 0xfd);
            StreamCodeByte (csfile, (unsigned char) (64 + destreg * 8 + 6));
            DDirImmediateOffset(csfile, ddirmode, 3);
            break;

        case z80ia_nn:
            /* LD  A,(nn) */
            if (destreg == z80r_a) {
                /* if any DDIR suffix has been specified, generate opcodes now */
                if (ddirmode != ddirmode_notspecfied)
                    DDIRinstr(csfile, ddirmode);

                StreamCodeByte (csfile, 58);
                DDirImmediateAddress(csfile, NULL, ddirmode, 1);
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80ia_bc:
            if (destreg == z80r_a) {
                /* LD A,(BC) */
                StreamCodeByte (csfile, 10);
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80ia_de:
            if (destreg == z80r_a) {
                /* LD A,(DE) */
                StreamCodeByte (csfile, 26);
            } else {
                ReportError (csfile, Err_IllegalIdent);
            }
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            break;
    }
}


/*!
 * \brief
 * Generate opcodes for:
 * LD[W][.ddir] bc|de|hl|sp|ix|iy , nn
 * LD[W][.ddir] rr,rr
 * LD[W][.ddir] hl,i
 * \param csfile
 * \param destreg
 */
static void
LD_z380_16bitDestReg_SrcDirect (sourcefile_t *csfile, enum z80rr dstreg16, enum ddirmodes ddirmode)
{
    int lstpatchpos = 0;
    enum z80rr srcreg16 = CheckReg16Mnem (csfile);

    /* if any DDIR suffix has been specified, generate opcodes now */
    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode);

    /* first handle source as address */
    if (srcreg16 == z80rr_unknown) {
        if (dstreg16 == z80r_hl && CheckReg8Mnem(csfile) == z80r_i) {
            /* LD HL,I */
            StreamCodeByte (csfile, 0xdd);
            StreamCodeByte (csfile, 0x57);
            return;
        }

        switch (dstreg16) {
            /* LD[.ddir]  ix|iy , nn */
            case z80r_ix:
            case z80r_iy:
                StreamCodeByte (csfile, (dstreg16 == z80r_ix) ? 0xdd : 0xfd);
                StreamCodeByte (csfile, 33);
                lstpatchpos = 2;
                break;

            default:
                /* LD[.ddir]  bc|de|hl|sp , nn */
                StreamCodeByte (csfile, (unsigned char) (dstreg16 * 16 + 1));
                lstpatchpos = 1;
                break;
        }

        DDirImmediateAddress(csfile, NULL, ddirmode, lstpatchpos);
        return;
    }

    /* ld rr16,rr16 */
    switch (dstreg16) {

        case z80r_hl:
        case z80r_bc:
        case z80r_de:
            if (dstreg16 == z80r_hl)
                dstreg16 = z80r_hl_z380;

            switch (srcreg16) {
                case z80r_hl:
                    StreamCodeByte (csfile, (unsigned char) 0xcd | (unsigned char) 3*16);
                    StreamCodeByte (csfile, (unsigned char) 0x02 | (unsigned char) (dstreg16)*16);
                    break;
                case z80r_bc:
                    StreamCodeByte (csfile, (unsigned char) 0xcd | (unsigned char) 2*16);
                    StreamCodeByte (csfile, (unsigned char) 0x02 | (unsigned char) (dstreg16)*16);
                    break;
                case z80r_de:
                    StreamCodeByte (csfile, (unsigned char) 0xcd | (unsigned char) 16);
                    StreamCodeByte (csfile, 0x02 | (unsigned char) (dstreg16)*16);
                    break;

                case z80r_ix:
                case z80r_iy:
                    StreamCodeByte (csfile, (srcreg16 == z80r_ix) ? 0xdd : 0xfd);
                    StreamCodeByte (csfile, (unsigned char) 0x0b | (unsigned char) dstreg16*16);
                    break;

                default:
                    ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80r_ix:
        case z80r_iy:
            /* LD IX|IY, BC|DE|HL|IX|IY */
            StreamCodeByte (csfile, (dstreg16 == z80r_ix) ? 0xdd : 0xfd);

            switch (srcreg16) {
                case z80r_hl:
                case z80r_bc:
                case z80r_de:
                    if (srcreg16 == z80r_hl)
                        srcreg16 = z80r_hl_z380;
                    StreamCodeByte (csfile, 0x07 | (unsigned char) srcreg16*16);
                    break;

                case z80r_ix:
                case z80r_iy:
                    if (dstreg16 != srcreg16)
                        StreamCodeByte (csfile, 0x27);
                    else
                        ReportError (csfile, Err_IllegalIdent); /* ld ix,ix or iy,iy is illegal */
                    break;

                default:
                    ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80r_sp:
             /* special cases: LD SP,rr */
             switch (srcreg16) {
                 case z80r_hl:
                     /* LD  SP,HL  */
                     StreamCodeByte (csfile, 249);
                     break;

                 case z80r_ix:
                 case z80r_iy:
                     /* LD  SP,IX/IY */
                     StreamCodeByte (csfile, (dstreg16 == z80r_ix) ? 0xdd : 0xfd);
                     StreamCodeByte (csfile, 249);
                     break;

                 default:
                     ReportError (csfile, Err_IllegalIdent);
             }
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
            break;
    }
}


/**
 * @brief All LD instructions for Z80+Z380
 * @param csfile
 */
static void LD_z380_(sourcefile_t *csfile)
{
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);
    const unsigned char *exprptr;
    enum z80r srcreg8;
    enum z80r dstreg8;
    enum z80rr dstreg16;

    if (csfile->sym == lparen) {
        LD_z380_dst_indrct(csfile, ddirmode);
        return;
    }

    switch (dstreg8 = CheckReg8Mnem (csfile)) {
        case z80r_unknown:
            dstreg16 = GetLD16bitDestReg(csfile); /* LD rr, */
            if (asmerror == true)
                return; /* illegal AF or unknown 16bit register were identified */

            exprptr = MemfTellPtr(csfile->mf); /* poll for '(', to decide which instruction is to be expected */
            if (GetSym(csfile) == lparen) {
                /* LD rr,(nn)   ;  LD rr,(rr) */
                LD_z380_16bitDestReg_SrcindDirect(csfile, ddirmode, dstreg16);
            } else {
                /* LD[.ddir]  rr,nn   ;   LD[.ddir] rr,rr */
                MemfSeekPtr(csfile->mf, exprptr);
                GetSym(csfile);
                LD_z380_16bitDestReg_SrcDirect(csfile, dstreg16, ddirmode);
            }
            break;

        case z80r_f:
            /* LD F,? */
            ReportError (csfile, Err_IllegalIdent);
            break;

        case z80r_i:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            GetSym(csfile);
            if (CheckReg8Mnem (csfile) == z80r_a) {
                /* LD  I,A */
                StreamCodeByte (csfile, 0xED);
                StreamCodeByte (csfile, 71);
                return;
            }

            if (CheckReg16Mnem (csfile) == z80r_hl) {
                /* LD  I,HL */
                StreamCodeByte (csfile, 0xdd);
                StreamCodeByte (csfile, 0x47);
                return;
            }

            ReportError (csfile, Err_IllegalIdent);
            break;

        case z80r_r:
            /* LD  R,A */
            LD_ira(csfile, 79);
            break;

        default:
            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);
                return;
            }

            if (GetSym(csfile) == lparen) {
                /* LD  r,(HL)  ;   LD  r,(IX|IY+d)  ;  LD A,(nn)  ;  LD A,(BC|DE) */
                LD_z380_8bitdstreg_indrct (csfile, ddirmode, dstreg8);
                return;
            }

            srcreg8 = CheckReg8Mnem (csfile);
            if (srcreg8 == z80r_unknown) {
                /* LD  r,n */
                if (dstreg8 & 8) {
                    /* LD IXl,n or LD IXh,n */
                    StreamCodeByte (csfile, 0xDD);
                } else if (dstreg8 & 16) {
                    /* LD  IYl,n or LD  IYh,n */
                    StreamCodeByte (csfile, 0xFD);
                }
                dstreg8 &= 7;
                StreamCodeByte (csfile, (unsigned char) dstreg8 * 8 + 6);
                ProcessExpr (csfile, NULL, RANGE_8UNSIGN, 1);
                return;
            }

            if (srcreg8 == z80r_f) {
                /* LD x, F */
                ReportError (csfile, Err_IllegalIdent);
                return;
            }

            if ((srcreg8 == z80r_i) && (dstreg8 == z80r_a)) {
                /* LD A,I */
                StreamCodeByte (csfile, 0xED);
                StreamCodeByte (csfile, 87);
                return;
            }

            if ((srcreg8 == z80r_r) && (dstreg8 == z80r_a)) {
                /* LD A,R */
                StreamCodeByte (csfile, 0xED);
                StreamCodeByte (csfile, 95);
                return;
            }

            if ((dstreg8 & 8) || (srcreg8 & 8)) {
                /* IXl or IXh */
                StreamCodeByte (csfile, 0xDD);
            } else if ((dstreg8 & 16) || (srcreg8 & 16)) {
                /* IYl or IYh */
                StreamCodeByte (csfile, 0xFD);
            }
            srcreg8 &= 7;
            dstreg8 &= 7;

            /* LD  r,r */
            StreamCodeByte (csfile, (unsigned char) (64 + dstreg8 * 8 + srcreg8));
    }
}


/**
 * @brief LDW[.ddir] (BC|DE|HL),rr | LDW[.ddir] (BC|DE|HL),nn
 * @param csfile
 * @param dstreg the 16bit destination register BC, DE or HL
 * @param ddirmode
 */
static void
LDW_dst_bcdehl_indrct(sourcefile_t *csfile, enum z80ia dstreg, enum ddirmodes ddirmode)
{
    enum z80rr src16reg;

    if (csfile->sym != comma) {
        ReportError (csfile, Err_Syntax);
        return;
    }

    /* if any DDIR suffix has been specified, generate opcodes now */
    if (ddirmode != ddirmode_notspecfied)
        DDIRinstr(csfile, ddirmode);

    GetSym(csfile);
    src16reg = CheckReg16Mnem(csfile);

    if (dstreg == z80ia_hl)
        dstreg = z80ia_hl_z380; /* for Z380 mnemonics, HL = 011b */

    switch (src16reg) {
        case z80r_bc:
        case z80r_de:
        case z80r_hl:
            if (src16reg == z80r_hl)
                src16reg = z80r_hl_z380; /* for Z380 mnemonics, HL = 011b */

            StreamCodeByte (csfile, 0xfd);
            StreamCodeByte (csfile, (unsigned char ) (src16reg * 16 | 0x0c | dstreg));
            break;

        case z80r_ix:
            StreamCodeByte (csfile, 0xdd);
            StreamCodeByte (csfile, (unsigned char ) (dstreg * 16 | 1));
            break;

        case z80r_iy:
            StreamCodeByte (csfile, 0xfd);
            StreamCodeByte (csfile, (unsigned char ) (dstreg * 16 | 1));
            break;

        case z80rr_unknown:
            /*
             * LDW[.ddir] (BC|DE|HL),nn
             * 11101101 00pp0110 -n(low)- -n(high)
             */
            StreamCodeByte (csfile, 0xed);
            switch(dstreg) {
                case z80ia_bc:
                case z80ia_de:
                case z80ia_hl_z380:
                    StreamCodeByte (csfile, (unsigned char) (6 + dstreg*16));
                    break;

                default:
                    ReportError (csfile, Err_IllegalIdent);
                    return;
            }

            /* immediate constant size (16bit, 24bit or 32bit) depends on DDIR .IB, .IW or none */
            DDirImmediateAddress(csfile, NULL, ddirmode, 2);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/**
 * @brief all 16/32 bit destination indirect operations
 * @details
 * ldw[.ddir] (rr),rr
 * ldw[.ddir] (rr),nn
 * ldw[.ddir] (nn),rr
 * ldw[.ddir] (ix/iy/sp+d),rr
 * ldw[.ddir] rr,(ix/iy/sp+d)
 * @param csfile
 */
static void
LDW_16bitregIndrct(sourcefile_t *csfile, enum ddirmodes ddirmode)
{
    enum z80rr srcreg16;
    expression_t *idxoffsetexpr = NULL;
    enum z80ia iareg = CheckIndirectAddrMnem (csfile);

    switch (iareg) {
        case z80ia_bc:
        case z80ia_de:
        case z80ia_hl:
            /* LD  (HL),XX | LD (DE),XX | LD (BC),XX */
            LDW_dst_bcdehl_indrct(csfile, iareg, ddirmode);
            break;

        case z80ia_sp: /* LD[.ddir] (SP+d),rr */
        case z80ia_ix: /* LD[.ddir] (IX|IY+d),X */
        case z80ia_iy:

            if (csfile->sym != rparen) {
                /* an index offset expression is probably specified, parse it */
                if ((idxoffsetexpr = ParseNumExpr (csfile)) == NULL) {
                    return;    /* failed to parse to right bracket */
                }
            }

            if (GetSym(csfile) != comma) {
                ReportError (csfile, Err_Syntax);   /* comma missing! */
                FreeNumExpr (idxoffsetexpr);        /* remove this expression again */
                return;
            }

            GetSym(csfile);
            srcreg16 = CheckReg16Mnem (csfile);
            switch(srcreg16) {
                case z80r_bc:
                case z80r_de:
                case z80r_hl:
                case z80r_ix:
                case z80r_iy:
                    if (iareg == z80ia_sp)
                        LD_z380_IndirectIXIYSP_16bitSrcReg(csfile, ddirmode, idxoffsetexpr, z80ia_sp, srcreg16, 9);
                    else
                        LD_z380_IndirectIXIYSP_16bitSrcReg(csfile, ddirmode, idxoffsetexpr, iareg, srcreg16, 11);
                    break;

                default:
                    ReportError (csfile, Err_IllegalIdent);
            }
            break;

        case z80ia_nn:
            /* LD  (nn),rr  ;  LD  (nn),A */
            LD_z380_address_indrct (csfile, ddirmode);
            break;

        default:
            ReportError (csfile, Err_IllegalIdent);
    }
}


/**
 * @brief all 16/32 bit load operations (some are also alias for Z80 LD)
 * @details
 * ldw[.ddir] rr,nn
 * ldw[.ddir] rr,rr
 * ldw[.ddir] rr,(rr)
 * ldw[.ddir] (rr),rr
 * ldw[.ddir] (rr),nn
 * ldw[.ddir] (ix/iy/sp+d),rr
 * ldw[.ddir] rr,(ix/iy/sp+d)
 * @param csfile
 */
static void
LDW(sourcefile_t *csfile)
{
    enum z80rr dstreg16;
    enum ddirmodes ddirmode = FetchDdirSuffix(csfile);

    if (ddirmode == ddirmode_unknown)
        return; /* parsing of DDIR suffix gave syntax error */

    if (csfile->sym == lparen) {
        /* LDW[.ddir] (??),?? */
        LDW_16bitregIndrct(csfile, ddirmode);
        return;
    }

    if (CheckReg8Mnem (csfile) == z80r_i) {
        /* special case LDW I,HL */
        if (GetSym(csfile) != comma) {
            ReportError (csfile, Err_Syntax);
            return;
        }

        GetSym(csfile);
        if (CheckReg16Mnem(csfile) == z80r_hl) {
            StreamCodeByte (csfile, 0xdd);
            StreamCodeByte (csfile, 0x47);
        } else {
            ReportError (csfile, Err_IllegalIdent);
        }
        return;
    }

    dstreg16 = GetLD16bitDestReg(csfile); /* LD rr16, */
    if (asmerror == true)
        return; /* illegal AF or unknown 16bit register were identified */

    if (GetSym(csfile) == lparen) {
        /* LDW[.ddir] rr16,(nn)   ;  LDW[.ddir] rr16,(rr16) */
        LD_z380_16bitDestReg_SrcindDirect(csfile, ddirmode, dstreg16);
    } else {
        /* LDW[.ddir]  rr16,nn   ;   LDW[.ddir] rr16,rr16  ;  LDW HL,I */
        LD_z380_16bitDestReg_SrcDirect(csfile, dstreg16, ddirmode);
    }
}


/******************************************************************************
                               Public functions
 ******************************************************************************/


/*!
 * \brief Return pointer to function that parses Z380 mnemonic source line and generates code
 * \param st symbol type
 * \param id pointer to identifier
 * \return pointer to mnemonic parser function, or NULL if not found
 */
mnemprsrfunc
LookupZ380Mnemonic(enum symbols st, const char *id)
{
    return LookupIdentifier (id, st, (identfunc_t *) z380idents, sizeof(z380idents)/sizeof(idf_t));
}

